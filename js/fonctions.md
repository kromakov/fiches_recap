# Les fonctions

Des fonctions comme `alert()` sont disponible par défaut dans JavaScript.
Mais on peut aussi définir nous-même nos propres fonctions. Ça va nous permettre
d'exécuter une série d'instructions. On va pouvoir lui donner un ou des paramètres
qui seront repris dans le corps de la fonction.

## Fonction simple

### Définition

Pour définir une fonction, on utilise `function` :

```js
function hello() {
  console.log('hello world !');
}
```

Il est également possible de définir une fonction anonyme dans une variable

```js
var hello = function () {
  console.log('hello world !');
};
```

### Exécution

```js
hello(); // produira un 'hello world !' dans la console
```

## Fonction avec un retour

Dans la définition de notre fonction, on va définir une série d'instructions,
avec ce qu'on veut à l'intérieur. On va pouvoir aussi faire en sorte que cette
fonction "retourne" une valeur, avec `return` :

```js
function getMessage(name) {
  return 'hello ' + name +' !';
}

// message vaut 'hello Gringo !'
var message = getMessage('Gringo');
```

Cette fois-ci, au lieu d'afficher, on retourne une string, qui pourra être
utilisée par la suite.

**Attention** : Une fois que l'on utilise `return`, la fonction s'arrête.

## Fonction avec des paramètres

Si on souhaite utiliser une fonction en lui fournissant des arguments (lors de l'exécution),
il faut que la définition de la fonction possède des paramètres :

### Définition

La définition indique que la fonction prend un **paramètre** `name`. 
Le paramètre est comme une variable au sein de la fonction.

```js
function hello(name) {
  console.log('hello ' + name +' !');
}
```

### Exécution

Lors de l'exécution, on passe en **argument** la valeur `'Gringo'`. 
Cette valeur est assignée à `name` dans le corps de la fonction.

```js
hello('Gringo'); // produira un 'hello Gringo !' dans la console
```

**Qu'est-ce qu'il se passe ?**

1. J'exécute la fonction hello avec un argument `'Gringo'`
2. Ma fonction a été définie avec un paramètre `name`, qui va stocker l'argument donné.  
  > C'est comme si avant d'exécuter la fonction, on faisait `var name = 'Gringo';`
3. Chaque instruction de la fonction est interpretée

### Plusieurs paramètres 

Dans nos fonctions, on peut aussi avoir plusieurs paramètres.

```js
function addition(a, b) {
	return a + b;
}

// resultat vaut 12
var resultat = addition(5, 7);
```
