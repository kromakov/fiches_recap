# Événements

JavaScript est un langage tourné vers la gestion des interactions entre l'utilisateur et le navigateur. On parle d'événements utilisateurs : cliquer, scroller, soumettre un formulaire, etc.

## addEventListener

[[MDN] addEventListener](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener)

Permet de placer un nouvel écouteur d'événement, c'est-à-dire d'indiquer au navigateur comment réagir à des événements futurs :

```js
element.addEventListener(
  eventType,
  handler
)
```

où :

- `element` est une référence vers un nœud du [DOM](dom.md), obtenue auparavant ;
- `eventType` est une chaîne de caractère décrivant le type d'événement à surveiller, sur `element` ;
- `handler` est une fonction anonyme (`function() { … }`) ou une référence de fonction.

Exemple :

``` js
var element = document.getElementById('inscription');
var subscribeUser = function() {
  // TODO: enregistrer l'inscription de l'utilisateur
};
element.addEventListener('click', subscribeUser);
```

### Objet-événement

Le handler, aussi appelé callback ou écouteur d'événement, est automatiquement appelé par le navigateur, et reçoit en paramètre un objet décrivant l'événement qui vient de survenir :

``` js
var subscribeUser = function(evt) {
  console.log(evt);
  // TODO: enregistrer l'inscription de l'utilisateur
}
```

L'objet-événement contient de nombreuses informations utiles, et notamment une [méthode `preventDefault`](https://developer.mozilla.org/fr/docs/Web/API/Event/preventDefault) qui permet, lorsque c'est utile, de bloquer le comportement par défaut du navigateur :

``` js
document.getElementByTagName('form').addEventListener('submit', function(evt) {
  evt.preventDefault(); // empêche le rechargement automatique de la page
});
```

## Plus en détails

### Types d'événements

[[MDN] Liste des types d'événements](https://developer.mozilla.org/en-US/docs/Web/Events)

Il existe de nombreux types d'événements auxquels réagir. Il est même possible de créer ses propres types avec [`new Event('montype')`](https://developer.mozilla.org/fr/docs/Web/Guide/DOM/Events/Creating_and_triggering_events).

### Handler et traitement d'informations

Un événement, par exemple un `'click'` sur un `.button`, peut survenir plusieurs fois. La fonction handler sera executée à chaque occurence de l'événement, et comme pour toutes les fonctions en JS, il n'y a aucune persistance d'information entre les différents appels de fonction. Il convient donc de coder ses handlers de façon à ce qu'ils soient autonomes dans leurs traitements d'information.

Le handler peut toutefois recevoir un objet en paramètre, généralement nommé `event` ou `evt`. Cet objet est automatiquement rempli avec des informations diverses sur l'événement auquel le handler réagit. Il n'est pas possible de personnaliser cet objet. Par contre, l'objet donne accès à une référence vers l'élément du DOM qui a subit l'événement : `evt.target`. Cet élément du DOM peut tout à fait exploiter les [attributs `data-`](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Comment/Utiliser_attributs_donnes) pour transmettre des informations spécifiques, exploitables en JS dans le handler.

### Supprimer un écouteur d'événement

```js
bouton.removeEventListener('click', cliqueBouton);
```
