Vous souhaitez réaliser un objectif spécifique en JS ? Voici des pistes.

## Debugguer mon code

### `console.log`

https://developer.mozilla.org/fr/docs/Web/API/Console

```js
// Afficher une donnée en console :
console.log('debug');
console.log(myVar);
console.info(myVar);
console.warn(myVar);
console.error(myVar);
```

### `debugger`

https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/debugger

``` js
// Lancer le debugger
debugger;
```

## Gérer des types de données incompatibles

Attention aux types de données, notamment les données reçues depuis l'utilisateur :

```js
var number1 = prompt('Tapez un nombre');
// si l'utilisateur tape 12 au clavier, le résultat stocké est "12", une chaîne de caractère

console.log(number1 + 10);
// JS détermine que number1 est une chaîne, donc l'opérateur + est la concaténation, pas l'addition
// et la valeur 10 est automatiquement convertit en "10" => résultat : 22
```

Il est possible de convertir des valeurs dans d'autres types avec `Number`, `String`, `parseInt`, etc.

## Récupérer un élément HTML

```html
<p class="introduction">
  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
</p>

<div id="chapeau">
  <p class="important">
    Contenu important
  </p>
</div>
```

**Avec `getElementById()`**
```js
var elementChapeau = document.getElementById('chapeau')
```

**Avec `querySelector()`**
```js
var elementChapeau = document.querySelector('#chapeau')
```

**Avec `querySelectorAll()`**
```js
var elements = document.querySelector('#chapeau, .introduction');
//elements vaut NodeList [ <p.introduction>, <div#chapeau> ]

var introduction = elements[0]; // <p.introduction>
var chapeau = elements[1]; // <div#chapeau>
```

## Modifier le contenu d'un élément

Ajouter du contenu texte
```js
element.textContent = 'Hello!';
```

Ajouter du contenu HTML
```js
element.innerHTML = '<strong>Hello!</strong>';
```

## Modifier les attributs d'un élément

```js
// Récupérer l'élément...
var element = document.getElementById('link');

// ...puis ajouter des attributs
element.id = 'link-contact';
element.href = 'https://oclock.io';
element.className = 'navigation-link';
```

## Créer un élément

```js
var element = document.createElement('div');
```

Ajouter au DOM

```js
// Création
var item = document.createElement('li');
// Élément du DOM
var list = document.querySelector('ul');
// Ajout de l'élément au DOM
list.appendChild(item);
```

## Stocker de l'information

Les objets sont très flexibles

```js
var player = {
  name: 'Link',
  alive: true,
  level: 3,
  inventory: [
    'sword',
    'shield',
    'boomerang',
  ],
  move: function() {
    // ...
  }
};
```

## Organiser son code

Voir la fiche [Modules](modules.md).

## Exécuter quelque chose suite à un événement

Lors d'un clic, d'un envoi de formulaire, d'une pression sur une touche du clavier...

```js
// event : 'click', 'submit', 'keyup', 'DOMContentLoaded'...
// handler : fonction exécutée quand l'événement survient
element.addEventListener(type, handler);
```

En prenant la situation suivante

```html
<button id="change">Change le résultat</button>
<div id="resultat">aucun</div>
```

```js
// fonction exécutée lorsque l'événement se produira - communément appelée 'handler'
function changeResultat() {
  var resultat = document.querySelector('#resultat');
  resultat.textContent = 'nouveau résultat';
}

var boutonAjouter = document.querySelector('#ajouter');

// on ajoute un écouteur d'événement à notre bouton, il écoute le clic
boutonAjouter.addEventListener('click', changeResultat);
```

## Exécuter quelque chose après un certain temps

```html
<div id="resultat">aucun</div>
```

```js
function changeResultat() {
  var resultat = document.querySelector('#resultat');
  resultat.textContent = 'nouveau résultat';
}

// la fonction 'changeResultat' sera exécutée au bout 3000ms ou 3 secondes
setTimeout(changeResultat, 3000);
```
