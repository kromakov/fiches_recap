# Comment mettre à jour une fiche-récap ?

Tu t'es peut-être aperçu d'une faute d'orthographique ? D'une erreur de syntaxe dans un exemple de code ?
Tant mieux, c'est que les formateurs ont encore une part d'humanité !

## Où aller ? Sur quoi cliquer ?

En haut de chaque fiche-récap, tu trouveras ce bouton en forme de crayon.

![Bouton](https://user-images.githubusercontent.com/24500083/39151791-85566f56-4746-11e8-93d1-0955fa9f2c83.png)

En cliquant dessus, tu basculeras sur un éditeur de texte te permettant de faire les modifications que tu souhaites apporter.

**[Attention tu dois écrire en MarkDown !](https://fr.wikipedia.org/wiki/Markdown)**

![Editeur](https://user-images.githubusercontent.com/24500083/39151792-8570d8b4-4746-11e8-901d-05e6ac4789f8.png)

## J'ai fait mes modifications et maintenant ?

N'hésites pas à voir si tes corrections correspondent bien à ce que tu souhaites en cliquant sur *Preview changes*.

*C'est bon ? Tu es satisfait ?*

Alors tout en bas de la page, tu trouveras 2 champs à remplir :
* Le titre de ta modification. Je te conseille de mettre le chemin de la fiche ainsi que les modifications apportées.
* Tu peux décrire le plus précisément possible ce que modifie ta correction et éventuellement la nouveauté que tu souhaites apporter.

![Send](https://user-images.githubusercontent.com/24500083/39151790-853fe95c-4746-11e8-8adb-978731d1a595.png)

Il ne te reste plus qu'à appuyer sur le gros bouton vert *Create pull request* afin de faire partir ta requête aux admins !

![Pull](https://user-images.githubusercontent.com/24500083/39151789-8528f116-4746-11e8-84b5-e7d915c73e55.png)
