# Protocole HTTP

## Méthodes HTTP et leur sens

| Method | Sens |
|---|---|
| GET | Lire des données |
| POST | Insérer des données |
| PUT or PATCH | Mettre à jour ou insérer des données |
| DELETE | Supprimer des données |

En réalité, la plupart des navigateurs actuels ne supportent que POST et GET dans les formulaires HTML. D'autres sont mieux supportés avec [XMLHttpRequest](https://en.wikipedia.org/wiki/XMLHttpRequest).

Source : [Symfony : les fondamentaux HTTP](http://symfony.com/doc/current/introduction/http_fundamentals.html)
