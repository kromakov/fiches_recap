# Introduction

Lorsqu'on parle de conception de base de données, on emploie le terme de **_modélisation_**. L'objectif est de représenter et d'organiser les données de notre **_système d'informations_** de manière optimale.

## A quels problèmes cela répond-t-il ?

A première vue, organiser des données ne semble pas forcément problématique, si on pense à une suite de feuilles de tableur par exemple. Prenons un exemple concret :

### Une liste de livres

Titre|Auteur|Année|Genre|
-|-|-|-|
Harry Potter à l'école des sorciers|Joanne Kathleen Rowling|1997|Aventure|
Le Petit Prince|Antoine De Saint-Exupéry|1943|Philosophie|
Orgueil et préjugés|Jane Austen|1813|Littérature classique|
Harry Potter et la Chambre des Secrets|Joanne Kathleen Rowling|2002|Aventure|
Dix petits nègres|Agatha Christie|1939|Policier|
Journal|Anne Frank|1950|Autobiographie|
Les Misérables|Victor Hugo|1862|Litérature classique|
Harry Potter et le Prisonnier d'Azkaban|Joanne Kathleen Rowling|2004|Aventure|
Le Dernier Jour d'un condamné|Victor Hugo|1829|littérature classique|

Deux **problèmes** émergent de ce tableau très simple :
1. Des informations identiquent se répètent : les noms des auteurs et les genres. Dans le jargon on emploie le terme de **_redondance_**. Ces informations risquent donc d'être dupliquées de nombreuses fois, surtout quand on va s'attaquer à de vraies bibliothèques... Cela impliquera donc **_une utilisation superflue des ressources de stockage_**.
2. Lorsque nous saisissons nos informations nous pouvons faire des erreurs de frappe ou d'homogénéité des valeurs. Notez dans le tableau ci-dessus _Litérature_ sans _t_ ou encore _littérature_ avec ou sans majuscule. Il y a donc **_un risque d'hétérogénéité_** (et inversement les données ne seront pas _homogènes_).

La **solution** commune à ces deux problèmes est toute simple : nous allons organiser les genres dans un tableau à part. Dans le même temps et afin de préciser à quel genre appartient un livre, nous allons identifier chaque genre par un numéro unique. Cela donne :

#### Genres

N°Genre|Genre|
-|-|
1|Autobiographie|
2|Aventure|
3|Littérature classique|
4|Philosophie|
5|Policier|

#### Livres

Titre|Auteur|Année|N°Genre|
-|-|-|-|
Harry Potter à l'école des sorciers|Joanne Kathleen Rowling|1997|2|
Le Petit Prince|Antoine De Saint-Exupéry|1943|4|
Orgueil et préjugés|Jane Austen|1813|3|
Harry Potter et la Chambre des Secrets|Joanne Kathleen Rowling|2002|2|
Dix petits nègres|Agatha Christie|1939|5|
Journal|Anne Frank|1950|1|
Les Misérables|Victor Hugo|1862|3|
Harry Potter et le Prisonnier d'Azkaban|Joanne Kathleen Rowling|2004|2|
Le Dernier Jour d'un condamné|Victor Hugo|1829|3|

Ainsi, *la redondance a été supprimée* et nous avons *garanti l'homogénéité des données*. Ce faisant nous avons créé une **relation** entre deux tableaux (deux **tables**, deux **entités**), d'où le terme de *Bases de Données Relationnelles*.

> Définition : **Les entités** sont des objets ou des concepts que l'on utilise **pour regrouper nos données de manière cohérente** (dans le même esprit que ce qui peut se faire en POO).

## Une méthode

- La méthode va nous permettre de conceptualiser les données.
- Mais aussi d'utiliser une norme partagée par les collaborateurs d'un projet : chef de projet, développeurs, voire même le client.

### Intérêt de la conception

L'organisation des données va nous donner _en plus_ les avantages suivants :
- Nous faire se poser les bonnes questions sur les données et plus généralement sur le projet en lui-même.
- Comprendre les interactions au sein du projet.
- Apporter un gain de temps - et d'efficacité - sur le long terme.

## Méthodologie

On peut résumé la méthode comme ceci :
- **Expression du besoin** (client).
- _Analyse du besoin_ (identification des données et des entités dans un **Dictionnaire de Données**).
- Création du **Modèle Conceptuel** de Données (MCD). On parle également de **Modèle Entité-Association**.
- Création du **Modèle Logique** de Données (MLD).
- Création du **Modèle Physique** de Données (MPD).

Pas de panique, on va expliquer tout ça !

---

> [Retour au sommaire](./README.md)