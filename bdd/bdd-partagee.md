# Accéder à des bases données "partagées"

Si vous souhaitez tous utiliser la même base de données depuis vos téléporteurs, vous pouvez le faire :

1. Avec bdd hébergée sur le téléporteur de vos camarades.
2. Avec bdd hébergée sur serveur Gandi.

> A noter que **si vous travaillez sur le TP, ce TP doit être allumé et connecté** quand vos camarades souhaitent travailler dessus, **contrairement au serveur Gandi qui lui, est toujours disponible !**

## 1. Avec bdd hébergée sur le téléporteur de vos camarades

Il vous faut :
- L'adresse (hôte) VPN de la machine en question,
[voir la liste ici](https://docs.google.com/spreadsheets/d/1aO_kIoCHKjzTnhjKubc1hfnH-C8ocZTYzmG84rT6Zl8/edit#gid=95858001)
- Modifier une configuration dans le MySQL qui partagera sa bdd
  - éditer `nano /etc/mysql/mysql.conf.d/mysqld.cnf` (avec sudo si besoin) et commenter ligne 43 (avec le caractère \# en début de ligne) :
  - `# bind-address = 127.0.0.1`
  - cela permet que mysql n'"écoute" pas uniquement sur votre IP locale
  - éxécuter `service mysql restart` (avec sudo si besoin) pour redémarrer le serveur mysql

Ensuite :

- Vous connecter à PhpMyAdmin (PMA) **depuis le PMA du téléporteur qui hébergera la bdd partagée**
- Créer une bdd et l'utilisateur qui va avec ainsi que les droits associés en lecture/écriture/etc.
- Dans votre config de script PHP de connexion, renseignez :
  - host : l'adresse VPN issue de la liste par ex. `melanie-roussel.vpnuser.oclock.io`
  - le nom de la bdd créée,
  - le nom d'utilisateur créé
  - le mot de passe de l'utilisateur créé
  
Ces paramètres seront utilisés dans vos scripts PHP **(à la création de l'utilisateur dans PMA, pour le nom d'hôte du nom d'utilisateur, mettre `%`)**

=> Testez votre script d'accès à la bdd !

## 2. Avec bdd hébergée sur serveur Gandi

Il vous faut :
- Vous connecter à PhpMyAdmin (PMA) sur le serveur qui hébergera la bdd.
- Créer une bdd et l'utilisateur qui va avec ainsi que les droits associés en lecture/écriture/etc.
- Dans votre config de script PHP de connexion, renseignez :
  - host : **l'adresse IP du serveur en question (sans http)**
  - le nom de la bdd créée,
  - le nom d'utilisateur créé
  - le mot de passe de l'utilisateur créé

=> Le cas échéant il faudra peut-être modifier le fichier `mysql/mysql.conf.d/mysqld.cnf` comme indiqué plus haut.

=> Testez votre script d'accès à la bdd !

**En cas de souci allez parler à dario ou jc sur Slack :) !**
