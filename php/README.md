# PHP

## Fiches récap

- [La syntaxe](syntaxe.md)
- [Les conditions](conditions.md)
- [Les formulaires](forms.md)
- [Les boucles](boucles.md)
- [Les fonctions](fonctions.md)
- [Les fonctions utiles](fonctions-utiles.md)
- [Les sessions](sessions.md)
- [Problématique serveur](server.md)
- [Authentification, sécurité](securite.md)
- [Bufferisation de sortie (output buffering)](output-buffering.md)
