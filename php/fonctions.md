# Les fonctions

On sait que l'on utiliser des fonctions, comme `date()` ou `count()`.  
Mais on peut aussi définir nous-même nos propres fonctions. Ça va nous permettre
d'exécuter une série d'instruction. On va pouvoir lui donner un ou des paramètres,
qui sera repris dans le corps de la fonction.

## Définir une fonction

### Cas simple

Pour définir une fonction, on utilise `function` :

```php
<?php

function hello() {
	// Affiche une string
	echo "Hello Wolrd!";
}

```

### Avec des paramètres

Si on souhaite utiliser une fonction en lui fournissant des arguments,
il faut que la définition de la fonction possède des paramètres :

```php
<?php

function hello($name) {
	// Affiche une string
	echo "Hello $name";
}

```

### Avec un retour

Dans la définition de notre fonction, on va définir une série d'instructions,
avec ce qu'on veut à l'intérieur. On va pouvoir aussi faire en sorte que cette
fonction "retourne" une valeur, avec `return` :

```php
<?php

function getHello($name) {
	// Retourne une string
	return "Hello $name";
}

```

Cette fois-ci, au lieu d'afficher, on retourne une string, qui pourra être
utilisée par la suite. Ça sera plus clair quand on va…


## Exécuter une fonction

Une fois définies, on va pouvoir les utiliser. On appelle ça "exécuter"
la fonction.

### Cas simple

```php
<?php

// Je définis ma fonction
function hello() {
	echo "Hello World!";
}

// Je l'exécute
// Affiche : Hello World!
hello();

```

### Avec des paramètres

Si on a défini des paramètres dans la définition de ma fonction, je peux
exécuter ma fonction en lui donnant des "arguments", c'est-à-dire des valeurs
qui vont être prises comme paramètres.

```php
<?php

// Je définis ma fonction, avec le paramètre $name
function hello($name) {
	echo "Hello $name";
}

// J'exécute ma fonction avec l'argument "Rouky"
// Affiche : Hello Rouky
hello("Rouky");

```

Qu'est-ce qu'il se passe ?
1. J'exécute la fonction hello avec un argument `"Rouky"`

2. Ma fonction a été définie avec un paramètre `$name`, qui va stocker l'argument donné.  
C'est comme si avant d'exécuter la fonction, on faisait `$name = "Rouky";`

3. Chaque instruction de la fonction est interpretée

### Avec un retour

Lorsque l'on exécute une fonction définie avec un retour, non seulement les
instructions de la fonction seront exécutées, mais ensuite une valeur de retour
est envoyée. Ce retour peut être utilisée comme toute autre valeur, pour être
afficher avec `echo` ou stockée dans une variable par exemple.

```php
<?php

function getHello($name) {
	// On retourne une string
	return "Hello $name";
}

// La variable $phrase contient la valeur "Hello Julie"
$phrase = getHello("Julie");

```

**Attention** : Une fois que l'on utilise `return`, la fonction s'arrête.

```php
<?php

function getHello($name) {
	// On retourne une string, donc la fonction s'arrête
	return "Hello $name";

	// Le PHP n'ira jamais lire cette ligne
	echo "ce message ne sera jamais affiché !"
}

// La variable $phrase contient la valeur "Hello Julie"
$phrase = getHello("Julie");

```

Dans nos fonctions, on peut aussi avoir plusieurs paramètres ! :smiley:

```php
<?php

function addition($a, $b) {
	return $a + $b;
}

// Affiche : 12
echo addition(5, 7);

```

## Portée des variables

A chaque fois que l'on définit une fonction, on crée une nouvelle **portée de variable**,
c'est-à-dire qu'un nouveau stock de variable est créé.

``` php
<?php
$fruit = "Kiwi";

// Ici, la variable $fruit est définie.
// ...

function hello($str) {
	// Ici, la variable $fruit n'est pas définie.
	// Ici, la variable $str est définie, avec la valeur passée en argument
	// cette variable $str est uniquement créée dans la portée de la fonction

	// On peut aussi définir des variables à l'intérieur de la fonction
	// qui elles aussi seront détruites à la fin de la fonction
	// et ne se retrouveront pas dans la portée principale
	$name = 'Manu';
	echo $str . ' ' . $name . ' !';
}

// Ici, la variable $fruit est toujours définie.
// et la variable $str n'est pas définie.
// ...

// A l'intérieur de la fonction, $str vaudra "Bonjour"
// Affiche : Bonjour Manu !
hello('Bonjour');

// Ici, la variable $fruit est toujours définie.
// et la variable $str n'est toujours pas définie.
// ...

```

On peut utiliser le mot-clé `global` pour utiliser une variable définie dans
la portée principale :

```php
<?php

$fruit = "Kiwi";

function hello($str) {
	global $fruit;

	// Ici, la variable $fruit est définie, car on a explicitement
	// demandé avec `global` de récupérer la variable qu'on a créé.
	echo $str . ' ' . $fruit;
}

// Affiche : Bonjour Kiwi
hello('Bonjour');

```

### Petite histoire

Toutes les variables de mon script PHP sont dans une discothèque.

On distingue 2 parties dans la boite :
- le carré VIP (dans une fonction = `local`)
- le reste de la boite (hors des fonctions = `global`)

Par défaut, toutes les variables sont hors du carré VIP. Et on leur refuse l'accès au carré VIP.  
Le seul moyen d'entrer dans le carré VIP, c'est de donner le mot de passe => `global`

Ainsi, dans le code ci-dessus, la variable `$fruit` a donné le mot de passe `global $fruit;`, et a pu rentrer dans le carré VIP nommé `hello` !
