# PairProgramming

Le développement en _PairProgramming_ ou en binôme permet d'avoir 2 cerveaux en réflexion sur un seul code. On obtient un code plus propre et plus abouti.

Pour plus de détails sur cette technique de développement très utile en apprentissage mais très peu utilisé dans le monde du travail, direction [Wikipédia](https://fr.wikipedia.org/wiki/Programmation_en_bin%C3%B4me).

## Les rôles

- _Driver_ qui écrit le code
- _Observer_ qui lit le code et vérifie son exactitude, sa cohérence et son optimisation

## Mise en place

Pour travailler en téléprésentiel sur le même code, il faut :
- partager l'écran du _Driver_
- pouvoir discuter ensemble

Voici les différentes solutions techniques qui peuvent être mises en place.

### #1 Teletype + Discord/Slack

Le moyen le plus simple de partager son écran _Atom_ est l'utilisation du package [teletype](https://teletype.atom.io/).  
Teletype est une solution permettant à l' _Observer_ de modifier le code sur l'Atom du _Driver_.  
Cependant, il ne fonctionne pas toujours et permet parfois de ne partager qu'un seul fichier.

En complément, on peut utiliser _Slack_ ou _Discord_ pour se parler.

### #2 VSCode + LiveShare + Discord/Slack

Le moyen le plus simple de partager son écran _VSCode_ est l'utilisation de l'extension [liveshare](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare).  
LiveShare est une solution permettant à l' _Observer_ de modifier le code sur l'espace de travail du _Driver_.

En complément, on peut utiliser _Slack_ ou _Discord_ pour se parler.

### #3 Atom-pair + Discord/Slack

Le second moyen le plus simple de partager son écran _Atom_ est l'utilisation du package [atom-pair](https://atom.io/packages/atom-pair).  
Atom-pair est une solution permettant à l' _Observer_ de modifier le code sur l'Atom du _Driver_.  
Cependant, il n'est plus maintenu depuis mi-2017.

En complément, on peut utiliser _Slack_ ou _Discord_ pour se parler.

### #4 Tokbox

La [démo de tokbox](https://opentokdemo.tokbox.com/) permet de tester la visioconférence proposée par Tokbox.  
Le partage d'écran est possible en passant par une extension à installer.  
Il n'y a pas de limitation connue à ce jour.

### #5 Hangouts

[Hangouts](https://hangouts.google.com) permet de faire des visioconférences gratuitement.  
On n'a pas encore constaté de limitation dans le partage d'écran.

### #6 Appear.in

[Appear.in](https://appear.in/) permet de faire des visioconférences gratuitement jusqu'à 4 personnes.  
Le partage d'écran est possible en passant par une extension à installer.  
Cependant, le partage d'écran est limité à 20 minutes...
