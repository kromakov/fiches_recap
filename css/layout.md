# Layout

Ressources pour comprendre la mise en page en CSS

- [MDN : Introduction au positionnement CSS](https://developer.mozilla.org/fr/Apprendre/CSS/Introduction_%C3%A0_CSS/La_disposition)
- [LearnLayout : Apprendre les mises en page CSS](http://fr.learnlayout.com/)
- [MDN : Le modèle de boite](https://developer.mozilla.org/fr/Apprendre/CSS/Introduction_%C3%A0_CSS/Le_mod%C3%A8le_de_bo%C3%AEte)
- [CSS Reference.io : Le modèle de boite](http://cssreference.io/box-model/)
- [CSS Reference.io : Le positionnement](http://cssreference.io/positioning/)
- [MDN : Disposition multi-colonnes](https://developer.mozilla.org/fr/docs/Web/CSS/Colonnes_CSS/Utiliser_une_disposition_multi-colonnes)
- [MDN : Utilisation des flexbox](https://developer.mozilla.org/fr/docs/Web/CSS/Disposition_des_bo%C3%AEtes_flexibles_CSS/Utilisation_des_flexbox_en_CSS)