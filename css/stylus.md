
`Stylus` :  

Stylus sert à écrire du CSS mais légèrement différemment du CSS de base  
[Site Web de Stylus](http://stylus-lang.com/)

On connait tous la syntaxe normal de CSS mais je vous la remet on sait jamais :wink:
```css
body {
  font: 12px Helvetica, Arial, sans-serif;
}

a.button {
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
}
```

**Mais quel est l'interêt premier de Stylus?**   
Hé bien, en plus des variables (que l'on va voir plus bas), on va pouvoir se passer des `{ }` et des `;`.  
Donc le code plus haut devient alors :

```css
body
  font: 12px Helvetica, Arial, sans-serif

a.button
  -webkit-border-radius: 5px
  -moz-border-radius: 5px
  border-radius: 5px
```

## Mais avant tout !

Pour information : l'extension de fichier Stylus : `.styl`  

Attends !!! Comment on installe `Stylus` ?  :scream:

- Avec `Yarn` :
`yarn add --dev stylus-brunch`

Mais... Comment faire pour avoir la coloration syntaxique sous ATOM ?  
- Dans un `Shell` : `apm language-stylus`


## Les variables

```css
$black = #000;
$red = #EE1225

#monSuperBloc
  background: $red
  color: $black
```

## Les selecteurs
- Parent Reference :
Tout en suivant la norme [BEM](http://getbem.com/introduction/), on aura un code HTML qui peut avoir cette forme là par exemple :
```html
<div id="presentation">
  <div id="presentation-content">
    Ce code HTML est vraiment le meilleur !
  </div>
</div>
```

En CSS de base on aura une syntaxe comme ça :
```css
#presentation {
  color: #123;
}

#presentation-content {
  font-size: 1em;
}

#presentation-content:hover {
  font-weight: bold
}
```

Avec Stylus :
```css
#presentation
  color: #123

  &-content
    font-size: 1em

  &:hover
    font-weight: bold
```

## Import
On souhaite par exemple mettre dans un fichier `chart.styl` (qui se situe dans le dossier `dossierRacineDuProjet/style`) des variables qu'on utilisera plus tard dans un autre fichier stylus... Mais comment faire pour les utiliser plus tard ? Tout simplement, dans le fichier où on souhaite avoir accès aux variables précédemment déclaré, on utilise alors `@import "dossierRacineDuProjet/style/chart.styl"`
