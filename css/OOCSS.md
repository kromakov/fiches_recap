CSS orientée-objet (OOCSS)
==========================

> Écrire de la CSS, c'est facile. Écrire de la CSS qui tienne la route, et dans le temps, c'est autre chose !

Il y aurait comme une sorte de malédiction planant sur CSS. Impossible d'avoir un corpus CSS un tant soit peu volumineux qui ne devienne pas un sac de nœuds. Un secret bien gardé (ou peu connnu ?) dans l'univers CSS est qu'il ne faut pas *écrire* de la CSS, il faut la *construire*. Avec des règles et tout et tout :muscle:

## Un problème de spécificité

Pour comprendre pourquoi on se retrouve très facilement avec de la CSS spaghettis, étudions attentivement l'exemple suivant. On se place dans le cas où on cherche à modifier le style du titre d'un « module », une boîte de contenu texte, pour le mettre en avant. Par exemple parce que le module transmet une information urgente.

Le premier essai est simplement d'ajouter une classe sur le titre :

``` diff
# La modification.

 <div class="some-module">
-  <h2>
+  <h2 class="urgent">
     Special Header
   </h2>
 </div>

```

``` html
<!-- Le résultat. -->

<div class="some-module">
  <h2 class="urgent">
    Special Header
  </h2>
</div>
```

Mais voilà que [les ennuis commencent](http://jsbin.com/xeziqi/edit?html,css,output) :

``` css
/* La CSS associée. */

.some-module > h2 {
  /* Styles par défaut pour le titre d'un module. */
}

.urgent {
  /* Ce style ne s'appliquera en fait *jamais* ! */
}

.some-module .urgent {
  /* Ah, celui-là s'applique, cool. */
}
```

Ce code CSS à deux problèmes principaux, qui sont liés au sélecteur `.some-module > h2`.

1. Ce sélecteur définit le style de base, mais il est fortement couplé au DOM.
2. Il possède une spécificité de 11 (voir ce [calculateur de spécificité CSS](https://specificity.keegan.st/)).

Le couplage CSS-HTML est problématique car si le `<h2>` est déplacé, ou remplacé par un `<h3>` ou un `<header>`, il faudra modifier la CSS — alors même que le *rôle* du titre d'un module n'a pas changé. La spécificité haute sur la règle de base est quant elle problématique car pour surcharger ce sélecteur, il faudra se placer soit dans une spécificité de 11 et *après* (donc il faudra faire très attention à l'ordre de chargement), soit dépasser 11 : `.some-module .urgent` (20), `.some-module > h2.urgent` (21) ou encore `#special-case .urgent` (110). La haute spécificité du sélecteur de base « contamine » tout le corpus CSS, et représente une dette technique.

Ces problèmes sont très fréquents et jouent bien souvent de concert à travers des sélecteurs longs qui ont plusieurs niveaux de couplage et une très haute spécificité : `#details-pane #products-wrapper .product > img:first-child`, ce genre de choses. Tout ça devient très pénible à gérer dès lors que le corpus CSS est un peu volumineux et/ou que la spécificité moyenne des sélecteurs est élevée. Comme en général, pour s'en sortir, on ajoute des sélecteurs *encore plus spécifiques et encore plus longs* que ce qui existe déjà, le problème ne fait qu'empirer. On se retrouve alors au mieux à traîner des sélecteurs alambiqués dans le seul but d'essayer de *défaire* ou d'*outrepasser* l'existant ; au pire, plus personne ne veut toucher à la CSS.

Afin de se débarasser de ces problèmes, toutes les métholodogies modernes de CSS recommandent de suivre une règle simple : viser une complexité de la plus basse possible en utilisant un nommage sémantique basé sur des `.classes-sémantiques`. Pourquoi ?

### Génèse d'une `.classe-sémantique`

Pour améliorer notre code et pouvoir utiliser cette classe `.urgent` qui à l'air si pratique, on peut penser à se doter d'une classe `.title` pour gérer le style de titre par défaut :

``` diff
- <div class="some-module">
+ <div>
-  <h2 class="urgent">
+  <h2 class="title urgent">
     Special Header
   </h2>
 </div>
```

``` html
<div>
  <h2 class="title urgent">
    Special Header
  </h2>
</div>
```

``` css
.title {
  /* Styles par défaut (optionel). */
}

.urgent {
  /* Styles pour un titre urgent. */
}
```

À ce stade, le couplage au DOM a bel et bien été éradiqué. Contrat rempli ?

Et bien, certes, il est beaucoup plus simple et rapide de faire des modifications ; mais les classes `.title` et `.urgent` sont désormais tellement génériques — on dit « réutilisables » pour être dans le coup — que dans la pratique, elles vont encourager la composition de « micro-classes » (`title urgent`, puis `title urgent big`, puis…) pour arriver à produire le « macro-état » (le style final désiré). Cela va mécaniquement augmenter la spécificité des sélecteurs. Pas vraiment une solution satisfaisante à moyen terme.

Une classe *wrapper* semble alors tout indiquée :

``` diff
- <div>
+ <div class="module">
   <h2 class="title urgent">
     Special Header
   </h2>
 </div>
```

``` html
<div class="module">
  <h2 class="title urgent">
    Special Header
  </h2>
</div>
```

``` css
.module .title {
  /* Styles par défaut. */
}

.module .title.urgent {
  /* Styles pour un titre urgent. */
}
```

C'est mieux : le sélecteur `.module .title` permet de comprendre tout de suite à quoi on a affaire, en l'ocurrence au *titre d'un module*. Les micro-classes elles-mêmes ne sont pas vraiment très précises, mais leur combinaison permet de créer du sens. Le problème, c'est que les sélecteurs possèdent maintenant une spécificité de 20 ou plus. Les symptômes *et* la maladie sont toujours les mêmes, on a juste nettoyé la plaie.

Pour *réellement* abaisser la spécificité des sélecteurs, tout en évitant couplage et micro-classes globales, CSS ne laisse d'autre choix que d'intégrer un espace de nom explicite dans les classes elles-même :

``` diff
 <div class="module">
-  <h2 class="title urgent">
+  <h2 class="module-title urgent">
    Special Header
  </h2>
 </div>
```

``` html
<div class="module">
  <h2 class="module-title urgent">
   Special Header
 </h2>
</div>
```

``` css
.module-title {
  /* Styles par défaut. */
}

.module-title.urgent {
  /* Styles pour un titre urgent. */
}
```

Ou [en SASS](http://sass-lang.com/guide), syntaxe scss :

``` scss
.module-title {
  /* Styles par défaut. */

  &.urgent {
    /* Styles pour un titre urgent. */
  }
}
```

Le principal intérêt d'un tel « préfixe », ou *namespace*, réside dans le fait qu'il permet de transférer de CSS vers HTML la gestion de la relation entre des concepts — ici, un module et son titre. Avec un sélecteur comme `.module .title`, la CSS essayait de gérer cette relation parent-enfant en utilisant le seul outil à sa disposition : un sélecteur à spécificité haute (20). Cette technique fonctionne à court terme, mais comme on l'a vu, elle n'est pas scalable à plusieurs titres. Avec le *namespace*, le HTML peut désormais dire à un élément « comporte toi comme un titre de module ! » et coté CSS, la complexité moyenne des sélecteurs à été effectivement réduite : `.module.title` est devenu `.module-title` (20 => 10) et `.module.title.urgent` est devenu `.module-title.urgent` (30 => 20). C'est déjà beaucoup mieux !

Mais est-ce *vraiment* mieux ? Notre problème initial était de contrecarrer `.some-module > h2` (11). Au final, aussi bien la toute première solution (`.some-module .urgent`) que la dernière (`.module-title.urgent`) ont une spécificité de 20. Certes, chemin faisant, le style de base est devenu `.module-title` (11 => 10), mais manifestement quelque chose cloche.

### Notion de modificateur d'état

Le problème sous-jacent, c'est que `.urgent` est toujours un concept global qu'on peut rattacher aussi bien à un titre qu'à un paragraphe, un `<div>`, etc. Si la notion d'urgence, et le style qui y est associé, est *effectivement* quelque chose pensé et designé pour être réutilisable, pas de problème : le gain de 10 points de spécificité dans les sélecteurs du type `.foobar.urgent` sera le prix à payer. Mais bien souvent, cette flexibilité et cette réutilisabilité ne sont ni utiles, ni souhaitables. Pourquoi alors ne pas suivre notre logique jusqu'au bout ? Relions la notion d'urgence à la notion de titre d'un module, directement dans la classe CSS :

``` diff
 <div class="module">
-  <h2 class="module-title urgent">
+  <h2 class="module-title module-title-urgent">
     Special Header
   </h2>
 </div>
```

``` html
<div class="module">
  <h2 class="module-title module-title-urgent">
    Special Header
  </h2>
</div>
```

``` css
.module-title {
  /* Styles par défaut. */
}

.module-title-urgent {
  /* Styles pour un titre urgent. */
}
```

Ou en SASS (les exemples à suivre seront tous [en SASS](http://sass-lang.com/guide), syntaxe scss) :

``` scss
.module-title {
  /* Styles par défaut. */

  &-urgent {
    /* Styles pour un titre urgent. */
  }
}
```

Bien que manifestement plus verbeux, `.module-title-urgent` possède deux grandes qualités :

* une spécificité de 10
* une sémantique complète

Sa spécificité basse rend ce sélecteur très facile et fiable à surcharger, et ce depuis n'importe quel endroit du corpus CSS. Pas de dette technique, et on s'affranchit en plus de la notion d'ordre de déclaration / chargement de la CSS. Le coté sémantique tient au fait qu'en lisant `module-title-urgent`, on comprend d'emblée qu'il s'agit d'un *titre*, dans sa variante *urgente*, trouvé *dans un module* et pas ailleurs (ce n'est pas juste `title-urgent`, par exemple). Le sélecteur devient très résilient au changement, on s'affranchit de la dépendance au DOM.

> Le terme « sémantique » fait référence au fait que la classe est à la fois explicite et déclarative (j'y viens ci-après). En anglais, on pourrait dire *self-explanatory*. Je n'aime en fait pas trop « sémantique », qui est traditionnellement un concept HTML à destination des machines (parser `<article>` plutôt que `<div>`, etc.), et préfèrerais parler de *fully-qualified selector*. Mais au final, peu importe le jargon tant qu'on comprend de quoi on parle !

### Utiliser une haute spécificité à bon escient

Cette technique de nommage basé sur des *namespaces* explicites règle les problèmes de couplage et de spécificité mis en évidence au départ. Mais en plus de ça, à partir du moment où la plupart des sélecteurs du corpus CSS ont une spécificité basse, il devient beaucoup plus simple de gérer les cas particuliers. Par exemple, l'utilisation d'un *wrapper* n'est pas du tout proscrite, à condition qu'il exprime l'intention de gérer un tel cas particulier :

``` html
<!-- J'adopte à partir d'ici une convention consistant à utiliser deux espaces entre les noms
     de classes CSS, pour améliorer la lisibilité. -->
<div class="module  module-special">
  <h2 class="module-title">
    Special Header
  </h2>
</div>
```

``` scss
.module-title { … }

.module-special {
  .module-title {
    /* Styles pour un titre de module spécial. */
  }
}
```

Il est particulièrement intéressant de constater que cette architecture à spécificité basse permet de facilement gérer le cas d'un module « spécial » muni d'un titre « urgent », sans augmenter la spécificité des sélecteurs :

``` html
<div class="module  module-special">
  <h2 class="module-title  module-title-urgent">
    Special Header
  </h2>
</div>
```

``` scss
.module-special {
  .module-title-urgent {
    /* … */
  }
}
```

Le code CSS requis est très simple, et on *sait* que tout va bien se passer et s'appliquer correctement, car tout a été construit en ce sens.

Selon nos besoins, on pourrait tout aussi bien imaginer relier la notion d'urgence, non pas au titre d'un module, mais au module lui-même :

``` html
<div class="module  module-urgent">
  <h2 class="module-title">
    Special Header
  </h2>
</div>
```

``` scss
/* Module -- styles de base. */
.module { … }
.module-title { … }

/* Module -- variante Urgente. */
.module-urgent {
  .module-title { … }
}
```

> En résumé : plus on est précis dans son nommage pour les règles de base, moins les sélecteurs sont spécifiques (en valeur numérique), et moins on a de question à se poser et de travail à effectuer.

## OOCSS

Si vous avez un peu suivi l'actualité de CSS, tout ça devrait vous rappeler un mot-clé en vogue : OOCSS (*Object Oriented CSS*). On parle en effet beaucoup de cette CSS « orientée objet », malheureusement sans forcément comprendre ce dont il s'agit.

La programmation orientée objet (POO) est un paradigme de programmation où l'unité de base, l'objet, permet de mettre en place une architecture de code basée sur quatre grands piliers :

1. abstraction
2. encapsulation
3. héritage
4. polymorphisme

Des concepts plutôt abscons, qui paraissent bien éloignés de CSS, mais qui sont en fait plutôt simples et qui peuvent, dans une certaine mesure, être appliqués à CSS pour se simplifier la vie au quotidien. Voyons comment, en partant d'un exemple concret qui n'a rien à voir avec CSS.

* On souhaite représenter la notion très générale de « forme géométrique » dans un programme.
* On trouvera des carrés, des cercles, etc.
* Chaque forme doit avoir une couleur.
* Chaque forme doit pouvoir dire combien de coté(s) elle a.
* Chaque forme doit pouvoir calculer son aire.

Dans un langage de POO comme Ruby, cela donnerait :

``` ruby
class Shape
  attr_accessor :color
  attr_reader :sides

  def initialize(color = :transparent)
    @color = color
    @sides = nil
  end
end

class Square < Shape
  attr_accessor :width, :height

  def initialize(width, height, *args)
    @sides = 4
    @width = width
    @height = height
    super(*args)
  end

  def area
    width * height
  end
end

class Circle < Shape
  attr_accessor :radius

  def initialize(radius, *args)
    @sides = 1
    @radius = radius
    super(*args)
  end

  def area
    2 * Math::PI * radius
  end
end
```

Une fois cette implémentation en place, elle nous donne accès à [une API](https://fr.wikipedia.org/wiki/Interface_de_programmation), une interface « orientée objet » pour manipuler des formes géométriques :

``` ruby
square1 = Square.new(10, 10, :red)
square2 = Square.new(2, 4, :blue)

square1.color # :red
square1.sides # 4
square1.area  # 100

square2.color # :blue
square2.sides # 4
square2.area  # 8

a_circle = Circle.new(5)
a_circle.color # :transparent
a_circle.sides # 1
a_circle.area # 31.42
a_circle.radius=(10)
a_circle.area # 62.83
```

Revisitons les quatres concepts de la POO à la lumière de ce code :

1. Abstraction : il est sans doute possible de donner une définition mathématique du concept de forme géométrique, mais pour nous autres programmeurs chargés de résoudre un problème précis, une forme géométrique (`Shape`) sera quelque chose qui possède une couleur (`@color`), un nombre de cotés (`@sides`) et qui est capable de calculer son aire (`area`). Point à la ligne. Le terme « abstraction » est à prendre dans le sens de « simplification » — simplification du réel, s'entend. On pourrait ainsi se doter d'abstractions `Car`, `User`, `ForumTopic`, etc. selon nos besoins.
2. Encapsulation : `square1` est une « instance » de l'abstraction `Square` ; `square2` également. Ces deux instances possèdent des caractéristiques communes, qu'elles tirent de l'abstraction dont elle sont issues (par exemple, en tant qu'instances de `Square`, aussi bien `square1` que `square2` sont capables de calculer leur aire). Mais elles ont aussi un état propre : `square1` a une hauteur différente de `square2`, etc. Quand on parle d'encapsulation, on parle à la fois d'état interne, et d'interface pour gérer cet état, le tout au niveau d'une instance. L'encapsulation, c'est l'idée que chaque objet, c'est-à-dire chaque instance d'une abstraction, se comporte tel un individu particulier, avec ses propres caractéristiques (son état), et ses propres moyens d'actions (son interface, dont tout ou partie peut-être tirée de l'abstraction à laquelle l'instance appartient). Cette architecture de code est très pratique, car une instance devient alors un petit soldat dans un corps d'armée, tous les objets collaborant pour échanger et modifier des données selon les règles définies par le programmeur, dans le but de résoudre des problèmes. Un objet à un nom (`a_circle`), un état (`@color`, `@radius`, …), et on peut lui donner des ordres (`a_circle.radius=(10)` => change ton rayon !) ou lui poser des questions (`a_circle.radius` => quelle est ton aire ?).
3. Héritage : `Square` et `Circle` sont deux cas particuliers de la notion générale de forme géométrique, `Shape`. À ce titre, ils ont tout aussi bien des traits commun (une couleur…) que des spécificités (un nombre de coté différent, une façon de calculer leur aire différente…). L'héritage permet de « factoriser » ce qui est commun entre plusieurs abstractions. La façon de gérer cet héritage d'état et d'interface dépend du langage. En Ruby, par exemple, l'héritage est géré au niveau des catégories d'objets (`class`), tandis qu'en JavaScript, des objets spécialisés sont utilisés pour en relier d'autres entre-eux (héritage prototypal). CSS aussi possède sa propre logique d'héritage, un mix de *cascade* (le *C* de CSS) et de règles de spécificités.
4. Polymorphisme : quand plusieurs abstractions partagent tout ou partie d'une interface, on parle de polymorphisme — littéralement, plusieurs formes. L'idée est que des objets appartenant à des abstractions différentes (un carré n'est pas un cercle…) sont capables de réagir à des instructions identiques : `a_square.area` et `a_circle.area`. Cette capacité à créer des interfaces qui vont jouer le rôle de « méta-abstraction » est important, car il permet de se découpler de la connaissance fine de ce qu'on manipule. À partir du moment où je sais qu'on me donne une forme géométrique, n'importe laquelle, je serai en mesure de calculer son aire. L'objet peut bien avoir plusieurs formes, être un carré, un cercle ou autre chose encore, peu importe : parce que l'interface est commune, je n'ai plus à me soucier de savoir comment faire, ni comment demander — calculer l'aire, ce sera toujours `.area`.

Ces concepts fondamentaux de POO sont très utiles pour concevoir des programmes complexes, résilients et maintenables. CSS n'a pas de notion d'objet, de classe, d'état ou d'interface, mais en fait, ces mêmes concepts y existent à l'état latent et ne demandent qu'à être structurés.

### Le cas Bootstrap

On connaît tous l'impératif, ce mode grammatical qui sert à exprimer une injonction, à donner un ordre : « mange ta soupe ! » Si on veut qu'un ordre soit suivi d'effet, mieux vaut être spécifique, non ?

Bootstrap est une librairie présentée comme OOCSS, qui met à disposition un ensemble de classes CSS qu'on va parfois devoir mixer pour graduellement changer le look 'n feel d'un élément, jusqu'à ce qu'il ressemble à ce qu'on souhaite. Prenons un exemple de code HTML qui utilise Bootstrap :

``` html
<div class="container">
  <div class="row">
    <section class="content">
      <h1>Table Filter</h1>
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="pull-right">
              <div class="btn-group">
                <button type="button" class="btn btn-success btn-filter" data-target="pagado">Pagado</button>
                <button type="button" class="btn btn-warning btn-filter" data-target="pendiente">Pendiente</button>
                // …
```

Pour faire le lien avec la POO, Bootstrap considère qu'un nœud du DOM (un `<div>`, etc.) fait office d'objet, que son état est le style visuel final désiré, et que l'interface de l'objet correspond à l'ensemble des classes impératives comme `row` ou `pull-right`.

C'est une approche intéressante, notamment parce qu'elle est très simple à prendre en main et permet de prototyper rapidement. Mais cet exemple démontre avant tout pour moi l'absence d'un modèle de pensée cohérent dans Bootstrap, et explique pourquoi Bootstrap représente une grosse dette technique sur bien des projets.

Certaines classes de Bootstrap sont impératives par elles-même (`pull-right`), d'autres le sont par composition transverse (`container`, `row` et `col-*`) ou composition directe (`btn` et `btn btn-success`), tandis qu'enfin d'autres « sonnent » sémantiques mais doivent quand même être associées à des micro-classes impératives généralistes (`panel-body` précisé par `pull-right`). Cette composition de classes dans tous les sens et à plusieurs niveaux d'abstractions phagocyte peu à peu tout le DOM, et représente un terrain ultra-favorable aux problèmes de spécificités CSS. En outre, les tentatives sémantiques comme `panel` ne sont qu'une illusion : un tel `panel` Bootstrap a été conçu pour être réutilisable, donc générique, de sorte que *by design*, il va être nécessaire de faire de la composition de classes et/ou de la surcharge agressive pour personnaliser le design. Bootstrap est efficace et résilient à partir du moment où on l'utilise pour ce qu'il a été pensé : une bibliothèque de styles *figée*. Au-delà, danger.

Plus fondamentablement, Bootstrap essaye d'englober sous une même logique POO deux technologies aux responsabilités fondamentalement orthogonales : HTML (structure) et CSS (présentation). Pour ce faire, il extrait un maximum de chose de CSS pour les exposer dans des micro-classes impératives, ce qui aboutit à un fort couplage, pour notre plus grand déplaisir. À la clé, un code pas maintenable, pas scalable et qui se met en travers du chemin des évolutions stylistiques et structurelles.

Dans ces conditions, est-il réaliste, possible voire souhaitable d'appliquer un modèle dit « orienté objet » à CSS et HTML ? La réponse à toutes ces questions est **oui**, mais en abandonnant toute démarche impérative. En clair, en laissant CSS bosser tranquille.

## Une entreprise CSS qui roule

Le duo HTML/CSS a été pensé pour se faire une *déclaration* d'amour permanente, pas pour s'échanger une liste de course au détail. CSS, en particulier, saura se montrer le plus performant si on lui tient un langage hautement déclaratif, qui lui laisse un maximum de marge de manœuvre. Et il se trouve — oh comme la nature est bien faite ! — que CSS possède une architecture tout à fait adaptée à la programmation déclarative, la *cascade*.

> Si la différence entre impératif et déclaratif n'est pas claire, [voici un bon résumé](http://fr.slideshare.net/netguru/programming-paradigms-which-one-is-the-best).

On souhaite obtenir une CSS scalable, facilement extensible, à laquelle on puisse apporter des modifications en toute confiance. Pour ça, il faut que le corpus CSS reste simple, donc que les sélecteurs aient une spécificité moyenne basse. La meilleur manière de faire ça, on l'a vu, c'est de mettre en place des *namespaces*. Ils vont agir comme des **abstractions**, minimisant le couplage entre CSS et HTML. Et de ce premier concept d'abstraction, découlent tous les autres.

À partir d'une abstraction donnée (`button`), sur un projet qui grossit, on cherchera inévitablement à déployer des variations. Tout le sel de l'intégration CSS tient en effet à la gestion de cas particuliers : « ce bouton-ci est actif », « celui-là est désactivé », etc. Chaque variation sera telle une **instance** spécifique d'une abstraction commune, et en tant qu'instance, elle aura un état et une interface. En OOCSS, état et interface sont une seule et même chose, un nom de classe : `button-active`.

On mettra également à profit une forme d'**héritage** un peu particulière intégrée nativement à CSS, la *cascade*, pour rester [DRY](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas) : `button button-active`, `button button-disabled`.

Et dans les quelques cas particuliers où état et interface gagneraient à être séparés, de sorte qu'un style profite au plus grand nombre, le **polymorphisme** pourra être implémenté par des micro-classes globales : `button hidden` et `link hidden`.

### Les employés, tous syndiqués chez BEM

Tout ça, c'est très bien, mais encore faut-il l'appliquer, et sur la durée. Rien de mieux pour ça que de se doter de quelques règles simples et efficaces. En CSS, tout passe par le nom des classes, donc ces règles seront avant tout liées à une convention de nommage. L'une d'entre elles s'appelle BEM : *Bloc, Element, Modifier*. Elle est populaire car simple, efficace, et de surcroît pas *trop* contraignante (elle l'est forcément un peu !)

> BEM is a development methodology that allows team members to collaborate and communicate ideas using the unified language consisting of simple yet powerful terms: blocks, elements, modifiers. It is a collection of ideas and methods. Each company and each team may integrate it into an existing workflow gradually, finding out what works best for them.

L'idée de BEM, c'est de faire de l'OOCSS, mais de remplacer la terminologie classique de la POO (abstraction / encapsulatruc / blablabla / *boooring!* ) par quelque chose qui colle plus à l'interface utilisateur (UI). Pour plus de détails, [voir cet article](https://philipwalton.com/articles/side-effects-in-css/) qui explique « pourquoi BEM ? ».

* Un bloc (*block*) représente un objet de l'UI : un formulaire de login, un menu, un profil utilisateur…
* Un élément (*element*) est un composant interne d'un bloc : un bouton de validation *du formulaire*, un item *du menu*, la preview de l'avatar *de l'utilisateur*…)
* Un modificateur (*modifier*) représente une variation possible d'un bloc ou d'un élément : le formulaire de login, mais *replié* ; un item *actif* du menu ; le menu, mais en version *condensé pour téléphones portables*…

Ces relations sont matérialisées au travers d'une syntaxe hautement déclarative, avec des *fully-qualified selectors* (en gros, des classes sémantiques). La syntaxe officielle est la suivante :

``` css
.human-being {}                 /* block */
.human-being--female {}         /* block--modifier */
.human-being__hand {}           /* block__element */
.human-being__hand--left {}     /* block__element--modifier */

/* Mais aussi : */
.human-being--female .human-being__hand {}         /* block--modifier  block__element */
.human-being--female .human-being__hand--left {}   /* block--modifier  block__element--modifier */
```

On peut tout à fait envisager une autre convention, tout en gardant la même architecture :

``` css
.humanBeing {}
.humanBeing--female {}
.humanBeing-hand {}
.humanBeing-hand--left {}
.humanBeing--female .humanBeing-hand {}
.humanBeing--female .humanBeing-hand--left {}
```

Avec BEM, on obtient un code HTML purement déclaratif vis-à-vis de CSS :

``` html
<form class="site-search  site-search--full">
  <input type="text" class="site-search__field">
  <input type="Submit" value ="Search" class="site-search__button">
</form>
```

Pour plus d'exemples, voir [ici](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/), [là](http://getbem.com/introduction/) et bien sûr [sur le site officiel](https://en.bem.info/methodology/naming-convention/).

On retrouve ainsi les concepts fondamentaux de la POO, mais appliqués à une interface utilisateur. Ces principes nous permettent de construire une CSS fiable, résiliente et facile à modifier. Contrairement à Bootstrap qui travaillait contre CSS, BEM cherche à remettre l'intégrateur en position de responsabilité. Vous allez pouvoir décider de l'organisation interne la plus adéquate, en fonction de ce que déclare être un élément du DOM, tel que `.site-search__button`. Envie d'utiliser uniquement de la CSS pur jus ? Chaque classe sera alors un assemblage « impératif » de règles CSS. Plutôt versé dans les préprocesseurs comme Sass ou Less ? Vous pourrez mettre à profit variables, mixins et autres *extend* pour éviter de la duplication et composer vos classes haut-niveau à partir de briques réutilisables.

> En utilisant la convention officielle, voilà ce que deviendraient certains des sélecteurs pour le module et son titre :
> 
> | Avant                                  | Après                                     |
> |----------------------------------------|-------------------------------------------|
> | `.module-title`                        | `.module__title`                          |
> | `.module-title-urgent`                 | `.module__title--urgent`                  |
> | `.module-special .module-title`        | `.module--special .module__title`         |
> | `.module-special .module-title-urgent` | `.module--special .module__title--urgent` |


## En résumé

La logique un peu verbeuse de BEM découle d'une analyse fine des problèmes et solutions habituellement rencontrés en CSS. Cette convention permet de construire pro-activement un corpus CSS :

* maintenable
* compréhensible
* facile à modifier et à étendre
* performant
* dépourvu de code inutile
* gèrant bien le *responsive*

Vu comme ça, peu importe si le nom des classes est un peu long, l'[auto-complétion](https://fr.wikipedia.org/wiki/Compl%C3%A8tement) et les pré-processeurs sont là pour ça !
