Serveur
=======

Cette section traite de l'installation et de l'administration d'un serveur Web.

Pour comprendre le fonctionnement d'un serveur Web, en savoir plus sur Apache, Nginx, etc. consultez la [section dédiée au modèle client/serveur](../culture-dev/client-serveur).
