# Installation serveur ubutnu

## Installation d'apache : 

`sudo apt-get install apache2`

Redirection d'url 

`sudo a2enmod rewrite`

Dans /etc/apache2/apache2.conf

```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

## Mysql 

`sudo apt-get install mysql-server`

Autoriser la connexion avec root si besoin

https://stackoverflow.com/questions/41645309/mysql-error-access-denied-for-user-rootlocalhost

```
sudo mysql    
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
```

identifiant et mot de passe : root

## Installation de PHP/PDO

`sudo apt-get install php-mysql`

## Phpmyadmin

`sudo apt-get install phpmyadmin`

Suivre les étapes

Créer un lien symbolique sur le serveur

`sudo ln -s /usr/share/phpmyadmin /var/www/html`

## Modules php

Manipulation d'images

`sudo apt-get install php-gd`

Nécessaire pour symfony

`sudo apt-get install php-xml`

## Composer

Prérequis 

`sudo apt-get install zip unzip`

https://getcomposer.org/download/

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

Puis `mv composer.phar /usr/local/bin/composer`

## Gestion des droits pour symfony

`sudo apt-get install acl`

## Installation de git 

`sudo apt-get install git`

Changement nom

```
git config --global user.name NOM
```

Changement e-mail

```
git config --global user.email "email@email.com"
```

## Installation de node avec NVM 

https://github.com/creationix/nvm

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

ou

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

Relancer le terminal et  `nvm install node` pour installer la dernière version

## Installation de yarn 

https://yarnpkg.com/lang/en/docs/install/#debian-stable

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
```

```bash
sudo apt-get update && sudo apt-get install yarn
```


