## Protocole

![Architecture client/serveur sur Internet](../img/architecture-internet-clientserveur.png)

---

Pour schématiser on peut se poser ces 3 questions :

- Comment communiquer ? => TCP
- Vers quelle destination ? => IP
- Avec quelle _grammaire_ ? => Nom du protocole
- Pour quel besoin ? => Programme associé

### Couche de transport

Dans le cadre d'échange d'informations sur les réseaux, la couche de transport se charge de transmettre les données d'un système vers un autre.

La couche communément utilisée est Transmission Control Protocol (« protocole de contrôle de transmissions »), abrégé **TCP**. Sur Internet, on parle de TCP/IP.

### Adresse IP, port

Une [adresse IP](https://fr.wikipedia.org/wiki/Adresse_IP) est un numéro d'identification attribué de façon permanente ou provisoire à chaque périphérique relié à un réseau informatique (et qui utilise l'*Internet Protocol*). L'adresse IP est à la base du système d'acheminement (routage) des paquets de données sur Internet.


### Couche application

C'est dans la couche application **que se situent la plupart des programmes réseau**.

Ces programmes et les protocoles qu'ils utilisent incluent HTTP (Web), FTP (transfert de fichiers), SMTP (messagerie), SSH (connexion à distance sécurisée), DNS (recherche de correspondance entre noms et adresses IP) et beaucoup d'autres.

- TELNET, FTP, SMTP, HTTP, SSH, DNS, etc. (voir [Wikipédia](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet))
- En ce qui nous concerne, le protocole DNS (Domain Name System) **permet de traduire les noms de domaine en adresses IP**.

#### FAI

Le Fournisseur d'Accès à Internet (FAI) permet de relier un utilisateur au réseau Internet. Toutes les communications qui sortent ou entrent dans votre machine passent d'abord par votre FAI.
