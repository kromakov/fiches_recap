Culture dev
===========

Évoluer dans le monde de la programmation, ce n'est pas que coder !

Gestion des relations humaines, connaissances transversales, humour de _branch_, éléments historiques… tous ces éléments contribuent à une bonne insertion dans le monde _tech_.

## Programmation 

* [Les bases de la programmation](./les-bases.md)
* [Les paradigmes de programmation](./paradigmes)
* [À propos des conventions de nommage](./case-styles.md)

## Architecture

* [Le modèle client/serveur](./client-serveur)
* [Le protocole](./internet/protocole.md)

## Histoire, Culture

* [Bref historique d'Internet](./internet/historique.md)