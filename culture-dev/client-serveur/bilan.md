Avantages & inconvénients du modèle client/serveur
==================================================

## Avantages

- _Un_ serveur représente une autorité de connaissance et de prise de décision (_Single Source of Truth_)
  - _Le_ serveur peut en réalité être constitué de plusieurs machines, qu'il faudra coordonneer, mais du point de vue des clients, il n'y a qu'un seul serveur qui réagit de façon prédictible et fiable
  - => Modèle simple et efficace
- La centralisation des responsabilités :
  - facilite la mise en place et le contrôle de ces responsabilités (sécurité)
  - peut faciler la gestion du cache, des sauvegardes, etc.
  - peut faciliter la montée à l'échelle (_scaling_)

Techniquement parlant, le modèle client/serveur :

- encourage le partage des données par centralisation
- permet d'intégrer différents services sous une même bannière, le serveur
- facilite le découplage entre les services, communs pour tous le clients, et les plateformes particulières de ces clients, grâce à l'utilisation de protocole normés
- encourage l'interopérabilité entre les données
- tend à simplifier la maintenance des systèmes informatiques
- tend à simplifier la _gestion_ de la sécurité (à défaut d'augmenter la sécurité en tant que telle)
- est un choix raisonnable (facile à comprendre, à implémenter)

## Inconvénients

- _Un_ serveur peut être amené à gérer beaucoup de travail et de responsabilités (trop ? _Single Point of Failure_)
  - _Le_ serveur peut en réalité être constitué de plusieurs machines, qu'il faudra coordonner (pas simple !)
  - => Modèle simple mais pas toujours efficace
- La centralisation des responsabilités :
  - représente un point de faiblesse (faille de sécurité, indisponibilité du serveur)
  - peut paradoxalement complexifier la gestion du cache, des sauvegardes, etc.
  - peut paradoxalement complexifier la montée à l'échelle (_scaling_)

Techniquement parlant, le modèle client/serveur :

- participe de la congestion des réseaux
- n'est pas résilient
- n'est pas toujours performant

## Concepts associés

- L'architecture [trois tiers](https://fr.wikipedia.org/wiki/Architecture_trois_tiers)
- Alternative au modèle client/serveur : [P2P](https://fr.wikipedia.org/wiki/Pair_%C3%A0_pair)
- Le modèle [maître/esclave](https://fr.wikipedia.org/wiki/Ma%C3%AEtre-esclave) (renversement du client/serveur)
