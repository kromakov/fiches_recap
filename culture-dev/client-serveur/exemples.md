Exemples d'utilisation du modèle client/serveur
===============================================

## Dans la vraie vie

- Un client au guichet de la Poste
- Un client dans un restaurant (« _hey, serveur !_ »)
- Un enfant qui pose une question à ses parents, puis une autre, puis une autre, puis…

## En informatique

- Langages de requêtes en base de données (SQL)
- Programme utilisant une fonction pour en récupérer la valeur de retour
- Navigateur internet et serveur web
