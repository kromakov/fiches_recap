Paradigmes de programmation
===========================

_À la découverte des grands paradigmes de programmation._

## Vue d'ensemble

Il existe de nombreux paradigmes, voici les plus utilisés :

- [impératif](./impératif)
  - procédural
  - [orienté objet](./orienté-objet)
- [déclaratif](./déclaratif)
  - [fonctionnel](./fonctionnel)
  - logique
  - par contrat
  - par flux
  - événementiel

De nombreux langages sont dits « [multi-paradigmatiques](./multi-paradigmes) » et offrent tout ou partie des avantages (et parfois des inconvénients…) des paradigmes purs.

## Pour aller plus loin…

* https://en.wikipedia.org/wiki/Programming_paradigm
* https://www.info.ucl.ac.be/~pvr/jussieuParadigms2008.pdf (avec beaucoup de _fôtes d'ortaugrafe_)
