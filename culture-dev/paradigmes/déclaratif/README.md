Paradigme déclaratif
====================

* Avantages : naturel, optimisé, élégant, facilement testable, prédictible
* Inconvénients : possiblement limité, difficile à prendre en main, gestion des effets de bord délicate

## Définition

La programmation déclarative consiste à porter son attention sur le résultat à atteindre, et non sur le cheminement logique pour y arriver. Le but : être concis et efficace, déléguer le gros du travail à la machine !

> Concrètement, la programmation déclarative s'appuie nécessairement sur une implémentation [impérative](../impératif), laquelle est « simplement » cachée derrière une [interface](https://fr.wikipedia.org/wiki/Interface_de_programmation) : une fonction, une classe, un socket, un _endpoint_ HTTP, etc.

Un langage déclaratif offre en générale une syntaxe plus élégante et plus adaptée à l'expression d'un besoin spécifique, comparativement à un langage impératif — mais au prix d'une moindre liberté. Ce constat est toutefois à nuancer, car le paradigme [fonctionnel](./fonctionnel), une variante du déclaratif, permet de créer « à la volée » des portions de programnes (voire des programmes entiers) déclaratifs, sans contraintes particulières sur la forme syntaxique.

## Exemple

``` css
/* CSS */
.important {
  color: red;
}
```

``` html
<!-- HTML -->
<p>Une phrase est déclarative par nature.</p>
<p>Même quand elle donne un ordre : soyez-en sûr !</p>
```

``` puppet
# Puppet config file
file { 'nginx.conf':
  ensure  => present,
  path    => '/etc/nginx/nginx.conf',
  source  => [
      "puppet:///modules/site/nginx.conf--${::fqdn}",
      "puppet:///modules/site/nginx.conf" ],
}
```