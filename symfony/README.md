# Symfony

## Site officiel :

http://symfony.com/

**La documentation officielle de Symfony est une mine d'or**, très bien faite et sur laquelle vous pouvez compter comme ressource principale pour votre apprentissage, que vous soyez novice ou expert en Symfony.

## Programme de la spé Symfony O'Clock

- [Le programme de la spé](programme.md)

### Introduction

- [A propos de Symfony, installation, Composer, architecture](themes/symfony-basics.md)
- [Le système de Bundle](themes/bundles.md)

### Routing et contrôleurs

- [Requête, front controller, routes, méthodes de contrôleurs, réponse](themes/core-routing-controllers.md)
- [Cookies, Session et FlashMessages](themes/session.md)
- [Exposer les routes en JS avec FOSJsRoutingBundle](http://symfony.com/doc/current/bundles/FOSJsRoutingBundle/index.html)

### Twig

- [Twig avec Symfony](themes/twig.md)
- [Personnaliser les pages d'erreur](http://symfony.com/doc/3.4/controller/error_pages.html)
- [Intégration de code JavaScript dans la vue](themes/twig-js.md)
- [Personnaliser les thèmes de formulaires ou de template CRUD avec Twig](themes/twig-override.md)

### JSON

- [JSON avec Symfony](themes/json.md)

### Bases de données avec Doctrine

- [Créer, utiliser la ligne de commande doctrine, sauvegarder, récupérer, mettre à jour, supprimer, architecture](themes/S2J1-Doctrine.md)
- [Associations avec Doctrine](themes/S2J2-Doctrine.md)
- [Doctrine Query Language (DQL), Doctrine QueryBuilder](themes/S2J1-Doctrine.md#dautres-façons-daccéder-aux-données)
- [Données de test, données factices avec Doctrine et Faker](themes/fixturesbundle-faker.md)

### Formulaires

- [Les formulaires (base)](themes/forms.md)
- [Les formulaires avec Doctrine](themes/forms-doctrine.md)

### Sécurité

- [Configurer le composant Security depuis une entité User](themes/security-user.md)
- [Condensé sur les Rôles](themes/roles.md)

### Services

- [Création d'un service](themes/services.md)

### Commandes Console

- [Création d'une commande console](themes/command.md)

### Upload de fichier avec et sans Doctrine

- [Upload lié à une entité Doctrine](themes/upload.md)
- [Améliorer la gestion des fichiers uploadés](themes/upload-more.md)

### Manipulation d'images avec LiipImagineBundle

- [LiipImagineBundle](themes/liip-imagine-bundle.md)

### Gestion des erreurs avec Symfony et PHP

- [Résoudre les erreurs](themes/errors.md)

### Tests

- [Tests unitaires](themes/tests-unit.md)
- [Tests fonctionnels](themes/tests.md)

### Déploiement sur serveur Gandi

- [Connexion en SSH, git, mysql, gd, phpmyadmin, composer, symfony, debug, cache](themes/deploiement.md)

### Symfony Démo : l'appli de démo tout-en-un !

**Appli faite pour apprendre Symfony !**

- [Introduction](https://symfony.com/blog/introducing-the-symfony-demo-application)
- [Repo github](https://github.com/symfony/symfony-demo)

## Liens utiles

### Symfony
+ [Book](http://symfony.com/doc/current)
+ [Pré-requis](http://symfony.com/doc/current/reference/requirements.html)
+ [Install Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
+ [Installation](http://symfony.com/doc/current/best_practices/creating-the-project.html)
+ [Getting Started](http://symfony.com/doc/current/) (Controller, Session, Flash, Request, Response, Twig)
+ [Best practices](http://symfony.com/doc/current/best_practices/)

### Twig
- [Voir la référence (bas de page)](https://twig.sensiolabs.org/doc/2.x/)
- [Aperçu global des fonctionnalités](https://twig.sensiolabs.org/doc/2.x/templates.html)
