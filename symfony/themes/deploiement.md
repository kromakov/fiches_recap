# Déployer une application Symfony sur votre hébergement

## Note

Connectez-vous au serveur via `ssh user@x.x.x.x` (IP du serveur).

Toutes les commandes s'effectuent en `root` (faire `su` + pass user), _sauf_ les commandes propres à notre appli (`composer install`, `php bin/console`, etc.).

## Installation des programmes nécessaires

Le serveur qui vous a été fourni par O'clock pour mettre en ligne vos sites est un serveur "nu", c'est-à-dire sans interface graphique par défaut, il vous faut donc installer serveurs, librairies, extensions vous-même en ligne de commande.

Une fois connecté en SSH, nous allons installer git, mysql, la libraire GD pour PHP ainsi que plusieurs modules PHP, phpmyadmin, composer, puis configurer Symfony, activer le debug, mettre à jour le cache. Enfin, nous créerons un VirtualHost sous Apache pour faire pointer un sous-domaine de notre nom de domaine vers un répertoire précis de notre application.

> Avant tout, nous devons vérifier que le gestionnaire de paquets `apt` connaît l'existence du dépôt _universe_, qui contient la liste des packages le plus répandus :

> Avec root, afficher le fichier des _sources_ via `nano /etc/apt/sources.list.d/sources.list` et y ajouter ça (permet l'accès aux dépôt de debian stretch et d'autres dépôts de la communauté) :

```
deb http://httpredir.debian.org/debian stretch main contrib non-free
deb-src http://httpredir.debian.org/debian stretch main contrib non-free
deb http://httpredir.debian.org/debian stretch-updates main contrib non-free
deb-src http://httpredir.debian.org/debian stretch-updates main contrib non-free
```

> Puis faire `apt update` afin de mettre à jour la liste des dépôts.

git
---
`apt install git`

mysql
-----
`apt install mysql-server`

Si PHP non installé
-------------------
`php -v` => indique la version si installé
`apt install php`

module mysql pour PHP
---------------------
`apt install php-mysql`

Cela va installer MariaDB (remplacement libre de mySQL suite à rachat par Oracle). Un mot de passe root sera demandé, le saisir et le noter. Ensuite on va se connecter à MariaDB via :

- `mysql -u root -p` (user root et demande de passe)
- Puis créer une base et utilisateur dans l'interface :
```
CREATE DATABASE nom_base; -- crée la base
CREATE user "nom_utilisateur"@"localhost"; -- crée le user
SET password FOR "nom_utilisateur"@"localhost" = password("mot_de_passe"); -- ajoute un mot de passe au user
GRANT ALL ON nom_base.* TO "nom_utilisateur"@"localhost"; -- donne les droits au user sur la base
```

> Vous poivez installer et utiliser phpMyAdmin avec le user root créé à l'install si vous ne souhaitez pas le faire à la main ici.

module graphique GD pour PHP
-------------------------------

Permet la manipulation de fichiers images sur le serveur.

`apt install php-gd`

module simpleXml pour PHP
-------------------------

Composer et Symfony auront besoin de ce module.

`apt install php-xml`

phpMyAdmin (optionnel)
----------
- `apt install phpmyadmin`

Configurer phpmyadmin avec un user root.

Pour configurer PMA à l'URL de votre serveur `/phpmyadmin`, éditez le fichier `/etc/apache2/apache2.conf` et ajoutez à la fin `Include /etc/phpmyadmin.apache.conf`

Redémarrez Apache `service apache2 restart`.

Configurer le cas échéant un utilisateur et une base de données pour accueillir le projet.

#### Alternative simple à PMA

En cas de souci d'install ou d'utilisation de phpMyadmin, on placera https://www.adminer.org/ à la racine du dossier web (attention à na pas laisser ce fichier présent en production !). **Adminer est une alternative simple et puissante à PMA**.

Zip et unzip
------------
On installera également `zip` et `unzip` afin de povoir télécharger et décompresser les archives via _composer_ :
- `apt install zip unzip`

Composer
--------
Voir la fiche récap' : [Installer Composer](https://getcomposer.org/download/)

Pour utiliser `composer` il faut le placer dans le dossier `bin`, 2 options :
- Soit via `php composer-setup.php --install-dir=bin --filename=composer` _avant l'install complète (2 lignes seulement depuis le lien ci-dessus)_
- Soit en déplaçant l'archive composer.phar `mv composer.phar /bin/composer`

Symfony
-------
Faire un `git clone` de votre dépôt depuis le dossier `/var/www/html/`, puis `composer install` depuis le dossier de votre projet cloné, pour installer les vendors, dont Symfony.
Modifier app_dev.php le temps de tester la mise en place du site. Vous pouvez pour cela commenter le gros bloc `if` qui correspond à la sécurité du app_dev.php, ou bien ajouter votre adresse IP dans ce même bloc afin de pouvoir y accéder en production.

Gestion des droits d'écriture du dossier `var/` de Symfony
----------------------------------------------------------
Le chmod ne sera pas suffisant ici pour s'en sortir, car 2 utilisateurs Linux vont devoir écrire dans le répertoire `var/` : ww`w-data`, qui correspond à Apache et vous, quand vous exécuterez des lignes de commande.

Nous devons d'abord installer un gestionnaire de droits plus poussés, ACL :
- `apt install acl`

Exemple avec un nom d'utilisateur `dario`, depuis la racine de votre projet Symfony :
```
setfacl -dR -m u:www-data:rwX -m u:admin:rwX var
setfacl -R -m u:www-data:rwX -m u:admin:rwX var
```
Vous devez bien sûr remplacer `admin` par votre nom d'utilisateur (celui du _login ssh_).

[En savoir plus](http://symfony.com/doc/3.4/setup/file_permissions.html)

Activer la réécriture d'URL
---------------------------

Elle permet d'indiquer des motifs de comportement concernant l'URL, Symfony s'en sert notamment pour renvoyer les paramètres de requête vers le front controller.

`a2enmod rewrite` puis redémarrer Apache `sudo service apache2 restart`.

Activer .htaccess
-----------------

Les fichiers `.htaccess` permettent de créer des configurations locales d'Apache, depuis le répertoire où ils se trouvent. Pour activer htaccess partout dans www, éditer votr ficiher de configuration Apache :

`sudo nano /etc/apache2/apache2.conf` et remplacez `AllowOverride None` par `AllowOverride All`
```
<Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```

Redémarrez Apache `service apache2 restart`.

Création d'un sous-domaine
--------------------------
Depuis l'interface web Gandi, cliquez sur l'engrenage au niveau du domaine concerné, puis ajouter un sous-domaine en bas de page dans la zone indiquée, par ex. `blog.votredomaine.com`. **Il faudra attendre de quelques heures jusqu'à 24 heures que la propagation des DNS se fasse**, selon l'hébergeur et le FAI.

Apache VirtualHost
------------------
Afin que le nom de domaine soit pris en charge par le serveur web Apache, il faut modifier le fichier `/etc/apache2/sites-available/000-default.conf` et y ajouter les lignes suivantes :

```
# S'assure qu'Apache écoute sur le port 80
Listen 80

# Ecoute les virtual host sur toutes les adresses IP (*)
NameVirtualHost *:80

<VirtualHost *:80>
    # Chemin vers le répertoire web de votre projet Symfony
    DocumentRoot /var/www/html/votre_dossier/web
    # Votre domaine, auquel vous accéderez via http://faq.mondomaine.com/
    ServerName faq.mondomaine.com
</VirtualHost>
```
Activez le site via `a2ensite 000-default.conf` (déjà fait pour celui-ci). En cas de souci essayez de placer ce virtual host dans un fichier à part, disons `faq.mondomaine.com.conf` et `a2ensite faq.mondomaine.com.conf`.

Redémarrez Apache `service apache2 restart`. Il ne vous reste plus qu'à accéder à http://votre_nom_de_domaine_local/ depuis votre navigateur.
