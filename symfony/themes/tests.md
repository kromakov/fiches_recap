# Tests

Ajouter des lignes de code à votre application ajoute potentiellement de nouveaux bugs. Pour créer des applications plus fiables, il est recommandé de créer des tests unitaires et des tests fonctionnels.

[D'après la doc Symfony : Testing](https://symfony.com/doc/current/testing.html).

# Installer PHPUnit

Symfony recommande la version [installée en PHAR](https://phpunit.de/manual/current/en/installation.html#installation.phar) (PHP Archive).

Installation recommandée, en ligne de commande :

```
wget https://phar.phpunit.de/phpunit-6.1.phar
chmod +x phpunit-6.1.phar
sudo mv phpunit-6.1.phar /usr/local/bin/phpunit
phpunit --version
```

Doit afficher à la fin :

`PHPUnit x.y.z by Sebastian Bergmann and contributors.`

## Tests fonctionnels

### Principe d'un test fonctionnel :

- Exécuter une requête
- Tester la réponse
- Cliquer sur un lien ou un formulaire
- Tester la réponse
- Recommencer

### Etude du test fourni avec "symfony new _project_"

```php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

// To run your functional tests, the WebTestCase class bootstraps the kernel of your application.
class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        // The createClient() method returns a client...
        $client = static::createClient();

        // ...which is like a browser that you'll use to crawl your site:
        $crawler = $client->request('GET', '/');

        // Use assertions to test that it actually does what you expect it to
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        // Usage d'un sélecteur CSS pour cibler un ou des éléments du DOM
        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
}
```

## Exécuter un test

Depuis la racine du projet, en ligne de commande :

```
// éxécute tous les tests de l'application
phpunit

// éxécute tous les tests du répertoire Util
phpunit tests/AppBundle/Util

// éxécute un test en particulier
phpunit tests/AppBundle/Util/CalculatorTest.php

// éxécute tous les tests d'un Bundle
phpunit tests/AppBundle/
```

## Test minimal pour les routes accessibles

http://symfony.com/doc/current/best_practices/tests.html#functional-tests

## Simuler une authentification pour les tests

http://symfony.com/doc/current/testing/http_authentication.html

## Tester du code qui interagit avec la base de données

https://symfony.com/doc/current/testing/database.html

## Tests et recettes

Référence et philosophie des tests, très bien faite : http://www.test-recette.fr/
