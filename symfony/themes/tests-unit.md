# Tests

Ajouter des lignes de code à votre application ajoute potentiellement de nouveaux bugs. Pour créer des applications plus fiables, il est recommandé de créer des tests unitaires et des tests fonctionnels.

[D'après la doc Symfony : Testing](https://symfony.com/doc/current/testing.html).

# Installer PHPUnit

Symfony recommande la version [installée en PHAR](https://phpunit.de/manual/current/en/installation.html#installation.phar) (PHP Archive).

Installation recommandée, en ligne de commande :

```
wget https://phar.phpunit.de/phpunit-6.1.phar
chmod +x phpunit-6.1.phar
sudo mv phpunit-6.1.phar /usr/local/bin/phpunit
phpunit --version
```

Doit afficher à la fin :

`PHPUnit x.y.z by Sebastian Bergmann and contributors.`

## Tests unitaires

Un test unitaire permet de tester une classe simple (comme un Service Symfony par exemple).

Exemple de test d'une classe "Calculatrice".

```php
// src/AppBundle/Util/Calculator.php
namespace AppBundle\Util;

class Calculator
{
    public function add($a, $b)
    {
        return $a + $b;
    }
}
```
Pour tester cela, créer le fichier suivant :

```php
// tests/AppBundle/Util/CalculatorTest.php
namespace Tests\AppBundle\Util;

use AppBundle\Util\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calc = new Calculator();
        $result = $calc->add(30, 12);

        // Affirme que votre Calculatrice ajoute les nombres correctement
        $this->assertEquals(42, $result);
    }
}
```

Pour lancer le test : `phpunit tests/AppBundle/Util/CalculatorTest.php`

[Continuer sur le site de Symfony](http://symfony.com/doc/current/testing.html)
