# Utilisation et création de services avec Symfony

D'après la doc officielle : https://symfony.com/doc/current/service_container.html

## Introduction

Comme nous l'avons vu précédemment, Symfony est plein d'objets utiles tels que Mailer, Twig ou Doctrine, ou encore Session. A chaque fois que vous installerez un nouveau Bundle vous aurez potentiellement accès à encore plus de ces objets.

Dans Symfony ces objets sont appelés **services** et chaque service existe au sein d'un objet très spécial appelé **service container**.

Si vous connaissez le nom de ce service, vous pouvez l'appeler depuis un contrôleur via :

```php
$logger = $this->container->get('logger');
$mailer = $this->container->get('mailer');
$session = $this->container->get('session');
// Autre façon d'appeler $this->getDoctrine()->getManager();
$entityManager = $this->container->get('doctrine.entity_manager');
```

## Accéder à et utiliser les services

Les services sont accessibles à tout moment depuis un contrôleur, en utilisant la suggestion de type et l'injection de dépendance :

```php
// src/AppBundle/Controller/ProductController.php
// ...

use Psr\Log\LoggerInterface;

/**
 * @Route("/products")
 */
public function list(LoggerInterface $logger)
{
    $logger->info('Look! I just used a service');

    // ...
}
```

Le nom des services disponibles dans votre application Symfony sont accessibles via :

`php bin/console debug:container`.

## Créer et configurer des services

Créez votre service dans le dossier `App\Service` :

```php
// src/App/Service/MessageGenerator.php
namespace App\Service;

class MessageGenerator
{
```
Note: Il est aussi proposé de gérer la création de vos services dans `App\Util` (cf best practices). Auquel cas, il faut adapter la création de votre dossier & namespace en conséquence. 

Puis dans votre contrôleur :

```php
use App\Service\MessageGenerator;

public function new(MessageGenerator $messageGenerator)
{
    // thanks to the type-hint, the container will instantiate a
    // new MessageGenerator and pass it to you!
    // ...

    $message = $messageGenerator->getHappyMessage();
    $this->addFlash('success', $message);
```

Depuis Symfony 3.3, le service est configuré automatiquement.

On peut accéder au service via son `id` qui dans ce cas est son nom de classe :

```php
// only works if your service is public
$messageGenerator = $this->get(MessageGenerator::class);
```

## Injecter d'autres services ou une configuration dans un service

Dans le service :

### Sans paramètre

```php
use Psr\Log\LoggerInterface;

class MessageGenerator
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getHappyMessage()
    {
        $this->logger->info('Message generated!');
        // ...
    }
}
```

### Avec paramètres

```php
use Psr\Log\LoggerInterface;

class MessageGenerator
{
    private $logger;
    private $loggingEnabled;

    public function __construct(LoggerInterface $logger, $loggingEnabled)
    {
        $this->logger = $logger;
        $this->loggingEnabled = $loggingEnabled;
    }

    public function getHappyMessage()
    {
        if ($this->loggingEnabled) {
            $this->logger->info('Message generated!');
        }
        // ...
    }
}
```

Dans la config du service :

```yml
# app/config/services.yml
parameters:
    enable_message_logging:  true

services:
    App\Service\MessageGenerator:
        arguments:
            $loggingEnabled: '%enable_message_logging%'
```

On peut aussi récupérer ce paramètre directment depuis le contrôleur :
```php
// this ONLY works if you extend Controller
$adminEmail = $this->container->getParameter('admin_email');

// or a shorter way!
// $adminEmail = $this->getParameter('admin_email');
```

## Public Versus Private Services

```php
// but accessing directly from the container does NOT Work if private (default)
$this->container->get(MessageGenerator::class);
```

```yml
# app/config/services.yml
services:
    # ... same code as before

    # explicitly configure the service
    AppBundle\Service\MessageGenerator:
        public: true
```

https://symfony.com/doc/current/service_container.html#public-versus-private-services
