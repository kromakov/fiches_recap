## A propos de Symfony

+ Open Source ([Contribuer](http://symfony.com/doc/current/contributing))
  [What is Symfony ? > Symfony Roadmap > release process details](http://symfony.com/doc/current/contributing/community/releases.html)
+ Web framework vs Composants stand-alone
  * Full-stack [PHP framework](http://symfony.com/at-a-glance)...
  * ... basé sur un ensemble de [composants indépendants](http://symfony.com/components)
  * avec lesquels vous pouvez même construire votre [propre framework](http://symfony.com/doc/current/create_framework)
  * exemple de Silex : micro framework  
    => controllers, models, fonctionnent différemment de symfony. Mais basés sur (certains) mêmes composants.  
    https://silex.sensiolabs.org/
  * lecture optionnelle : http://symfony.com/doc/current/introduction/from_flat_php_to_symfony2.html
+ [Pourquoi utiliser un framework ?](http://symfony.com/why-use-a-framework)

## Installation

### Création d'un nouveau projet standard basé sur le framework avec Composer
#### Nouveau projet avec Composer

Si pas déjà fait, [installer composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx), puis :

`composer create-project symfony/website-skeleton my-project`

> Warning : En VM Server, si erreurs, exécuter `export COMPOSER_PROCESS_TIMEOUT=4000` pour allonger le timeout de l'installation.

#### Serveur intégré

Exécuter l'application fraichement créée en lançant le serveur intégré à Symfony, depuis la racine de votre projet via
```
php bin/console server:run
```
(ou `server:start`, la différence étant la trace des accès avec `server:run`).

#### Vérifier les [pré-requis](http://symfony.com/doc/current/reference/requirements.html) :

```
cd mon_projet/
composer require symfony/requirements-checker
```
Puis accéder à `check.php` dans le dossier *public*  à l'aide du navigateur (par ex. `http://127.0.0.1:8000/check.php`).

> Attention : vous pouvez lancer plusieurs serveurs depuis plusieurs projets Symfony, le port peut donc changer : 8000, 8001, 8002, etc.

#### Pour installer les vendors suite à un cloning de dépôt

```
cd mon_projet/
composer install
```

#### Pour mettre à niveau votre application Symfony

Voir en bas de la page "Setup", les liens suivants :

- [Upgrading a Major Version (e.g. **3**.4.0 to **4**.0.0)](https://symfony.com/doc/current/setup/upgrade_major.html)
- [Upgrading a Minor Version (e.g. 3.**3**.3 to 3.**4**.0)](https://symfony.com/doc/current/setup/upgrade_minor.html)
- [Upgrading a Patch Version (e.g. 3.3.**2** to 3.3.**3**)](https://symfony.com/doc/current/setup/upgrade_patch.html)

#### Installer un bundle via Composer

Cherchez le bundle que vous souhaitez installer, sur [Packagist.org](https://packagist.org/), (admettons `friendsofsymfony/user-bundle`) puis une fois trouvé, installez-le comme suit :

`composer require friendsofsymfony/user-bundle`

### Installation de la démo officielle complète (avant V4)

`composer create-project symfony/symfony-demo`

Ce projet utilise le gestionnaire de bases de données SQLite, si vous avez une erreur 500, installez SQLite en LDC :

`sudo apt-get install php7.1-sqlite`

=> si on vous dit que la configuration du php.ini est en conflit, choisissez `Garder la version actuelle`

Puis redémarrez le serveur web Apache :

`sudo service apache2 restart`

### Donner le droit au serveur web d'écrire dans le dossier `var`  
En se plaçant à la racine du projet :
```
sudo setfacl -dR -m u:www-data:rwX -m u:mint:rwX var
sudo setfacl -R -m u:www-data:rwX -m u:mint:rwX var
 ```
 Cela permet de donner les droits de lecture (r), d'écriture (w) et d'éxécution (X) aux utilisateurs `www-data` et `mint`, sur les sous-dossiers et fichiers du dossier `var`. En pratique, quand vous naviguez sur le site, `www-data` (Apache) vient écrire dans ce dossier (cache, sessions, logs). Lorsque vous (`mint`) exécutez des lignes de commandes `php bin/console <command>`, vous devez également pouvoir écrire dans ce répertoire.

Sinon plus simple mais moins "propre" (si la commande du dessus ne fonctionnait pas) :
 `sudo chmod -R 777 var`

## Architecture
[Système de fichiers](http://symfony.com/doc/current/page_creation.html#checking-out-the-project-structure)

### Composants
* [Symfony Core: Router et Controllers](core-routing-controllers.md)

![Ecosystem Symfony](img/symfony-ecosystem.png)  
