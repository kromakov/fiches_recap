### Programme :

#### S0 (2j) : Rappels PHP/POO
- Bases
- POO
- Héritage
- Namespace
- Framework, pourquoi ?

#### S1 : Une première appli
+ Configuration par défaut du FW
+ Routing
+ Contrôleurs
+ Composants (Request, Response)
+ Vues avec Twig
+ Services (Session, Flash messages)

#### S2 : Jouer avec la couche Modèle
+ Doctrine (couche Modèle, entités, persistence)
+ ORM, Entity, Repository
+ Console
+ Associations
+ Créer des données
+ Vues avec Twig Part 2 (export JSON)

#### S3 : Formulaires, authentification, rôles
+ Création de formulaire
+ Lier un formulaire à une entité
+ Console
+ Authentification
+ Rôles (autorisations)
+ Accéder aux permissions des utilisateurs

#### S4 : Services, commande, bundle externe, API, tests, déploiement
+ Services : création d'un service
+ Evénements
+ Créer une API
+ Créer une commande de console
+ Utiliser un Bundle de manipulation d'images
+ Tests fonctionnels
+ Déploiement sur serveur Gandi
