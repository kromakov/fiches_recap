# Avant l'Apothéose

Et bien il y a toute la formation O'clock, les cours, les challenges faciles (et les autres... :joy:), les "journées" de repos (les dimanches après-midi généralement), l'entraide entre étudiants, les cookies, les yeux qui piquent, etc.

## Projets

Au cours de la spé, un repo GitHub dédié sera ouvert pour que chaque étudiant de la promo puisse y placer ses idées de projet.  
Les autres étudiants pourront y écrire leur souhait de participer à tel ou tel projet.

Vers la fin de la spé :
- s'il manque des idées de projet, O'clock pourra fournir une ou plusieurs idées qui vont en faire transpirer plus d'un :smiling_imp:
- les groupes seront formés pour chaque projet en essayant de répartir équitablement les étudiants au sein des équipes
