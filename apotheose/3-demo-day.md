# The D-Day

## Quelques jours avant la présentation

- On analyse les bugs restants, et toute fonctionnalité qui n'aurait pas été terminée : il va falloir faire des choix sur ce qu'on choisit de traiter pour les derniers jours : mieux vaut prioriser ce qui a un impact sur la démo
- On décide qui va présenter. Ça peut être plusieurs personnes, à condition de bien être au clair sur qui présente quelle partie et dans quel ordre, sinon ça peut vite perdre en clarté et dépasser le temps imparti.
- Si pour une raison X ou Y le groupe ne peut/veut pas présenter via le partage d'écran du Teacher (vnc), on le contacte pour l'en informer et faire des tests avec lui (et pas la veille à 23h)
- Je préviens mon entourage pour qu'ils puissent assister à la présentation de mon projet sur Twitch <3


## La veille de la présentation

- On ne touche plus au code ! Ou alors avec toute la prudence du monde (pour les fioritures ^^), faudrait éviter de faire tout sauter avec un merge foireux au dernier moment, ce serait dommage
- On se fait un petit plan de la présentation, surtout si on n'est pas une bête de l'improvisation.

### Quelques exemples de sujets à aborder

- Une présentation de chacun (spé suivie et rôle sur le projet, voire chemin professionnel)
- Parler de l'origine du projet, de comment ça s'est monté, défini, redéfini
- Expliquer à qui ça s'adresse, et à quel besoin votre projet répond
- Faire une liste des fonctionnalités à montrer à l'écran
- Noter les points qui ont été les plus difficiles, les embûches rencontrées en chemin
- Expliquer vos choix techniques (librairies, setup...) de manière à peu près compréhensible pour des prophanes - ça c'est un bon entraînement :P
- Faire un bilan des choses que le projet vous a apportées (compétences, mais pas que)
- Noter dans un coin les fun facts et anecdotes à raconter en bonus
- Parler de l'avenir du projet, si vous voulez le continuer après la formation ou non
- Si c'est pertinent pour votre cas, il peut être intéressant de parler des fonctionnalités auxquelles vous avez renoncé, par manque de temps, ou que vous prévoyez pour une V2
- Faire une spéciale-dédicace à votre chien ou poisson rouge qui sera présent dans la salle, on n'en doute pas

## Le jour de la présentation

- On encourage les camarades de promo qui font leur démo ; on leur glisse quelques questions dans le chat ou vocalement quand ils en demandent
- Je présente ou aide la personne qui présente mon projet - en complétant ce qu'elle dit ou en réagissant aux questions par exemple (chat et/ou vocal)
- et that's all :)
