# Apothéose

Et oui, à la fin, on va pouvoir mettre en application toutes les notions vues chez O'clock en réalisant un super-méga-tiptop projet de la mort qui tue :tada: :heart_eyes:

Cool :sunglasses: mais maintenant il faudrait savoir comment organiser tout cela :thinking:

- [avant l'Apothéose](1-before.md)
- [durant l'Apothéose](2-during.md)
- [l'Apothéose de l'Apothéose (journée de démo)](3-demo-day.md)
- [après l'Apothéose](4-after.md) (et donc après la formation O'clock)
