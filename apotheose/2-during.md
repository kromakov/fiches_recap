# Pendant l'Apothéose

C'est le moment de **s'éclater** :
- à coder 10 heures par jour
- à mettre en pratique tout ce qu'on a vu
- à taper sur notre clavier à cause d'un bug dont il n'est pas responsable
- à créer des branches Git
- à crier de joie lorsqu'un' bug est résolu
- à parler chinois à ses proches parce que un `state` n'a pas été transmis aux `props` d'un composant
- à se taper la tête contre le mur en constatant que le bug venait d'une erreur de typo
- à siroter une :beer: ou un :cocktail: pendant que Symfony génère tous les Controllers

Oui, ok, ça va être top, mais encore faut-il **bien s'organiser** pour en profiter pleinement :arrow_down:

## Navigation

- [Horaires](#horaires)
- [Rôles de chacun](#rôles-de-chacun)
- [O'clock Consulting](#oclock-consulting)
- [Une question technique ?](#une-question-technique-)
- [Suivi Projet](#suivi-projet)
- [Points Projet](#points-projet)
- [Titre Pro](#titre-pro)
- [Gestion de projet](#gestion-de-projet)
- [Préparation de la démo](#préparation-de-la-démo)

## Horaires

Ce sont les mêmes que pendant les cours.  
=> 9h-15h + 2 heures de travail sans horaire fixe

<details><summary>Placement citation</summary>

_Seul on va plus vite, ensemble on va plus loin_

</details>

## Rôles de chacun

`101` rôles devront être attribués dans chaque équipe :
- Git master
- Product Owner
- Project Manager
- Lead developer front
- Lead developer back

Un étudiant peut "cumuler" plusieurs rôles. Chaque rôle supplémentaire augmentera sa rémunération de 20%.

:warning: Peu importe le(s) rôle(s) de chacun, vous devrez essayer de coder dans toutes les parties du code (ne pas se contenter d'HTML/CSS, ou des Controllers)

<details><summary>Description des rôles</summary>

##### Git master

- maitrise l'outil Git
- vient en aide à ses collègues s'il a un souci avec Git
- est le seul développeur autorisé à toucher à la branche `master`

##### Product Owner

- est le développeur à l'origine du projet
- donne son avis sur la priorité des tâches lors des sprints plannings
- s'assure que le projet réalisé reste cohérent avec l'objectif

##### Project Manager

- supervise l'avancement global du projet
- s'assure que chaque tâche est bien effectuée
- utilise un outil pour gérer les tâches du projet (Trello par exemple)

##### Lead developer front

- supervise la partie "front" du développement
- effectue les choix techniques sur la partie "front" si nécessaire

##### Lead developer back

- supervise la partie "back" du développement
- effectue les choix techniques sur la partie "back" si nécessaire

</details>

## O'clock Consulting

Les équipes se sont pas lancées seules dans la fosse aux lions.

_O'clock Consulting_ met à disposition 1 à 2 consultants pour aider et superviser les équipes dans la gestion de leur projet.
Ce sont des conseillers (payés à prix d'or). Ils ne pourront pas fournir de réponses techniques précises.
De plus, ces conseillers étant globalement surbookés (voyages entre les bureaux de New York, Londres, Paris et Singapour), des rendez-vous hebdomadaires _Suivi Projet_ seront organisés afin de s'assurer de la présence de chacun et de centraliser/regrouper les demandes.

<details><summary>Placement citation</summary>

_Le meilleur conseil est l'expérience, mais ce conseil arrive toujours trop tard._ - John Petit-Senn

</details>

## Une question technique ?

_O'clock Consulting_ a mis en place un process pour que ses employés hautement
qualifiés puissent aider des équipes de développements.

=> Ouvrir une **issue** dans le **dépôt Projects** de votre promo

:warning: Il est nécessaire de **bien détailler le problème rencontré** dans l'issue.  
Par exemple, il peut être intéressant d'y placer l'erreur affichée, de pousser
le code sur GitHub et de définir la portion de code supposément responsable.

N'hésitez pas à présenter les pistes de résolution que vous avez déjà envisagées,
et le résultat obtenu avec chacune.

## Suivi Projet

Régulièrement (1 fois par semaine généralement), une réunion entre chaque équipe et le(s) consultant(s) est mise en place.  
Le but de cette réunion est de faire un point sur l'avancement du projet, les sujets bloquants
et de répondre à toutes les questions rassemblées au cours de la semaine.

<details><summary>Placement citation</summary>

_Tout le monde est un génie. Mais si on juge un poisson sur sa capacité à grimper à un arbre, il passera sa vie à croire qu’il est stupide._ - Albert Einstein

</details>

## Points Projet

Une fois par semaine (généralement les jeudis), toute la promo se retrouve dans le Cockpit
"comme au bon vieux temps" pour faire un point sur chaque projet.  
Dans chaque équipe, tour à tour, l'un des membres présentera le projet et son état actuel.

On utilisera un channel vocal en parallèle ; vérifiez bien vos micros :wink:

## Titre Pro

> Quoi ? Mais le _Titre Pro_ c'est après la formation !?

Et bien non, il se **prépare tout au long de l'Apothéose**.  
En effet, le dossier du Titre Pro se fait sur vos réalisations lors de l'Apothéose.
Ainsi, il y a des documents à alimenter tout au long du projet, mais rien de chronophage.

<details><summary>Placement citation</summary>

_Ne remets pas au lendemain ce que tu peux faire le jour même_

</details>

#### Journal d'équipe

Pour obtenir un dossier clair et précis, chaque équipe va créer un _Journal d'équipe_.  
Chaque étudiant y ajoutera, chaque jour, une phrase décrivant le travail effectué la veille.

#### Carnet de bord

En plus du _Journal d'équipe_, chaque étudiant va tenir un _Carnet de bord_ pour
y ajouter, chaque jour, ce qu'il a effectué, les problèmes rencontrés, les solutions
trouvées, et ce qui l'a aidé.  
C'est comme un _journal intime_, mais pas "intime" (car sur le code) et pas "journal" (car pas papier) :exploding_head:

## Gestion de projet

#### Cahier des charges

- Description du Projet
- Définition des rôles de chacun
- Description des fonctionnalités
- Représentation visuelle de la base de données
- Si il y a lieu, vie du projet après l'Apothéose

:warning: Une fois le cahier des charges réalisé (BDD inclue), prendre contact avec _O'clock Consulting_
afin d'organiser un RDV pour valider le cahier des charges.

#### SCRUM

- 4 sprints de 1 semaine
- sprint 0 = organisation avant de coder
- sprint 3 (dernier) = debug, fignolage, détails, préparer de la démo

## Préparation de la démo

Une démo fluide et fonctionnelle est bien plus agréable qu'une démo hachée et bugguée...

Pour éviter les problèmes, la veille de la démo, le(s) consultant(s) vont :
- demander quel développeur se chargera de faire la démo
- valider la solution technique pour diffuser l'écran du développeur
- fournir un ordre de passage
