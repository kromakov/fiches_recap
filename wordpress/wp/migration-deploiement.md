# Migration et Déploiement

## Migration partielle
### Import / Export de données

Dans la partie Outils de l'interface d'administration de WordPress, il est possible d'exporter tout ou partie des contenus de WordPress au format **WordPress eXtended RSS** (ou WXR).

Ce format sera rapide à importer via le plugin proposé par worg. [wordpress-importer / Importateur WordPress](https://fr.wordpress.org/plugins/wordpress-importer/)

## Migration complète

### Manuellement

[Le codex](https://codex.wordpress.org/fr:Deplacer_WordPress) et cet [article](https://www.gregoirenoyelle.com/wordpress-migrer-son-site-local-vers-le-serveur-en-ligne/), en Français, récapitule avec ecrans, les différentes étapes.

#### Fichiers

* Copie des fichiers
* Droits / Permissions

#### Site URL

Soit depuis l'interface d'admin soit depuis PHPMyAdmin, changement des wp_options url

#### BDD 

via PHPmyAdmin Export des données du site actuel et import dans la nouvelle BDD.

#### Mise à jour de `wp-config.php`

Modification des informations de la BDD

#### Search-Replace-DB

Une fois la base de donnée installée, 

Un script de chercher/remplacer sur le [Blog de Dave Coveney](http://davidcoveney.com/575/php-serialization-fix-for-wordpress-migrations/) datant de 2009, dont l'objectif est simple, remplacer toutes les occurrences de l'URL actuelle du site par la future URL et ce même dans les données sérialisées au sein des options de WordPress.

Une version revue est didponible sur le site de [interconnect it](https://interconnectit.com/products/search-and-replace-for-wordpress-databases/) ou sur [github](https://github.com/interconnectit/Search-Replace-DB/)

D'autres scripts existent, notamment [DBSR](https://github.com/DvdGiessen/DBSR)

#### Nettoyage et test

* Test de la nouvelle installation
* Suppression de l'utilitaire de remplacement d'url

### Via Plugin

[Duplicator](https://wordpress.org/plugins/duplicator/) propose depuis l'interface d'admin, d'exporter l'ensemble de l'installation WordPress, ses contenus et sa configuration en quelques écrans. Quelques limitations existent avec le multisite et certaines installations custom.

De nombreuses [videos](https://www.youtube.com/watch?v=yZ7pHmR9JC8) de démonstration existent autour de ce plugin. 

### via WP-CLI pour la BDD

* Export : `wp db export`
* Import : `wp db import database_export.sql`
* Nettoyage en mode test : `wp search-replace website.dev website.com --dry-run`
* Nettoyage réel : `wp search-replace website.dev website.com`

Cet [article](http://wpkrauts.com/2014/how-to-migrate-wordpress-multisite-with-wp-cli/) détail les différentes étapes