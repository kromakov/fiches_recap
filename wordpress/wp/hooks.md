# Hooks

* [Liste des Hooks](http://adambrown.info/p/wp_hooks) Il y en a quand même beaucoup...
* [Une formidable article de Smashing Magazine](https://www.smashingmagazine.com/2011/10/definitive-guide-wordpress-hooks/) à lire absolument
* [Hooks sur le Codex](https://codex.wordpress.org/Plugin_API/Hooks)

## Qu'est-ce que les Hooks

Ce sont des points d'accroches utiles à WordPress lors de son chargement. Des fonctions sont associées à chaque hook, quand ce hook est appelé toutes les fonctions associées le sont aussi.

Il est donc possible d'utiliser les hooks existant ou bien d'en créer soi-même.

### Rien de vraiment nouveau

<a href="../medias/wordpress_core_load.png" target="_blank"><img src="../medias/wordpress_core_load.png"></a>

Via `get_header()`, le template `header` du thème est chargé. La particularité de ce template est de contenir notamment un appel de fonction `wp_head`. Cette fonction, disponible ici `wp-includes/general-template.php`, ne fait rien d'autre que d'executer l'action `wp_head`.

```php
function wp_head() {
	/**
	* Print scripts or data in the head tag on the front end.
	*
	* @since 1.5.0
	*/
	do_action( 'wp_head' );
}
```

cette action `wp_head` va executer l'ensemble des fonctions qui lui font référence. Un petit tour dans ce fichier (l. 238) `wp-includes/default-filters.php`.

### Types de hooks

Il y en a 2 : action et filtre

#### Action : action hook ou "do"

Les hooks d'action peuvent par exemple :

* Charger une configuration pour un thème
* Ajouter un script js au footer
* Enregistrer une information supplémentaire lors d'une édition d'article
* ...

Les hooks d'actions ne sont pas tous disponibles au même moment, ils accompagnent le chargement de WordPress.

##### `do_action()` & `add_action()`

[`do_action()`](https://developer.wordpress.org/reference/functions/do_action/) exécute l'action appelée

[`add_action()`](https://developer.wordpress.org/reference/functions/add_action/) attache/accroche une fonction à une action

###### Exemples

```php
function oclock_dump_message($message = 'INIT') {
	var_dump("START DUMP ".$message);	
}

add_action('init','oclock_dump_message');
add_action('oclock_dump','oclock_dump_message');
```

Dans un fichier de template : `do_action('oclock_dump','Hello');`

#### Filtre : filter hook ou "customize"

Les hooks de filtre peuvent par exemple : 

* Ajouter du contenu après les posts
* Modifier la longueur d'un contenu à afficher
* Modifier une information avant l'affichage
* ...

Des filtres sont disponible pour presque toutes les valeurs sur lesquelles WordPress intervient.


##### `apply_filter()` & `add_filter()`

[`apply_filter()`](https://developer.wordpress.org/reference/functions/apply_filters/) applique un filtre

[`add_filter()`](https://developer.wordpress.org/reference/functions/add_filter/) attache/accroche une fonction à un filtre

###### Exemples

```php
function oclock_format_title($title) {
	$newTitle = $title. ' - ' . esc_attr( get_bloginfo( 'name' ) );
	return '<strong>' . $newTitle . '</strong>';
}

add_filter('the_title', 'oclock_format_title');
```

## Outils

### Afficher tous les hooks

Le petit utilitaire [Simply Show Hooks](https://wordpress.org/plugins/simply-show-hooks/) permet de consulter la liste des hooks et les functions associées. 

### Listing de toutes les fonctions associées à un hook

```php
function show_hooked_functions( $hook = '' ) {
    global $wp_filter;
    if( empty( $hook ) || !isset( $wp_filter[$hook] ) ) {
    	error_log('Pas de hook mentionné');
		return;
	}

    echo '<pre>';
    print_r( $wp_filter[$hook]->callbacks );
    echo '</pre>';
}

show_hooked_functions('wp_head');
```
## Astuces

### Désactiver l'enregistrement automatique

```php
function disable_auto_save(){
    wp_deregister_script('autosave');
}
add_action( 'wp_print_scripts', 'disable_auto_save' );
```

### Ajouter un contenu à la fin de chaque post

```php
function add_post_content($content) {
	if(!is_feed() && !is_home()) {
		$content .= '<p>This article is copyright &copy; ' . date('Y') . '&nbsp;' . bloginfo('name') . '</p>';
	}
	return $content;
}
add_filter('the_content', 'add_post_content');
```

Ces astuces sont inspirées par [WPRecipes](http://www.wprecipes.com/?s=hook). Bien que très ancien, ce blog propose plusieurs 'hack' toujours très pratique.
