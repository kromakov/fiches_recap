# WordPress et Composer

Plusieurs articles abordent la problématique de WordPress et Composer : 

* [Rarst.net](http://composer.rarst.net/)
* [Roots.io](https://roots.io/using-composer-with-wordpress/)
* [Gilbert Pellegrom - WordPress+Git+Composer](https://deliciousbrains.com/storing-wordpress-in-git/)

## Composer pour l'installation de WordPress

Sur Packagist.org, [johnpbloch/wordpress](https://packagist.org/packages/johnpbloch/wordpress) est un repo mit à jour perpetuellement avec les dernières sources de WordPress.org.

Il est possible de s'appuyer sur ce repo pour installer WordPress au sein d'un projet.

```json
{
  "require": {
    "php": ">=5.4",
    "johnpbloch/wordpress": "4.*"
  },
  "extra": {
    "wordpress-install-dir": "wp"
  }
}
```

Cette configuration permet de récupérer WordPress dans un sous dossier `wp/`

## Composer, les plugins, les thèmes

[wpackagist](https://wpackagist.org/), un repo de packages associés à WordPress. Les thèmes et plugins peuvent être définis dans la config de composer afin de les installer rapidement via `composer install`.

```json
{
  "repositories": [
    {
      "type": "composer",
      "url": "https://wpackagist.org"
    }
  ],
  "require": {
    "php": ">=5.4",
    "johnpbloch/wordpress": "4.*",
    "wpackagist-plugin/advanced-custom-fields": "*",
    "wpackagist-plugin/contact-form-7": "*",
    "wpackagist-theme/hueman":"*",
    "wpackagist-theme/twentyseventeen":"*",
    "wpackagist-theme/bento":"*"
  },
  "extra": {
    "wordpress-install-dir": "wp",
    "installer-paths": {
        "content/plugins/{$name}/": ["type:wordpress-plugin"],
        "content/themes/{$name}/": ["type:wordpress-theme"]
    }
  }
}
```

### Pour aller plus loin

Via composer, il est possible d'exécuter des scripts après install / update

Ce script permet de nettoyer l'installation de wp et de modifier le fichier index.php

```json
"scripts": {
    "clean-wp-install": [
      "cp wp/index.php ./index.php",
      "rm -r wp/wp-content"
    ],
    "post-install-cmd": [
      "@clean-wp-install"
    ],
    "post-update-cmd": [
      "@clean-wp-install"
    ]
  }
```

## Bedrock

Un boilerplate de projet WordPress s'appuyant sur Composer et Wpackagist : [Bedrock](https://roots.io/bedrock/)