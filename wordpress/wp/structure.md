# WordPress : Structure

![core load](../medias/wordpress_core_load.png)

[Liste des fichiers de WordPress](https://codex.wordpress.org/WordPress_Files)

## Root

### index.php

Point d'entrée du site

### wp-blog-header.php

Point d'entrée de la configuration de wordpress

### wp-config.php

LE fichier de configuration

### wp-settings.php

Le hub de WordPress

* Chargement des MU plugins
* Chargement des plugins
* Chargement des functions globales
* Chargement des fonctions propres aux thèmes
* Initialisation de routines de traitement
* Récupération du user courant
* Check de configuration
* Fallback en cas de besoin
* Timers d'exécution
* Configuration de l'inclusion du thème
* ...
    
## /wp-admin

### wp-admin/admin.php

Le point d'entrée et le noyau de l'admin

* Configuration et connection à la db
* Construction des menus
* Chargement des données
* ...

## /wp-includes

Répertoire pour l'ensemble des fonctionnalités de WordPress
* Système de templates
* Marqueurs
* Hooks
* Conditions
* Fichiers de fallback
* ...

## /wp-content

Ce répertoire n'est pas mit à jour avec les updates de WordPress

### /wp-content/mu-plugins

Les plugins "Must Use", ils sont chargés avant tous les autres (comme no-white-screen)

### /wp-content/plugins

Les plugins de WordPress

### /wp-content/uploads

Tous les fichiers gérés par le media manager

### /wp-content/lamguages

Fichiers de traductions propres à WordPress et aux thèmes natifs WordPress

### /wp-content/themes

Les thèmes

## BDD

Tour d'horizon rapide

![DB WordPress](../medias/db-wp-442.png)

[WordPress DB](https://codex.wordpress.org/Database_Description)

* **wp_options** options du site, d'install, de configuration (wp-includes/option.php)
* **wp_users** Les utilisateurs du CMS
* **wp_usermeta** les metadata des utilisateurs : nom, prenom, préférences
* **wp_posts** tous les posts du CMS (pages, posts, révisions, items de menu, ...)
* **wp_postmeta** template, custom field, info d'édition, etc...
* **wp_comments** commentaires
* **wp_commentmeta** metadata des commentaires
* ...

