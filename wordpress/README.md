# WordPress

## Fiches Récap

* [Programme](programme.md)
* [Introduction](wp/intro.md)
* [Installation de WordPress](wp/installation.md)
* [Fichier wp-config.php](wp/wp-config.md)
* [Structure de WordPress](wp/structure.md)
* [Ressources diverses](outils/ressources.md)
* [Atom](outils/atom.md)
* [NPM](outils/npm.md)
* [SASS](outils/sass.md)
* [Browser-sync](outils/browser-sync.md)
* [Brunch](outils/brunch.md)
* [Ressources autour de WordPress](wp/ressources.md)
* [Structure d'un thème](themes/structure.md)
* [Premier thème : oFirst](themes/ofirst.md)
* [Starter theme & theme framework](themes/starter-framework.md)
* [Templates](themes/templates.md)
* [Loop](themes/loop.md)
* [Hooks](wp/hooks.md)
* [Theme Setup / Features](themes/setup-theme.md)
* [Custom fields](plugins/custom-fields.md)
* [i18n - l10n](themes/i18n-l10n.md)
* [Thèmes à connaître](themes/top-themes.md)
* [Plugins à connaître](plugins/top-plugins.md)
* [Intro au Customizer](themes/customizer.md)
* [Custom Post Types](plugins/custom-post-types.md)
* [Structure plugin](plugins/structure.md)
* [Shortcode](plugins/shortcodes.md)
* [Widgets](plugins/widgets.md)
* [Liste des APIs WordPress](wp/apis.md)
* [Thème enfant](theme/child-theme.md)
* [Custom Backoffice](plugins/custom-bo.md)
* [Intro REST API](wp/rest-api.md)
* [WordPress et Composer](wp/wp-composer.md)
* [Migration et déploiement](wp/migration-deploiement.md)
* [Metadata et Meta boxes](plugins/metadata.md)
* [WP-CLI](wp/wp-cli.md)
* [Roles et Capacités](wp/roles-capacites.md)


## Autres lectures

* [ACF | Using acf_form to create a new post](https://www.advancedcustomfields.com/resources/using-acf_form-to-create-a-new-post/)
* [ACF | Querying relationship fields](https://www.advancedcustomfields.com/resources/querying-relationship-fields/)
* [Writing WP-CLI Commands that Work - WPShout](https://wpshout.com/links/writing-wp-cli-commands-work/)
* [3 Approaches To Adding Configurable Fields To Your WordPress Plugin](https://www.smashingmagazine.com/2016/04/three-approaches-to-adding-configurable-fields-to-your-plugin/)
* [How To Make WordPress Hard For Clients To Mess Up – Smashing Magazine](https://www.smashingmagazine.com/2016/07/how-to-make-wordpress-hard-for-clients-to-mess-up/)
* [Schedule Events Using WordPress Cron – Smashing Magazine](https://www.smashingmagazine.com/2013/10/schedule-events-using-wordpress-cron/)
* [Brian S. Reed: Creating SOLID (not STUPID) Plugins and Themes | WordPress.tv](http://wordpress.tv/2017/04/13/brian-s-reed-creating-solid-not-stupid-plugins-and-themes/)
* [Comparing WordPress REST API Performance to admin-ajax.php](https://deliciousbrains.com/comparing-wordpress-rest-api-performance-admin-ajax-php/)
* [Beginner’s Tutorial On Using Yoast WordPress SEO Plugin](https://www.shivarweb.com/3902/beginners-guide-using-yoast-wordpress-seo/)
* [Hardening WordPress « WordPress Codex](https://codex.wordpress.org/Hardening_WordPress#File_Permissions)
* [Dwayne McDaniel: WP-CLI – Don’t Fear the Command Line | WordPress.tv](http://wordpress.tv/2017/04/20/dwayne-mcdaniel-wp-cli-dont-fear-the-command-line/)
* [Ajax et WordPress : le guide](https://www.seomix.fr/ajax-wordpress/)
* [Creating a Mobile App with WP-API and React Native](https://deliciousbrains.com/creating-mobile-app-wp-api-react-native/)
* [Dependency Management and WordPress: A Proposal](https://deliciousbrains.com/dependency-management-wordpress-proposal/)
* [Allow Visitors to Email You with Contact Form 7 - WPShout](https://wpshout.com/quick-guides/create-email-contact-form-7/)
* [10 Things Every WordPress Plugin Developer Should Know – Smashing Magazine](https://www.smashingmagazine.com/2011/03/ten-things-every-wordpress-plugin-developer-should-know/)
* [Why I'm Relearning WordPress Development | WPShout](https://wpshout.com/why-learning-wordpress-development/)
* [Extending WordPress With Custom Content Types](https://www.smashingmagazine.com/2015/04/extending-wordpress-custom-content-types/)
* [Automating Local WordPress Site Setup with Scripts Part 3: Automating the Rest](https://deliciousbrains.com/automating-local-wordpress-site-setup-scripts-part-3-automating-rest/)
* [WordPress Custom Image Sizes: How to Add and Use Them | WPShout](https://wpshout.com/adding-using-custom-image-sizes-wordpress-guide-best-thing-ever/)
* [The Designer’s 4 Step Guide to Securing WordPress | Webdesigner Depot](https://www.webdesignerdepot.com/2017/04/the-designers-4-step-guide-to-securing-wordpress/)
