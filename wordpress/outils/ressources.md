# Ressources / Outils divers

## Admin

* [Chmod & Chown - La gestion des droits sous GNU/Linux](http://www.leshirondellesdunet.com/chmod-et-chown)

## Lectures

* [Front-End Developer Handbook 2017](https://frontendmasters.com/books/front-end-handbook/2017/)

## Typographie et icones

* [Optimisation de lisibilité](https://typesetwith.me/)
* [Bibliothèque The League](https://www.theleagueofmoveabletype.com/)
* [Bibliothèque - FontSquirrel](https://www.fontsquirrel.com/)
* [Bibliothèque - Dafont](http://www.dafont.com/fr/)
* [Bibliothèque - Google Fonts](https://fonts.google.com/)
* [Font d'icones - Font Awesome](http://fontawesome.io/)
* [Font d'icones - IcoMoon](https://icomoon.io/)

## Images et videos

* [Unsplash Photos](https://unsplash.com/)
* [Mazwai Videos](http://mazwai.com/#/)
* [Pexels Videos](https://videos.pexels.com/)
* [Pexels Photos](https://www.pexels.com/)

## Plugins JavaScript / jQuery

* [Rellax - Vanilla parallax](https://github.com/dixonandmoe/rellax)
* [Scrollex - jQuery ScrollSpy](https://github.com/ajlkn/jquery.scrollex)
* [Scrolly - jQuery parallax simple](https://github.com/Victa/scrolly)