# Atom

## Packages fortement recommandés

* [atom-beautify](https://atom.io/packages/atom-beautify)
* [Emmet](https://atom.io/packages/emmet)
* [Linter](https://atom.io/packages/linter)
* [Linter UI](https://atom.io/packages/linter-ui-default)
* [Linter eslint](https://atom.io/packages/linter-eslint)
* [Linter html](https://atom.io/packages/linter-htmlhint)
* [Linter PHP](https://atom.io/packages/linter-php)
* [Linter stylelint](https://atom.io/packages/linter-stylelint)
* [Intentions](https://atom.io/packages/intentions)

## Packages optionnels

* [minimap](https://atom.io/packages/minimap)
* [Pigments](https://atom.io/packages/pigments)
* [Multi-cursor](https://atom.io/packages/multi-cursor)
* [Flow-IDE](https://atom.io/packages/flow-ide)
* [autocomplete-wordpress-hooks](https://atom.io/packages/autocomplete-wordpress-hooks)
* [brunch-with-atom](https://atom.io/packages/brunch-with-atom)

## Emmet

[Documentation](https://docs.emmet.io/)

### Imbrication

```
div>ul>li
```

```html
<div>
    <ul>
        <li></li>
    </ul>
</div>
```

### Frères

```
div+p
```

```html
<div></div>
<p></p>
```

### Scope

```
div+div>p>span+em^bq
```

```html
<div></div>
<div>
    <p><span></span><em></em></p>
    <blockquote></blockquote>
</div>
```

### Duplication

```
ul>li*5
```

```html
<ul>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
</ul>
```

### Attributs

#### ID et CLASS

```
div#header+div.page+div#footer.class1.class2.class3
```

```html
<div id="header"></div>
<div class="page"></div>
<div id="footer" class="class1 class2 class3"></div>
```

#### Autres attributs

```
td[title="Hello world!" colspan=3]
```

```html
<td title="Hello world!" colspan="3"></td>
```

### Contenu

```
a{En savoir plus}
```

```html
<a href="">En savoir plus</a>
```

## Exemple de fichier `.eslintrc`

```json
{
   "env": {
       "browser": true
   },
   "extends": "eslint:recommended",
   "rules": {
       "indent": ["error", 2],
       "quotes": ["error", "single"],
       "semi": ["error", "always"],
       "no-console": "off",
       "eqeqeq": "warn"
   }
}
```

## Exemple de fichier `.stylelintrc`

```json
{
  "rules": {
    "block-no-empty": true,
    "color-hex-case": "upper",
    "color-hex-length": "short",
    "color-no-invalid-hex": true,
    "indentation": 2
  }
}
```