# Back-office Custom

## Page et sous-page

WordPress propose un moyen très simple pour ajouter des pages dans l'interface d'administration. Comme à l'accoutumée, il faudra se baser sur un hook pour enregistrer les pages.

[Menus de l'interface d'administration](https://codex.wordpress.org/Administration_Menus)

Le hook cible est `admin_menu`

### Exemple de page

```php
function oagency_settings_add_plugin_page() {
	add_menu_page(
		'oAgency Settings', // Titre de la page
		'oAgency Settings', // Label du menu
		'manage_options', // Capacité utilisateur
		'oagency-settings', // Slug
		'oagency_settings_create_admin_page', // function
		'dashicons-admin-generic', // dashicons ou url d'icone
		3 // Position dans le menu d'admin
	);
}
add_action('admin_menu', 'oagency_settings_add_plugin_page');

function oagency_settings_create_admin_page(){
	?>
	<div class="wrap">
    	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
    	<p>Contenu de la page</p>
    </div>
	<?php
}
```

#### Positions par défaut dans le menu d'administration

* 2 – Dashboard
* 4 – Separator
* 5 – Posts
* 10 – Media
* 15 – Links
* 20 – Pages
* 25 – Comments
* 59 – Separator
* 60 – Appearance
* 65 – Plugins
* 70 – Users
* 75 – Tools
* 80 – Settings
* 99 – Separator


### Exemple de sous page

```php
function oagency_settings_add_plugin_page() {
	add_menu_page(
		'oAgency Settings', // Titre de la page
		'oAgency Settings', // Label du menu
		'manage_options', // Capacité utilisateur
		'oagency-settings', // Slug
		'oagency_settings_create_admin_page', // function
		'dashicons-admin-generic', // dashicons ou url d'icone
	    100 // Position dans le menu d'admin
	);

	add_submenu_page(
		'oagency-settings', // Slug de la page parente
		'Général', // Titre de la page
		'Général', // Label du menu
		'manage_options', // Capacité utilisateur
		'oagency-settings', // Slug
		'oagency_settings_create_admin_page' // function
	);
    add_submenu_page(
		'oagency-settings', // Slug de la page parente
		'Comptes', // Titre de la page
		'Comptes', // Label du menu
		'manage_options', // Capacité utilisateur
		'oagency-settings-account', // Slug
		'oagency_settings_color_page' // function
	);
}
add_action('admin_menu', 'oagency_settings_add_plugin_page');
```

Il est possible de spécifié pour les slugs parents, les slug de WordPress

* **Tableau de bord:** ‘index.php’
* **Articles:** ‘edit.php’
* **Medias:** ‘upload.php’
* **Pages:** ‘edit.php?post_type=page’
* **Commentaires:** ‘edit-comments.php’
* **Custom Post Types:** ‘edit.php?post_type=your_post_type’
* **Apparence:** ‘themes.php’
* **Extensions:** ‘plugins.php’
* **Utilisateurs:** ‘users.php’
* **Outils:** ‘tools.php’
* **Réglages :** ‘options-general.php’
* **Network Settings:** ‘settings.php’

Il existe des fonctions prédéfinies pour cibler ces différents niveaux

* [`add_dashboard_page()`](https://codex.wordpress.org/Function_Reference/add_dashboard_page)
* [`add_posts_page()`](https://codex.wordpress.org/Function_Reference/add_posts_page)
* [`add_media_page()`](https://codex.wordpress.org/Function_Reference/add_media_page)
* [`add_pages_page()`](https://codex.wordpress.org/Function_Reference/add_pages_page)
* [`add_comments_page()`](https://codex.wordpress.org/Function_Reference/add_comments_page)
* [`add_theme_page()`](https://codex.wordpress.org/Function_Reference/add_theme_page)
* [`add_plugins_page()`](https://codex.wordpress.org/Function_Reference/add_plugins_page)
* [`add_users_page()`](https://codex.wordpress.org/Function_Reference/add_users_page)
* [`add_management_page()`](https://codex.wordpress.org/Function_Reference/add_management_page)
* [`add_options_page()`](https://codex.wordpress.org/Function_Reference/add_options_page)

## Settings

WordPress propose la [Settings API](https://codex.wordpress.org/Settings_API) afin de gérer des options facilement.

Le hook cible est `admin_init`

### Exemple

```php
function oagency_settings_register_settings() {
	register_setting(
		'oagency_settings_option_group', // Groupe, utiliser pour générer les settings
		'oagency_settings_option_name', // Nom, utiliser pour retrouver les valeurs
		'oagency_settings_sanitize' // sanitize_callback
	);

	add_settings_section(
		'oagency_settings_setting_section', // id
		'Settings', // title
		'oagency_settings_section_info', // callback
		'oagency-settings-admin' // page
	);

	add_settings_field(
		'twitter_account', // id
		'Compte Twitter', // title
		'twitter_account_callback', // callback
		'oagency-settings-admin', // page
		'oagency_settings_setting_section' // section
	);
}
add_action('admin_init','oagency_settings_register_settings');
```

### Utiliser ces déclarations

Dans la déclaration des sections et settings un callback est précisé.

```php
function twitter_account_callback()
{
    // get the value of the setting we've registered with register_setting()
    $setting = get_option('oagency_settings_option_name');
    // output the field
    ?>
    <input type="text" name="oagency_settings_option_name" value="<?= isset($setting['twitter_account']) ? esc_attr($setting['twitter_account']) : ''; ?>">
    <?php
}
```

Il est possible d'aller plus loin avec l'utilisation de `settings_fields()`

---

## Exemple de Page + Setting

```php
class OAgencySettings {
	private $oagency_settings_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'oagency_settings_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'oagency_settings_page_init' ) );
	}

	public function oagency_settings_add_plugin_page() {
		add_menu_page(
			'oAgency Settings', // page_title
			'oAgency Settings', // menu_title
			'manage_options', // capability
			'oagency-settings', // menu_slug
			array( $this, 'oagency_settings_create_admin_page' ), // function
			'dashicons-admin-generic', // icon_url
			100 // position
		);
	}

	public function oagency_settings_create_admin_page() {
		$this->oagency_settings_options = get_option( 'oagency_settings_option_name' ); ?>

		<div class="wrap">
			<h2>oAgency Settings</h2>
			<p>Page de reglages</p>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'oagency_settings_option_group' );
					do_settings_sections( 'oagency-settings-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function oagency_settings_page_init() {
		register_setting(
			'oagency_settings_option_group', // option_group
			'oagency_settings_option_name', // option_name
			array( $this, 'oagency_settings_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'oagency_settings_setting_section', // id
			'Settings', // title
			array( $this, 'oagency_settings_section_info' ), // callback
			'oagency-settings-admin' // page
		);

		add_settings_field(
			'afficher_twitter_0', // id
			'Afficher Twitter', // title
			array( $this, 'afficher_twitter_0_callback' ), // callback
			'oagency-settings-admin', // page
			'oagency_settings_setting_section' // section
		);

		add_settings_field(
			'compte_twitter_1', // id
			'Compte Twitter', // title
			array( $this, 'compte_twitter_1_callback' ), // callback
			'oagency-settings-admin', // page
			'oagency_settings_setting_section' // section
		);
	}

	public function oagency_settings_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['afficher_twitter_0'] ) ) {
			$sanitary_values['afficher_twitter_0'] = $input['afficher_twitter_0'];
		}

		if ( isset( $input['compte_twitter_1'] ) ) {
			$sanitary_values['compte_twitter_1'] = sanitize_text_field( $input['compte_twitter_1'] );
		}

		return $sanitary_values;
	}

	public function oagency_settings_section_info() {

	}

	public function afficher_twitter_0_callback() {
		printf(
			'<input type="checkbox" name="oagency_settings_option_name[afficher_twitter_0]" id="afficher_twitter_0" value="afficher_twitter_0" %s> <label for="afficher_twitter_0">Afficher le compte twitter</label>',
			( isset( $this->oagency_settings_options['afficher_twitter_0'] ) && $this->oagency_settings_options['afficher_twitter_0'] === 'afficher_twitter_0' ) ? 'checked' : ''
		);
	}

	public function compte_twitter_1_callback() {
		printf(
			'<input class="regular-text" type="text" name="oagency_settings_option_name[compte_twitter_1]" id="compte_twitter_1" value="%s">',
			isset( $this->oagency_settings_options['compte_twitter_1'] ) ? esc_attr( $this->oagency_settings_options['compte_twitter_1']) : ''
		);
	}

}
if ( is_admin() )
	$oagency_settings = new OAgencySettings();

/*
 * Accéder aux valeurs:
 * $oagency_settings_options = get_option( 'oagency_settings_option_name' ); // Array of All Options
 * $afficher_twitter_0 = $oagency_settings_options['afficher_twitter_0']; // Afficher Twitter
 * $compte_twitter_1 = $oagency_settings_options['compte_twitter_1']; // Compte Twitter
 */

```

## Astuces

### Outil pour la génération des pages et settings

Ce [générateur](http://wpsettingsapi.jeroensormani.com/) est très pratique mais rempli de pubs...

Autre option, [Jeremy Hixon](https://jeremyhixon.com/) propose sur son blog une liste de [générateurs](https://jeremyhixon.com/tools/) très efficace

### Supprimer l'aide

[Help tab](https://codex.wordpress.org/Class_Reference/WP_Screen/add_help_tab)

```php
//Remove Help tab
function wpc_remove_help( $old_help, $screen_id, $screen ){
	$screen->remove_help_tabs();
	return $old_help;
}
add_filter( 'contextual_help', 'wpc_remove_help', 999, 3 );
```

### Supprimer les options d'écrans

[Code Reference](https://developer.wordpress.org/reference/hooks/screen_options_show_screen/)

```php
//Remove Screen Options
add_filter('screen_options_show_screen', '__return_false');
```

### Supprimer les Widgets d'accueil

[Dashbaord_setup](http://codex.wordpress.org/Plugin_API/Action_Reference/wp_dashboard_setup)

```php

// Suppression des widgets du dashboard
function remove_dashboard_meta() {
    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

// Suppression du panneau "Welcome"
remove_action('welcome_panel', 'wp_welcome_panel');

```

### Créer un Widget d'accueil

[Code Reference](http://codex.wordpress.org/Function_Reference/wp_add_dashboard_widget)

```php
//Custom widget dashboard
function wpo_dashboard_widget_function() {
	echo ' code HTML de votre super widget ';
}
function wpo_add_dashboard_widgets(){
	wp_add_dashboard_widget('wpo_summary_dashboard_widget', 'Titre de votre super widget', 'wpo_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'wpo_add_dashboard_widgets' );
```

### Masquer des pages

[Reference](http://codex.wordpress.org/Plugin_API/Action_Reference/admin_menu)

```php
//Hide menu page
function wpo_remove_menus(){
	remove_menu_page( 'index.php' ); //Dashboard
	remove_menu_page( 'edit.php' ); //Posts
	remove_menu_page( 'upload.php' ); //Media
	remove_menu_page( 'edit.php?post_type=page' ); //Pages
	remove_menu_page( 'edit-comments.php' ); //Comments
	remove_menu_page( 'themes.php' ); //Appearance
	remove_menu_page( 'plugins.php' ); //Plugins
	remove_menu_page( 'users.php' ); //Users
	remove_menu_page( 'tools.php' ); //Tools
	remove_menu_page( 'options-general.php' ); //Settings
}
add_action( 'admin_menu', 'wpo_remove_menus' );
```

### Admin bar

[Reference](http://codex.wordpress.org/Function_Reference/remove_node)

### Supprimer les crédits du footer

```php
//Remove WordPress Footer Credits
function wpo_remove_footer_admin() {
	return '';
}
add_filter('admin_footer_text', 'wpo_remove_footer_admin');
```

### Supprimer la version du footer

```php
//Remove WordPress version in footer
function wpo_remove_version_footer() {
	remove_filter( 'update_footer', 'core_update_footer' );
}
add_action( 'admin_menu', 'wpo_remove_version_footer' );
```

### Styles pour le BO

[`admin_enqueue_scripts()`](http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts)

```php
//Load custom admin CSS
function wpo_load_custom_admin_css() {
	wp_enqueue_style('wpo_admin_css', 'url-exemple/admin_style.css');
}
add_action( 'admin_enqueue_scripts', 'wpo_load_custom_admin_css' );

```

### Login
#### Changer l'apparence

[Le codex](https://codex.wordpress.org/Customizing_the_Login_Form) traite cette problématique

```php
//Custom CSS Login
function wpo_custom_login() {
	wp_enqueue_style( 'wpo-login-css', '/url-exemple/stle_login.css');
}
add_action('login_head', 'wpo_custom_login');

//Custom Logo URL
function wpo_url_login(){
	return esc_url( home_url( '/' ) );
}
add_filter('login_headerurl', 'wpo_url_login');

//Custom Logo Title
function wpo_login_logo_url_title() {
	return 'Site réalisé par ...';
}
add_filter( 'login_headertitle', 'wpo_login_logo_url_title' );
```

#### Déplacer la page de connexion

[Move Login](https://wordpress.org/plugins/sf-move-login/) est un petit plugin très simple pour déplacer la page de connexion d'un site WordPress.

> Le `.htaccess` proposé doit être adapté en fonction du type d'installation retenue (classique, custom, multisite)
