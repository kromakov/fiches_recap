# Plugin : Structure

## Qu'est ce qu'un plugin

Un plugin offre la possibilité de facilement modifier, personnaliser et améliorer un projet WordPress en ajoutant des fonctionnalités basées sur [l'API dédiée](https://codex.wordpress.org/Plugin_API), sans toucher le coeur du système de WordPress.

La lecture du sujet [Writing a Plugin](https://codex.wordpress.org/Writing_a_Plugin) sur le codex est la meilleure source d'informations disponible.

## Types de plugins

Il existe 2 types de plugins

### [plugins](https://codex.wordpress.org/Plugins)

Les plugins classiques, ils sont installés dans le dossier `plugins` du `wp-content`.

Ils sont téléchargeables / activables / désactivables depuis l'interface d'administration.


### [mu-plugins](https://codex.wordpress.org/Must_Use_Plugins)

Les 'must use` plugins sont installés dans un dossier `mu-plugins` du `wp-content`.

Ils sont automatiquement actifs et chargés juste avant les plugins classiques. Ils ne sont pas accessible depuis l'interface d'administration.


## hello

Découverte d'un plugin de base, présent dans toutes les install de WordPress

## Premier plugin

### Structure de base

* plugins/
	* mon-plugin/
		* mon-plugin.php

### File header

le [File Header](https://codex.wordpress.org/File_Header) permet de définir, comme pour les templates ou les themes, des informations concernant le plugin.

### Vérification minimale

Le fichier de plugin ne doit pas être appelé directement.

```php
if ( ! defined( 'WPINC' )) { die; }
```

### Le PIRE exemple

**Ne JAMAIS faire ça !**

```php
// hello-world.php
//...FILE HEADER

echo 'Hello World';
```

### Amélioration

Il faut structurer son plugin via des fonctions / classes+methodes et greffer les actions à des hooks WordPress.

```php
// hello-world.php
//...FILE HEADER

function say_hello(){
	echo 'Hello World';
}

add_action('admin_footer', 'say_hello');
```

> Définir une nouvelle action (voir les [Hooks](../wp/hooks.md))
> Faire un plugin qui affiche le titre d'un post au hasard dans toutes les pages


## Activation et Désactivation

WordPress propose de fonctions pour intercepter l'activation ou la désactivation d'un Plugin et d'effectuer des actions associées (comme le refresh des permaliens lors de l'ajout d'un Custom Post Type)

```php
function activate_mon_plugin(){ 
	//...
}

function deactivate_mon_plugin(){ 
	//...
}


// Activation
register_activation_hook( __FILE__, 'activate_mon_plugin' );
// Désactivation
register_deactivation_hook( __FILE__, 'deactivate_mon_plugin' );
```

## Désinstallation

Il est possible d'ajouter des actions spécifiques lors de la désinstallation d'un plugin.

En plaçant un fichier `uninstall.php` à la racine du plugin avec cette configuration minimale

```php
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
```

## Boilerplates

### Classique

En s'appuyant sur ce [boilerplate](https://github.com/DevinVinson/WordPress-Plugin-Boilerplate) 

* mon-plugin/
	* admin/
	* inlcudes/
	* languages/
		* mon-plugin.pot
	* public/
	* README.txt
	* index.php
	* mon-plugin.php
	* uninstall.php

### MVC

Ce [boilerplate](https://github.com/iandunn/WordPress-Plugin-Skeleton) est structuré en MVC.

Afin d'en découvrir un peu plus, la lecture de [ce guide](https://iandunn.name/content/presentations/wp-oop-mvc/mvc.php#/) écrit par [Ian Dunn](https://iandunn.name/) sur l'implémentation du MVC pour les plugins WordPress est un très bon point de départ.