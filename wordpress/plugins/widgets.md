# Widgets

Les widgets sont des éléments réservés aux Sidebars ou Widgets Area de WordPress.

Par défaut, il existe un certain nombre de widgets au sein de WordPress :

* Champ de recherche
* Archives
* Posts récents
* Catégories
* ...

Il est possible de créer de nouveaux widgets en se basant sur la classe `WP_Widget` que propose WordPress.

[Codex - Widgets API](https://codex.wordpress.org/Widgets_API)

L'idéal est de contenir un widget dans un plugin. Exportable et facilement modifiable sans se soucier des problématiques associées aux thèmes.

## Structure d'un widget

Un widget est une classe qui hérite de `WP_Widget` et son chargement se fait par `register_widget()` qui doit être appelée lors de l'execution du hook `widgets_init`

```php
class My_Widget extends WP_Widget {

	public function __construct() {
		$widget_options = array( 
			'classname' => 'my_widget',
			'description' => 'My widget description',
		);
		parent::__construct( 'my_widget', 'My Widget', $widget_options );
	}

	public function widget( $args, $instance ) {
		// Affichage du widget
	}

	public function form( $instance ) {
		// Formulaire pour la configuration du Widget
	}

	public function update( $new_instance, $old_instance ) {
		// Processus d'enregistrement des données / options du Widget
	}
}

function my_widget_loader() {
	register_widget( 'My_Widget' );
}

add_action( 'widgets_init', 'my_widget_loader' );
```

## Lectures et exemples

* Sur le [Codex](https://codex.wordpress.org/Widgets_API)
* [Custom Widget](https://code.tutsplus.com/articles/building-custom-wordpress-widgets--wp-25241) sur tutsplus, article ancien mais tjs d'actualité
* [Making first widget](https://wpshout.com/quick-guides/making-first-wordpress-widget/)