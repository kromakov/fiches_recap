# Custom fields

[Custom Fields sur le Codex](https://codex.wordpress.org/Custom_Fields)

## Accéder aux champs personnalisés

Dans l'édition d'un post > Options d'écran > [x] Champs personnalisés 

## Afficher les champs personnalisés

### Sous forme de liste pré-structurée

Le template tag [`the_meta()`](https://codex.wordpress.org/Template_Tags/the_meta), utilisable dans la boucle, permet de récupérer facilement la liste des champs personnalisés d'un post.

```php
<?php the_meta(); ?>
```

### Sous forme de tableau

Il est possible de récupérer les champs personnalisés sous forme de tableau via [`get_post_custom()`](https://codex.wordpress.org/Function_Reference/get_post_custom)

```php
<?php $custom_fields = get_post_custom(); ?>
```

Exemple de mise en place

```php
<?php
	// Récupération des champs personnalisés
	$custom_fields = get_post_custom(get_the_ID());
	// Ciblage d'une clé
	$my_custom_field = $custom_fields['my_custom_field'];
	// On boucle sur les différentes entrées
	foreach ( $my_custom_field as $key => $value ) {
		echo $key . " => " . $value . "<br />";
	}
?>
```

### Sous forme de tableau via les posts metadata

Les champs personnalisés sont stockés dans les metadata d'un post. Il est possible d'accéder aux metadata via [`get_post_meta()`](https://developer.wordpress.org/reference/functions/get_post_meta/).

```php
// Récupération des metadata
<?php $meta = get_post_meta( get_the_ID() ); ?>
// Récupération des valeurs de `ma_cle`
<?php $valeurs_ma_cle = get_post_meta( get_the_ID(), 'ma_cle' ); ?>
// Récupération de la première valeur de `ma_cle`
<?php $valeur_ma_cle = get_post_meta( get_the_ID(), 'ma_cle', true ); ?>
```

Cette fonction possède des variantes [`get_post_custom_values()`](https://codex.wordpress.org/Function_Reference/get_post_custom_values) pour la récupération des valeurs seules ou [`get_post_custom_keys()`](https://codex.wordpress.org/Function_Reference/get_post_custom_keys) pour les clés seules.

## Purger les CF inutilisés

### Avec un plugin

[Delete Custom Fields](https://wordpress.org/plugins/delete-custom-fields/)

### Depuis la BDD

#### Avec une requête

```sql	
DELETE FROM wp_postmeta WHERE meta_key = 'meta_key';
```

#### Depuis PMA

Toutes les metadatas de posts sont stockées dans dans la table `wp_postmeta`.

## Aller plus loin

Un formidable plugin permet de décupler le potentiel des custom fields. [Advanced Custom Fields](https://www.advancedcustomfields.com) - Une merveille !