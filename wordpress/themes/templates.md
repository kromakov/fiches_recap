# Thèmes : Templates

[UNE adresse ](https://codex.wordpress.org/Templates)

## Hierarchie

### Représentation de la hiérarchie des templates (modèles)

Plugin très utile : [What The File](https://wordpress.org/plugins/what-the-file/)

<a href="../medias/template_hierarchy.png" target="_blank"><img src="../medias/template_hierarchy.png" /></a>

[Hiérarchie WordPress](https://wphierarchy.com/)

### Fallback et ordre

Quelques exemples pour mieux comprendre la hiérarchie

#### Affichage d'un article

single.php > singular.php > index.php

#### Affichage d'une page

page.php > singular.php > index.php

#### Affichage de la page d'accueil

* Statique : front-page.php > index.php
* Derniers articles : home.php > index.php

#### Affichage d'une page d'erreur 404

404.php > index.php

#### Affichage d'une catégorie

category.php > archive.php > index.php

#### Affichage d'un tag

tag.php > archive.php > index.php

#### Affichage des résultats de recherche

search.php > index.php

## Templates tags

[Référence](https://codex.wordpress.org/Template_Tags)

Ce sont des outils servant à afficher les contenus de WordPress. 

* Templates
* Valeurs d'options
* Contenu de posts
* Menus
* Configuration
* Fonctionnalités

### Où les trouver ?

Les utilitaires de templates sont, pour la plupart, disponibles dans les fichiers `...-template.php` du dossier wp-includes

* [wp-includes/general-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/general-template.php#L0)
* [wp-includes/author-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/author-template.php#L0)
* [wp-includes/bookmark-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/bookmark-template.php#L0)
* [wp-includes/category-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/category-template.php#L0)
* [wp-includes/comment-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/comment-template.php#L0)
* [wp-includes/link-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/link-template.php#L0)
* [wp-includes/post-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/post-template.php#L0)
* [wp-includes/post-thumbnail-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/post-thumbnail-template.php#L0)
* [wp-includes/nav-menu-template.php](https://core.trac.wordpress.org/browser/trunk/src/wp-includes/nav-menu-template.php#L0)

### Quelques exemples

```php
<?php bloginfo('name'); ?>
```

```php
<?php the_title(); ?>
```

```php
<?php wp_list_categories(); ?>
```

```php
<?php get_header(); ?>
```

* [get_header()](https://developer.wordpress.org/reference/functions/get_header/)
* [get_footer()](https://developer.wordpress.org/reference/functions/get_footer/ "Function Reference/get footer")
* [get_sidebar()](https://developer.wordpress.org/reference/functions/get_sidebar/ "Function Reference/get sidebar")
* [get_template_part()](https://developer.wordpress.org/reference/functions/get_template_part/ "Function Reference/get template part")
* [get_bloginfo()](https://developer.wordpress.org/reference/functions/get_bloginfo/ "Function Reference/get bloginfo")
* [wp_title()](https://developer.wordpress.org/reference/functions/wp_title)
* [body_class()](https://developer.wordpress.org/reference/functions/body_class)
* [post_class()](https://developer.wordpress.org/reference/functions/post_class)
* [the_category()](https://developer.wordpress.org/reference/functions/the_category)
* [the_ID()](https://developer.wordpress.org/reference/functions/the_ID)
* [the_content()](https://developer.wordpress.org/reference/functions/the_content)
* [wp_nav_menu()](https://developer.wordpress.org/reference/functions/wp_nav_menu/)
* [get_the_post_thumbnail()](https://developer.wordpress.org/reference/functions/get_the_post_thumbnail/)
* ...

La liste complète des template tags en [français](https://codex.wordpress.org/fr:Marqueurs_de_Modele) 

### Conditional tags

[Référence](https://codex.wordpress.org/Conditional_Tags)

Outils facilitant l'aiguillage des affichages, templates, droits, ...

#### Quelques exemples

* [is_front_page()](https://codex.wordpress.org/Function_Reference/is_front_page)
* [is_single()](https://developer.wordpress.org/reference/functions/is_single/)
* [is_page()](https://developer.wordpress.org/reference/functions/is_page/)
* [is_category()](https://developer.wordpress.org/reference/functions/is_category/)
* ...

Par exemple, pour vérifier filter les types de pages d'accueil avec un comportement adapté.

```php
if ( is_front_page() && is_home() ) {
  // Homepage par défault 
} elseif ( is_front_page() ) {
  // Homepage statique (admin > lecture > page statique)
} elseif ( is_home() ) {
  // Page de blog
} else {
  // Autre chose
}
```

## Template personnalisé

Il est possible, en plus des templates par défaut de WordPress, de concevoir des templates personnalisés

```php
<?php
/*
Template Name : Modele d'exemple
*/

// ...

?>
```

Ce modèle sera disponible dans un sélecteur directement dans l'administration.

## Fragments de templates

Se basant sur des template tags, se principe vise à simplifier le chargement de template très précis. Par exemple, le logo et la baseline d'un header ou le copyright d'un footer, ...

### Importation de template

* [get_header()](https://developer.wordpress.org/reference/functions/get_header/) Tag pour le header du site
* [get_footer()](https://developer.wordpress.org/reference/functions/get_footer/) Tag pour le footer du site
* [get_sidebar()](https://developer.wordpress.org/reference/functions/get_sidebar/) Tag pour la sidebar (widget area) par défaut du site

Il est également possible d'importer des fragments de templates grâce à [`get_template_part()`](https://developer.wordpress.org/reference/functions/get_template_part/)

```php
get_template_part( 'template-parts/page/content', 'front-page' );
get_template_part( 'template-parts/page/content', 'page' );
```

### Retrouver un fichier

* [`get_theme_file_uri($fichier)`](https://developer.wordpress.org/reference/functions/get_theme_file_uri/) Retourne l'URL d'un fichier dans le thème
* [`get_theme_file_path($fichier)`](https://developer.wordpress.org/reference/functions/get_theme_file_path/) Retourne le chemin d'un fichier dans le thème
* [`get_parent_theme_file_uri($fichier)`](https://developer.wordpress.org/reference/functions/get_parent_theme_file_uri/) Retourne l'URL d'un fichier du thème parent
* [`get_parent_theme_file_path($fichier)`](https://developer.wordpress.org/reference/functions/get_parent_theme_file_path/) Retourne le chemin d'un fichier du thème parent

> Dans les versions précédentes de WordPress : 
>
> * [`get_template_directory_uri()`](https://developer.wordpress.org/reference/functions/get_template_directory_uri/)
> * [`get_template_directory()`](https://developer.wordpress.org/reference/functions/get_template_directory/)
> * [`get_stylesheet_directory_uri()`](https://developer.wordpress.org/reference/functions/get_stylesheet_directory_uri/)
> * [`get_stylesheet_directory()`](https://developer.wordpress.org/reference/functions/get_stylesheet_directory/)

