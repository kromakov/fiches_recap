# Intro au Customizer

Il est possible d'ajouter des options de configuration à un thème via le customizer 

[Codex - Customize Manager](https://codex.wordpress.org/Class_Reference/WP_Customize_Manager)

## Customizer API

![Organisation](../medias/customizer-orga.png)

### Panel

C'est un item placé dans la liste principale

### Section

[add_section()](https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section)

C'est un regroupement de réglages (controls/settings)

### Control

[add_control()](https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control)

C'est un type de champ pour attribuer une valeur à un setting

**type de control**
* text (par défaut)
* checkbox
* textarea
* radio
* select
* dropdown-pages
* email
* url
* number
* hidden
* date

D'autres controls existent comme [WP_Customize_Color_Control](https://codex.wordpress.org/Class_Reference/WP_Customize_Color_Control) ou [WP_Customize_Image_Control](https://codex.wordpress.org/Class_Reference/WP_Customize_Image_Control). Il est également possible de créer des types de control personnalisé, [cet article](http://ottopress.com/2012/making-a-custom-control-for-the-theme-customizer/) décrit la démarche en héritant de la classe [WP_Customize_Control](https://codex.wordpress.org/Class_Reference/WP_Customize_Control).

### Setting

[add_setting()](https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting)

C'est la valeur associée à un control


## Mise en place
### Déclaration

Idéalement la configuration est placée dans un fichier indépendant chargé depuis `functions.php`

```php
require get_parent_theme_file_path( '/inc/customizer.php' );
```

L'enregistrement de la configuration passe par le hook `customize_register`

```php
function montheme_customize_register( $wp_customize )
{
	// Configuration
}
add_action( 'customize_register', 'montheme_customize_register' );
```

### Ajout d'un panel

```php
$wp_customize->add_panel( 'montheme_theme_panel', [
    'title' => __( 'Mon theme', 'montheme' ), //titre
    'description' => 'MonTheme Panel - Gestion du theme', //desc
    'priority' => 1, //Emplacement
  ]);
```

### Ajout d'une section

```php
$wp_customize->add_section( 'montheme_footer' , [
    'title' => 'Footer',
    'panel' => 'montheme_theme_panel',
  ]);
```

### Ajout d'un setting

```php
$wp_customize->add_setting( 'montheme_footer_active', array() );
```

### Ajout d'un control

```php
$wp_customize->add_control( 'oagency_footer_active', array(
    'type' => 'checkbox',
    'section' => 'montheme_footer',
    'label' => __( 'Show footer text', 'montheme' ),
    ) );
```

### Exploiter dans un template

```php
//Le nom du setting
if(get_theme_mod('montheme_footer_active')) {
	// ...
}
```

## Quelques ressources

* [Snippet de customizer](http://web-profile.net/wordpress/docs/customization-api/)
* [Exemples de configuration](https://github.com/WPTRT/code-examples/tree/master/customizer)
* [Theme Customization API](https://codex.wordpress.org/Theme_Customization_API)
* [Quelques custom controls](https://github.com/bueltge/Wordpress-Theme-Customizer-Custom-Controls)

