


> Documentation déplacée ici: http://documentation.vpn.oclock.io/publique/teleporteur/vm/01-accueil



# Téléporteur virtualisé

L'objectif de ce document est de détailler la marche à suivre pour utiliser le Téléporteur de manière virtualisée.

## Qu'est-ce que le Téléporteur ?

Le Téléporteur est un système d'exploitation basé sur Linux Mint et adapté par O'clock. L'ensemble de la formation O'clock se déroule à l'intérieur du Téléporteur qui contient tout le nécessaire : navigateur web, éditeur de code, serveur HTTP, serveur de base de données, etc.

À l'origine, le Téléporteur était systématiquement livré à chaque étudiant sur une clé USB et il fallait l'utiliser "en bootant dessus". C'est-à-dire que l'ordinateur (qu'il soit Mac ou PC) démarrait directement sur le Téléporteur et court-circuitait complètement le système d'exploitation principal de l'ordinateur (Windows, Mac ou même Linux). Cette modalité n'était malheureusement pas compatible avec tous les ordinateurs (60-70%), nous avons maintenant généralisé l'utilisation du Téléporteur en mode virtualisé.

## Différentes manières d'utiliser le Téléporteur.

Le Téléporteur peut donc être utilisé de plusieurs manières :

  - via une machine virtuelle (VM) avec interface graphique (**plan A**) : que ce soit VirtualBox, VM Ware, Parallels, etc. le Téléporteur est exécuté à l'intérieur d'une machine virtuelle gérée par un OS "hôte" (Windows ou macOS).
 - via une VM en mode serveur (**plan B**) : le fonctionnement est très proche du mode précédent, la grosse différence c'est qu'on n'utilise pas du tout l'environnement graphique du Téléporteur mais uniquement ses fonctionnalités serveur.
 
Et puis il y a ce que nous appelons le "boot direct", c'est l'ancienne manière d'utiliser le Téléporteur, celle qui est encore détaillée dans les pages 1 à 14 du "Guide d'utilisation du Téléporteur" mais qui est progressivement abandonnée, au fur et à mesure que les promos BigBang, Cosmos et Discovery se terminent.

## Le mode virtualisé avec interface graphique (mode VM graphique)

En mode VM graphique, votre OS habituel (Windows ou macOS) sert uniquement à exécuter la machine virtuelle (VM) qui fera tourner le Téléporteur. Une fois la VM du Téléporteur lancée, vous la mettez en pleine écran et vous utilisez l'interface graphique du Téléporteur, exactement comme si vous l'aviez lancé en boot direct.

La différence c'est que votre OS habituel (hôte) fonctionne en parallèle en arrière-plan. Les ressources de l'ordinateur sont partagées entre les 2 environnements  car c'est comme si vous aviez deux ordinateurs en un. L'ordinateur doit donc être relativement récent et disposer, idéalement de 8Go de mémoire vive. 

Même si c'est assez peu fréquent, il peut arriver que des applications de l'OS hôte apparaîseent au dessus du Téléporteur (l'OS invité). C'est le cas par exemple si vous avez une application ouverte sur votre OS hôte qui veut afficher une notification (par exemple si vous recevez un mail), celle-si peut s'afficher par dessus l'OS invité (Téléporteur).

Pour ces deux raisons, il convient de **fermer toutes les applications de l'OS hôte avant de lancer la VM du Téléporteur**.

Ce mode est à préférer en début de formation car il vous permet d'avoir exactement les mêmes outils que les autres étudiants. Les formateurs pourront d'autant mieux vous guider et vous accompagner. Plus tard dans la formation, vous pourrez, si vous le souhaitez, évoluer vers le mode VM serveur.

Dans certains cas, le mode VM graphique ne fonctionne pas correctement. Dans ces cas, on met en place directement le mode VM serveur. C'est le cas par exemple de certains MacBook Pro Retina récents.

## Le mode virtualisé sans interface graphique (mode VM serveur)

En mode VM serveur, vous utilisez l'interface graphique de votre OS habituel (navigateur, éditeur de code, gestion des fichiers, etc.) et le Téléporteur est utilisé uniquement pour fournir l'environnement LAMP (Linux, Apache, MySQL, PHP).

Concrètement, comme ça se passe ? Vous démarrez votre ordinateur normalement et vous lancez la machine virtuelle du Téléporteur sous VirtualBox en mode "sans écran" (headless en anglais). Vous ouvrez Atom dans votre OS et vous travaillez dans des fichiers stockés dans les répertoires habituels de votre OS (sur le bureau, dossier Documents, etc.). Pour tester vos pages, vous lancez votre navigateur en lui indiquant l'adresse IP locale du téléporteur (au lieu de localhost). Le serveur web du Téléporteur reçoit la requête. Il peut la traiter car il a accès aux fichiers de votre projet via à un partage de fichiers entre votre OS et le Téléporteur.

Cela permet d'allier le confort d'utilisation de votre OS avec la puissance d'un environnement LAMP stable et professionnel.

# Mise en place

Les instructions suivantes sont communes à la mise en place du mode VM graphique et mode VM serveur.

Plusieurs étapes sont nécessaires. Lorsque la manipulation est différente entre Windows et macOS, deux parcours suivants seront décrits.

### Installation de VirtualBox

Nous utiliserons VirtualBox, sous macOS comme sous Windows, pour héberger la machine virtuelle permettant d'utiliser le Téléporteur de manière virtualisée.

On peut le télécharger à cette adresse : [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)

 - pour Windows : choisir le lien "Windows hosts"
 - pour macOS : choisir le lien "OS X hosts"
 
Téléchargez le fichier d'installation et installez-le normalement en suivant les différentes étapes (les paramètres par défaut conviennent).

Une fois VirtualBox installé, essayez de le lancer. Vous devriez voir une fenêtre de ce type (Figure 1 : VirtualBox installé)

![VirtualBox installé](img/virtualbox-install.png)

### Téléchargement du disque virtuel

**ATTENTION** : pour l'étape suivante vous allez devoir télécharger un fichier de 3,5 Go. Selon la vitesse de votre connexion, cela peut durer de quelques minutes à plusieurs heures. Par exemple, pour une connexion ADSL avec 5MBit/s en download, cela prendrait presque 2 heures ([estimation du temps de téléchargement](http://downloadtimecalculator.com/#file=3.5x2^30&speed=5x10^6&overhead=0.9)).

Vous pouvez télécharger le disque virtuel du Téléporteur au format VDI à l'adresse suivante :

 - [https://static.oclock.io/teleporteur/teleporteur-v3.vdi](https://static.oclock.io/teleporteur/teleporteur-v3.vdi) (3,5 Go)
 
Une fois le fichier téléchargé, vous pouvez vérifier l'intégrité du fichier via son *hash MD5* :

```
8239052ed75b0c2bee4d646a08d7c826  teleporteur-v3.vdi
```

Si vous ne savez pas comment faire cette vérification, voici un article complet qui vous indiquera les démarches selon votre OS : [http://www.commentcamarche.net/faq/41-md5sum-verifier-l-integrite-des-telechargements](http://www.commentcamarche.net/faq/41-md5sum-verifier-l-integrite-des-telechargements)

Une fois que vous avez téléchargé le fichier et que vous avez vérifié son intégrité, vous pouvez le renommer en `teleporteur.vdi`.

Vous pouvez passer directement à l'étape suivante : [Création de la machine virtuelle](#création-de-la-machine-virtuelle)

### Création d'un disque virtuel à partir de la clé USB contenant le Téléporteur

**Cette étape n'est utile que si vous disposez d'une clé USB Téléporteur (BigBang, Cosmos et Discovery). Si ce n'est pas votre cas, passez directement à la section [Création de la machine virtuelle](#création-de-la-machine-virtuelle).**

Il s'agit de copier le contenu de la clé USB contenant le Téléporteur à l'intérieur d'un fichier de type VDI qui sera ensuite utilisé pour simuler le disque dur de la machine virtuelle qui permettra de faire fonctionner le Téléporteur dans VirtualBox.

Avant de commencer la procédure, assurez-vous d'avoir au moins 32 Go de disponibles sur votre ordinateur.

La procédure est différente entre Windows et macOS.

#### Sous Windows

Les étapes et captures d'écran ci-dessous s'appliquent à Windows 7 mais il ne doit pas être très compliqué de reproduire ces manipulations sur d'autres versions de Windows.

##### Récupération du numéro du disque

- Branchez la clé USB contenant le Téléporteur à votre ordinateur
- Ouvrir le "Menu démarrer" et faire un clic droit sur "Poste de travail"
- Cliquer sur "Gérer"
- Dans l'application qui s'ouvre, sur la liste de gauche, chosir "Gestion des disques"
- Après un court temps de chargement, la partie droite de l'application se met à jour avec la liste de vos partitions (en haut) et de vos disques (en bas). Identifiez le numéro du disque qui correspond à la clé USB.

Dans la capture d'écran ci-dessous (Figure 2 : Gestion des disques), la première puce rouge numérotée (1) correspond au disque dur système (Disque 0) et la deuxième puce rouge numérotée (2) correspond à la clé USB (Disque 1).

![Gestion des disques](img/disk-management.png)

Pour identifier à coup sûr la clé USB, vous pouvez vous aider de la taille exprimée en Go : la clé USB fournie par O'clock a une capacité de 32 Go. Votre disque dur interne surement beaucoup plus.

Une fois que vous avez identifié le bon disque, notez son numéro : dans l'exemple ci-dessus c'est "Disque 1", on retient donc 1.

##### Cloner la clé USB

Il s'agit maintenant de copier le contenu de la clé USB dans un fichier qui deviendra le disque dur virtuel de la machine virtuelle.

Lancez l'invite de commandes en mode administrateur :

 - Menu démarrer
 - Tapez `cmd` dans la barre de recherche (1)
 - Clic droit sur le programme `cmd` (2)
 - Cliquez sur "Exécuter en tant qu'administrateur" (3)
 
 La fenêtre suivante s'affiche : (Figure 3 : Gestion des disques)
 
 ![Gestion des disques](img/run-cmd.png)
 
- Tapez la commande : `cd c:\Program Files\Oracle\VirtualBox` (on rentre dans le répertoire de VirtualBox qui contient un fichier exécutable que nous allons exécuter)
- Tapez la commande suivante **en remplaçant le `#` par le numéro du disque trouvé précédemment** qui correspond à la clé USB `VBoxManage.exe internalcommands createrawvmdk -rawdisk \\.\PhysicalDrive# -filename c:\usb.vmdk`

Cette commande permet de lire le contenu de la clé USB. Si elle a fonctionné, vous verrez s'afficher le message `RAW host disk access VMDK file c:\usb.vmdk created successfully.`

- Tapez la commande suivante `VBoxManage.exe clonemedium disk c:\usb.vmdk c:\teleporteur.vdi --format VDI`

Vous verrez ensuite des pourcentages correspondant à l'état d'avancement. Cette opération peut durer plusieurs minutes. Ne l'interrompez pas et ne retirez pas la clé pendant la copie.

À la fin de l'opération un message similaire s'affichera : 

`Clone medium created in format 'VDI'. UUID: e64187c6-589c-4ec2-a75f-4c6347171b75`

Vous avez maintenant un fichier à la racine de votre partition principale (`c:`) appelé `teleporteur.vdi` qui contient exactement le même contenu que la clé USB que vous avez reçu.

Vous pouvez maintenant passer à l'étape [Création de la machine virtuelle](#creation-de-la-machine-virtuelle).

#### Sous macOS

- Assurez-vous que la clé USB contenant le Téléporteur n'est pas branchée à votre Mac.
- Ouvrez le Terminal
- Tapez la commande `diskutil list`. Vous obtenez la liste des supports de stockage (disques durs, clés USB, etc.) présents dans votre Mac et, pour chaque disque dur, la liste des partitions qu'il contient.
- Regardez bien les supports de stockage présents **en l'absence de la clé USB du Téléporteur**
- Branchez la clé USB
- Tapez à nouveau la commande `diskutil list` et identifiez la clé USB qui est maintenant listée. Vous pouvez vous aider en cherchant les mots "`(external, physical)`" et du fait qu'elle a une capacité d'environ 32 Go
- Notez l'identifiant de la clé USB : `/dev/diskX` où `X` est un nombre spécifique à votre système (par exemple `/dev/disk2`)
- Positionnez-vous à l'aide de la commande `cd` dans un répertoire à l'intérieur duquel vous stockerez le fichier VDI du disque virtuel, par exemple un sous-dossier dans `Documents`
- Tapez la commande suivante `sudo VBoxManage convertfromraw [IDENTIFIANT-CLE] teleporteur.vdi --format VDI` en remplaçant  `[IDENTIFIANT-CLE]` par votre identifant de la clé USB (par exemple `/dev/disk2`)
- Un message vous indiquera que la conversion est en cours. Cela peut durer assez longtemps. Comptez 15-30 minutes.
- À la fin du processus, vérifiez que le fichier `teleporteur.vdi` a bien été créé et qu'il a une taille de fichier de plusieurs Go. C'est normal qu'il ne pèse pas 32 Go car la clé USB n'est pas pleine et l'espace vide est compressé.
- Modifiez les permissions du fichier `teleporteur.vdi` afin qu'il appartienne à votre utilisateur habituel, par la commande `sudo chown [UTILISATEUR] teleporteur.vdi` en remplaçant [UTILISATEUR] par le nom de votre utilisateur. Celui-ci est indiqué juste avant le `$` dans l'invite de commande du Terminal.

### Création de la machine virtuelle

Nous allons créer une nouvelle machine virtuelle dans VirtualBox.

- Menu "Machine" -> "Nouvelle"
- Nom et système d'exploitation :
  - Nom : Téléporteur
  - Type : Linux
  - Version : Ubuntu (64-bit)
- Taille de la mémoire : attribuez au moins 3 Go de mémoire vive à la VM. Laissez toujours 1 ou 2 Go de mémoire disponible pour l'OS hôte.
- Disque dur :
  - Utiliser un fichier de disque dur virtuel existant : choisir le fichier `teleporteur.vdi` créé à l'étape précédente

Il n'est maintenant plus nécessaire de brancher la clé USB car son contenu a été intégralement copié dans le fichier `teleporteur.vdi`. Vous pouvez donc la ranger en sécurité au cas où il soit nécessaire de refaire cette manipulation ultérieurement suite à une panne informatique par exemple.

### Test de la VM du Téléporteur

Démarrez maintenant la VM du Téléporteur et vérifiez que ces points fonctionnent :

 - Le Téléporteur démarre normalement et arrive sur le bureau avec le fond d'écran vert O'clock
 - Il y a une icône sur le bureau qui correspond au dossier partagé (vous n'avez pas encore accès à son contenu, c'est normal)
 - Le Téléporteur s'est connecté automatiquement à Internet et au VPN d'O'clock et vous pouvez accéder à des sites Internet
 
### Activation du presse-papier partagé

Pour pouvoir effectuer des copier-coller entre le Téléporteur et l'OS hôte, suivez les étapes suivantes :

 - Dans le Gestionnaire des machines virtuelles de Virtualbox, sélectionnez la VM "Téléporteur"
 - Cliquez sur l'icône "Configuration"
 - Section "Général", sous-section "Avancé"
 - "Presse-papier partagé" : "**Bidirectionnel**"

### Continuer la configuration

 - pour configurer le Téléporteur en mode VM graphique, continuez ici : [mode VM graphique](mode-VM-graphique.md)
 - pour configurer le Téléporteur en mode VM serveur, continuez ici : [mode VM serveur](mode-VM-serveur.md)
