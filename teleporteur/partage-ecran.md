# Partage d'écran

La fonctionnalité de partage d'écran consiste à afficher l'écran d'un autre étudiant, de la même manière que les formateurs affichent régulièrement les écrans des étudiants.

## Philosophie

Le partage d'écran est activé pour tous les Téléporteurs, sans mot de passe. Cela signifie que tout le monde peut afficher l'écran de tout le monde sans qu'aucune autorisation ne soit nécessaire. 

Cette ouverture s'explique par le fait que nous avons souhaité reproduire le plus fidèlement possible les interactions que l'on peut avoir dans une salle de classe physque. Dans une salle de classe, tout le monde peut, potentiellement, voir l'écran de tout le monde.

Malgré tout, dans une salle de classe, si quelqu'un se penche sur votre écran, vous vous en apercevez. Pour reproduire cela, lorsque quelqu'un affiche votre écran, vous recevez une notification visuelle et sonore.

Nous pensons que le partage d'écran est une fonctionnalité très utile et qui contribue à rendre la formation encore plus vivante. Nous vous encourageons donc à y avoir recours à chaque fois que cela vous semble pertinent, sans pour autant en abuser et en restant dans le strict cadre pédagogique.

## Installation de Vinagre (si nécessaire)

Pour afficher l'écran d'un autre étudiant, il est nécessaire d'installer l'application Vinagre via la commande suivante :

`sudo apt-get install vinagre` (à la question, "Souhaitez-vous continuer ?", répondez "O" puis Entrée)

## Affichage de l'écran d'un étudiant

Vinagre se lance depuis le menu des applications, rubrique "Internet > Visionneur de bureau distants".

Cliquez ensuite sur "Se connecter" et renseignez les informations suivantes :
 - Protocole : VNC
 - Hôte : adresse de la personne dont vous souhaitez afficher l'écran (cf. ci-dessous), par exemple `prenom-nom.vpnuser.oclock.io` (attention à ne pas inclure d'autres caractères avant ou après l'adresse)
 - Options VNC : "Vue seule" (si vous ne cochez pas cette case, vous piloterez la souris et le clavier de l'ordinateur concerné mais dans la grande majorité des cas ce n'est pas souhaitable)

Cliquez ensuite sur "Se connecter".

Dans le cas où l'étudiant n'utilise pas le téléporteur, le serveur VNC demande un mot de passe : 123456 !

## Adresses des étudiants

Chaque étudiant dispose de sa propre adresse à chaque fois qu'il est connecté au VPN.
