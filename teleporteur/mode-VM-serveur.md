


> Documentation déplacée ici: http://documentation.vpn.oclock.io/publique/teleporteur/vm/30-vm_serveur



Si auparavant vous avez bien suivi les étapes génériques du [mode VM](mode-VM) et que vous avez réussi à lancer le Téléporteur, continuez ici.

### Configuration du réseau

Il s'agit maintenant de configurer le réseau de la VM que nous venons de créer.

- Sélectionnez la VM "Téléporteur" et cliquez sur "Configuration"
- Section "Réseau"
  - "Mode d'accès réseau" : Accès par pont
  - "Nom" : choisissez l'interface réseau par laquelle votre OS hôte est connecté à Internet
  - Déroulez les paramètres avancés ("Avancé")
    - Notez la valeur contenue dans le champ "Adresse MAC", elle nous sera utile plus tard.

# Adresse IP du Téléporteur

On va maintenant relever l'adresse IP du Téléporteur. Attention, il s'agit d'une adresse dynamique temporaire attribuée par votre box ! Il est donc possible qu'elle change. Pour la rendre permanente, il faudra modifier la configuration du serveur DHCP de votre box (on verra ça plus loin).

Pour cela, il suffit de survoler l'icône de l'activité réseau, tout en bas à droite de la fenêtre de la VM :

![Adresse IP de la VM](img/virtualbox-ip-address.png)

Dans l'exemple ci-dessus, l'adresse IP est la suivante : `192.168.0.76`

Ouvrez maintenant un navigateur sur l'OS hôte et tapez cette adresse IP. Vous devriez accéder au serveur web du Téléporteur.

Si tout va bien, passons à l'étape suivante !

# Configuration du partage de fichiers entre le Téléporteur et l'OS hôte

On va maintenant préparer la VM au partage de fichiers entre l'OS hôte et le Téléporteur. L'idée c'est que les fichiers de travail (fichiers HTML, CSS, scripts PHP, JS, etc.) soient stockés sur l'OS hôte et que la VM du Téléporteur y ait accès via un partage de fichiers.

Avant de commencer, décidez où, dans votre OS hôte, vous aller stocker les fichiers de travail. Ça peut être dans un dossier sur le bureau ou à tout autre endroit de votre disque dur que vous pourrez retrouver facilement. À titre d'exemple et pour la suite des indications on va considèrer que les fichiers se trouvent dans un dossier intitulé `docroot` sur le Bureau de votre OS hôte (Windows ou macOS).
  
- Section "Dossiers partagés
  - Cliquez sur l'icône "Ajoute un nouveau dossier partagé"
  - Chemin du dossier : pointer vers le dossier `docroot` sur le Bureau de votre OS hôte
  - Nom du dossier : `docroot`
  - Lecture seule : laisser **décoché** 
  - Montage automatique : **cocher**
  - Configuration permanente : **cocher**
- Appuyez sur "OK" pour enregistrer la configuration de la VM
- Lancez la VM

La première chose à faire consiste à autoriser l'utilisateur `mint` à accéder au dossier partagé dont l'icône est visible sur le bureau du Téléporteur. Pour cela, il suffit d'ajouter l'utilisateur `mint` à un groupe d'utilisateurs (`vboxsf`) qui a les permissions de lecture/écriture sur le dossier partagé.

- Sur le Téléporteur, ouvrez un Terminal
- Tapez cette commande pour ajouter l'utilisateur au groupe vboxsf : "sudo adduser mint vboxsf" (sans les guillemets)
- Puis papez cette commande : "sudo adduser www-data vboxsf" (sans les guillemets)
- Redémarrez normalement le Téléporteur par le bouton `Menu` afin que cette modification soit prise en compte

Après le redémarrage du Téléporteur, double-cliquez sur l'icône du dossier partagé présente sur le bureau du Téléporteur. Vous devriez voir les fichiers du dossier partagé de l'OS hôte. Essayez également de créer des dossiers ou des fichiers afin de vous assurer que vous y avez bien accès en écriture.

Parfait, maintenant que le Téléporteur est capable de lire et écrire dans le dossier partagé stocké dans l'OS hôte, il s'agit de faire en sorte que ce dossier soit utilisé par Apache pour son DocumentRoot (`/var/www/html`).

Premièrement, vous allez déplacer tous vos anciens fichiers de travail contenus dans `/var/www/html` du Téléporteur (ou dans le lien symbolique `html` sur le bureau) vers le dossier partagé. Je vous conseille de le faire en deux étapes : d'abord vous les copiez dans le dossier partagé et après vous les supprimez du dossier `html`. Et si vous êtes presque aussi parano que moi, après les avoir copiés, vous vérifiez dans l'OS hôte qu'ils ont bien été copiés et qu'ils sont tous bien là. Et si vous êtes totalement aussi parano que moi, vous faites également une copie de sauvegarde avant de les supprimer !

Pensez à copier/supprimer également les éventuels dossiers/fichiers cachés. Pour les rendre visibles dans l'explorateur de fichiers du Téléporteur, utilisez le raccourci `Ctrl+H`.

Vous avez tous les dossiers/fichiers dans le dossier partagé et plus rien dans le dossier `html` ? Parfait.

On va maintenant faire pointer le dossier `/var/www/html` vers le dossier partagé.

- Ouvrez le Terminal du Téléporteur
- `cd /var/www`
- `sudo rmdir html`
- `sudo ln -s /media/sf_docroot /var/www/html` (où `sf_docroot` correspond au nom de l'icône du dossier partagé visible sur le bureau du Téléporteur)
- On redémarre Apache pour qu'il prenne en compte le nouveau dossier DocumentRoot : `sudo service apache2 restart`

Si tout s'est bien passé, le serveur web Apache du Téléporteur sert à présent les fichiers qui sont stockés sur votre OS hôte. Pour vous en assurer, crééz un nouveau fichier HTML sur l'OS hôte et lancez le navigateur de l'OS hôte en le faisant pointer sur l'adresse IP du téléporteur pour vérifier si Apache trouve bien le fichier.

Par exemple :
 - Fichier `test.html` stocké à la racine de `docroot`
 - URL du navigateur sur l'OS hôte : `http://[ADRESSE_IP_TELEPORTEUR]/test.html`

# Configuration du VPN du Téléporteur

Nous allons à présent désactiver le VPN au niveau du Téléporteur afin de pouvoir réutiliser le même certificat sur l'OS hôte.

D'abord, on va sauvegarder le certificat. Pour cela, ouvrez un Terminal sur le Téléporteur et taper la commande suivante :

`cp -r /oclock/vpn ~/Bureau`

Vérifiez que cela a bien créé un répertoire appelé `vpn` sur le Bureau du Téléporteur et qu'il contient plusieurs fichiers (`ca.crt`, `user.crt`, etc.).

Nous allons à présent compresser au format ZIP le répertoire `vpn` sur le Bureau du Téléporteur et nous envoyer le fichier zippé par mail pour le réutiliser sur l'OS hôte :
 - Bouton droit sur le dossier `vpn` sur le Burau du Téléporteur
 - "Compresser..."
 - Choisir l'extension ".zip" dans la liste déroulante
 - Appuyser sur "Créer"
 - Un fichier `vpn.zip` est créé sur le bureau
 - Connectez-vous à votre boîte mail sur Chrome pour vous envoyer ce fichier par mail, en pièce jointe

Nous allons maintenant désactiver le VPN du Téléporteur en supprimant tous les fichiers qui ont été créés lors de son activation. Pour cela, tapez dans le Terminal :

`sudo rm /oclock/config/* /oclock/vpn/user.* ; sudo service NetworkManager restart ; nmcli c down tap0`

Des messages indiquant que la la connexion VPN a échoué s'afficheront : c'est normal, vous pouvez les ignorer.

Vérifiez que la connexion Internet fonctionne sur le Téléporteur en ouvrant n'importe quelle page web. Si ce n'est pas le cas, cliquez sur l'icône du réseau dans la barre des notifications du Téléporteur et choisissez "Ethernet automatique". Ou bien essayez de désactiver et réactiver le réseau.

![Réseau du Téléporteur](img/reseau-tp.png)

Nous allons maintenant réactiver le Téléporteur avec un certificat **différent** :

 - Ouvrez le Terminal
 - Tapez : `sudo /opt/scripts/activate-tp.php`
 - Dans la boîte de dialogue qui s'ouvre, tapez la deuxième clé d'activation qui vous a été fournie pour le mode VM serveur. **Attention** : il ne doit pas s'agir de la première clé d'activation que vous avez renseigné lors du premier démarrage du Téléporteur. Si vous n'avez cette deuxième clé d'activation, demandez-la à Dario.
  - Cliquez sur "Valider" et attendez quelques instants que le message "La clé saisie a bien été reconnue" s'affiche. S'il ne s'affiche pas, c'est que la connexion Internet ne semble pas fonctionner. Essayez à nouveau de désactiver et réactiver le réseau. Au pire, redémarrez le Téléporteur et reprenez à partir de la réactivation du Téléporteur (commande `sudo /opt/scripts/activate-tp.php`).

À présent, votre Téléporteur est connecté à Internet et au VPN via un un certificat différent de celui que vous avez envoyé par mail afin de le réutiliser sur votr OS hôte.

# Configuration du VPN sur l'OS hôte

Nous allons à présent installer un client VPN sur votre OS hôte ainsi que le certificat que vous vous êtes envoyé par mail.

## Préparation du certificat

Pour cette étape vous aurez besoin d'utiliser l'éditeur de code Atom. Si vous ne l'avez pas déjà téléchargé et installé, faites-le : https://atom.io/

 - Pour commencer, téléchargez le fichier `vpn.zip` que vous vous êtes envoyé par mail et décompressez-le sur votre OS hôte.
 - Téléchargez le fichier suivant et enregistrez le sur bureau : https://static.oclock.io/oclock.ovpn
 - Si vous êtes sous Windows, assurez-vous que le fichier s'appelle bien `oclock.ovpn` sans autre extension, notamment sans extention `.txt`. Windows ajoute parfois une extension `.txt` cachée et cela empêche les étapes suivantes de fonctionner.
 - Ouvrez le fichier `oclock.ovpn` avec Atom

Vous allez maintenant devoir insérer dans ce fichier le contenu des fichiers `ca.crt`, `user.key` et `user.crt` contenus dans le dossier `vpn` que vous venez de décompresser à partir du fichier `vpn.zip`. Il va falloir être méticuleux dans cette opération :wink:

 - Le contenu du fichier `ca.crt` doit être inséré à l'intérieur des balises `<ca>` et `</ca>` (ligne 18)
 - Le contenu du fichier `user.key` doit être inséré à l'intérieur des balises `<key>` et `</key>` (ligne 21)
 - Le contenu du fichier `user.crt` doit être inséré à l'intérieur des balises `<cert>` et `</cert>` (ligne 24)

À chaque fois il faudra insérer le contenu du fichier indiqué à la place de la ligne contenant les instructions.

Par exemple, la section <ca> :

```
<ca>
Remplacer cette ligne par le contenu du fichier ca.crt. Ne pas supprimer les balises <ca> et </ca>. Ne pas ajouter de lignes vides avant ou après le contenu du fichier ca.crt.
</ca>
```

Doit devenir :

```
<ca>
-----BEGIN CERTIFICATE-----
BAYTAkZSMQ4wDAYDVQQHEwVQYXJpczEPMA0GA1UEChMGT2Nsb2NrMRIwEAYDVQQD
EwlPY2xvY2sgQ0ExDzANBgNVBCkTBnNlcnZlcjEeMBwGCSqGSIb3DQEJARYPZGFy
aW9Ab2Nsb2NrLmlvMB4XDTE2MDIxNTE1MjcxMFoXDTI2MDIxMjE1MjcxMFowczEL
MAkGA1UEBhMCRlIxDjAMBgNVBAcTBVBhcmlzMQ8wDQYDVQQKEwZPY2xvY2sxEjAQ
BgNVBAMTCU9jbG9jayBDQTEPMA0GA1UEKRMGc2VydmVyMR4wHAYJKoZIhvcNAQkB
Fg9kYXJpb0BvY2xvY2suaW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
AQDtS4BeJHhEOdA5UR4kBsuWwGi0va8WIbETSV57J+UI4HqOlLy48G/GTmh0QSKQ
YZ+/hYQG4FGMgsuOsoR7qSJiyT5G90bCor+cECOt5kszgx7dEzvcgPC47vwgDsJN
clwv1OEj8nvvc0u5Z6K7x53oUJrZVtmYmnBbOCAflpJrD7tJ/iSTVCKclQ3Sf7wz
EaTd5bwtt4U+K8QVhQRs9t/h/JTrP8kRJSjKWBa2zzk+9AEvlS++/eU97Ct2c0US
Af8wDQYJKoZIhvcNAQELBQADggEBAOg4KzqoAdVZS9ZOQt4DLBAOPKt2IpZXJ3J8
xSLe+gmiVD1Et5r3kjiDzJx3gGI/oU09xKy5+KGnxW7kqwm+m+Z1KnpqRc6hMG2H
I6D+UNuQTNl0Moqyoqyz5M6gDPh6K6cikorBQ7cO7pyHuTqp6D/LVrJXpxuJpux3
/GiPWqEzEmMtVGEJ1hXIX5HIQGDwcINexBqW73duZKrK4idWM515Tt1vKUKw717z
ohW0psDYEkZ7AtzIXFWLNe+U/OeDbTf+G2i5piyLRssA6qc=
-----END CERTIFICATE-----
</ca>
```

Une fois que vous avez intégré les 3 fichiers à l'intérieur de `oclock.ovpn`, vous pouvez enregistrer le fichier et quitter Atom.

Suivez les instructions suivantes en fonction de votre OS hôte : Windows ou macOS.

## Windows

Si vous êtes sous Windows, nous allons installer le logiciel OpenVPN :
 - Allez sur la page de téléchargement d'OpenVPN : https://openvpn.net/index.php/open-source/downloads.html
 - Téléchargez l'installeur pour Windows qui porte similaire à `openvpn-install-2.4.3-I602.exe`
 - Lancez l'installation
 - Acceptez les paramètres par défaut qui vous sont proposés durant l'installation (Next, next, next, etc... Install :stuck_out_tongue_winking_eye:)
 - Acceptez l'installation du périphérique virtuel qui est sont proposée (TAP)
 - Lancez le programme OpenVPN GUI : Menu démarrer > Programmes > OpenVPN > OpenVPN GUI
 - Un message d'erreur vous indique que le fichier de configuration n'a pas été trouvé. C'est normal, appuyez sur Ok et ignorez-le.
 - Dans la barre des notifications de Windows, en bas à droite de l'écran (à côté de l'horloge), reprérez l'icône de OpenVPN GUI appuyez sur le bouton droit de la souris
 - "Import file..."
 - Choisissez le fichier `oclock.ovpn` que vous avez téléchargé et modifié précédemment
 - Validez le message de confirmation
 - Cliquez à nouveau avec le bouton droit de la souris sur l'icône de OpenVPN GUI et choisissez "Connect"
 
Attendez quelques instants et vous devriez voir un message de confirmation apparaître : "oclock is now connected".

Pour confirmer que la connexion VPN O'clock est bien établie, ouvrez l'URL suivante dans votre navigateur : http://vpncheck.oclock.io/

## macOS

Si vous êtes sous macOS, nous allons installer le logiciel Tunnelblick :
 - Allez sur la page de téléchargement de Tunnelblick : https://tunnelblick.net/downloads.html
 - Téléchargez la version **Stable**
 - Lancez l'installation
 - À la question "Avez-vous de tels fichiers de configuration ?", appuyez sur le bouton "J'ai des fichiers de configuration"
 - Validez également le message suivant
 - Allez à l'endroit où se trouve le fichier `oclock.ovpn` que vous avez téléchargé et modifié précédemment et double-cliquez sur l'icône du fichier
 - Un message de TunnelBlick s'affiche, répondez "Moi seulement"
 - Tapez votre mot de passe utilisateur
 - Cliquez sur l'icône de Tunnelblick dans la barre de menus et choisissez "Connecter oclock"
 
 Attendez quelques instants et vous devriez voir un message de confirmation apparaître : "oclock is now connected".
 
 Pour confirmer que la connexion VPN O'clock est bien établie, ouvrez l'URL suivante dans votre navigateur : http://vpncheck.oclock.io/

# Installation et configuration d'un serveur VNC sur l'OS hôte

Le serveur VNC permet aux formateurs et aux autres étudiants de voir votre écran pour mieux vous aider. Cela ne fonctionne que lorsque vous êtes connecté au VPN O'clock.

Les instructions suivantes valent pour MacOS comme pour Windows.

 - Télécharger et installer VNC Connect : https://www.realvnc.com/download/vnc/
 - Au moment de choisir le type de licence à utiliser, choisir "Ouvrir une session RealVNC"
 - Renseigner les identifiants suivants :
   - Email : tech@oclock.io
   - Mot de passe : 7]|vtiA-fuAV~CZ
 - Choisir "Connectivité directe uniquement"
 - Lorsque VNC Connect est lancé, une icône spécifique apparaît dans la zone dédiée aux notifications
 - Rentrer dans les "Options" de VNC Connect :
   - Sécurité :
     - Chiffrement : de préférence inactif
     - Authentification : Mot de passe VNC (le mot de passe à configurer vous sera demandé ultérieurement et il doit être "123456")
   - Utilisateurs et autorisations :
     - Utilisateur invité VNC (guest) : Autorisations administratives
   - Expert :
     - AlwaysShared : Vrai
     - EnableGuestLogin : Vrai
   
Bien que nous ayons défini un mot de passe, la connexion à votre serveur VNC pourra se faire, sans mot de passe, en utilisant tout simplement l'utilisateur "guest".

# Installation d'autres logiciels sur l'OS hôte

Si vous ne les avez pas déjà, les logiciels suivants vous seront utiles pendant la formation :
 - Atom : https://atom.io
 - Google Chrome : https://www.google.fr/chrome/browser/

# Désactivation du mode graphique du Téléporteur

Une fois que vous avez terminé la configuration du Téléporteur en mode VM serveur, vous pouvez désactiver le mode graphique. Ainsi, lorsque vous lancerez le Téléporteur, il consommera très peu de ressources car l'interface graphique ne sera pas chargée.

Pour ce faire, ouvrez le Terminal et tapez la commande suivante :

`sudo systemctl set-default multi-user.target`

Il vous suffira de redémarrer pour constater que le Téléporteur ne lancera plus l'interface graphique au démarrage.

## Lancer l'interface graphique manuellement

Si vous voulez tout de même accéder à l'interface graphique, suivez les étapes suivantes :
 - À l'invite `mint login:` tapez `mint` (il se peut que le clavier soit configuré en AZERTY)
 - À l'invite `password` tapez Entrée (mot de passe vide)
 - Une fois identifié, tapez : `sudo service mdm start`
 
 Lors du prochain démarrage, le système démarrera en mode texte.
 
 ## Rétablir le démarrage automatique en mode graphique
 
 Pour rétablir de manière permanente le démarrage automatique en mode graphique, tapez la commande suivante :
 
 `sudo systemctl set-default graphical.target`

# Réduire les ressources du Téléporteur

Maintenant que le Téléporteur fonctionne en mode serveur, vous pouvez réduire les ressources qui lui sont allouées. Cela se passe dans la fenêtre de configuration de la machine virtuelle :
 - Mémoire vive : 1 Go
 - Nombre de processeurs : 1

# Se connecter en SSH au Téléporteur

TODO (authentification par clé SSH sur utilisateur root)

# Préparation à une journée de cours

Maintenant que tout est prêt, voici les étapes à effectuer tous les jours en allumant votre ordinateur :
 - Vous connecter au VPN (via TunnelBlick sous macOS ou via OpenVPN sous Windows)
 - Lancer VirtualBox
  - Démarrer la VM Téléporteur en mode "Démarrage sans affichage" :
    - Bouton droit de la souris sur la VM
    - "Démarrer"
    - "Démarrage sans affichage"

Dans ce mode, le Téléporteur s'exécutera mais ne sera pas visible. Il occupera donc encore moins de ressources.
