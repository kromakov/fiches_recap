


> Documentation déplacée ici: http://documentation.vpn.oclock.io/publique/teleporteur/vm/20-vm_graphique



Si auparavant vous avez bien suivi les étapes génériques du [mode VM](mode-VM) et que vous avez réussi à lancer le Téléporteur, continuez ici.

# Activer le support de la Webcam

Afin de permettre à la VM d'utiliser la Webcam de votre ordinateur, il est nécessaire d'installer le "Oracle VM VirtualBox Extension Pack" à l'adresse suivante : https://www.virtualbox.org/wiki/Downloads (lien "All supported platforms").

Si le Téléporteur est lancé, vous pouvez l'arrêter.

Une fois le fichier d'installation téléchargez, éxécutez-le. Cela ouvrira une fenêtre à l'intérieur de VirtualBox qui vous demandera de confirmer l'installation. Confirmez et suivez les instructions pour terminer l'installation de l'Extension Pack.

- Cliquez sur l'icône du Téléporteur dans VirtualBox et ensuite sur l'icône "Configuration"
- Entrez dans l'onglet "USB"
- Cochez la case "Activer le contrôleur USB"
- Cochez la sous-option "Contrôleur USB 2.0 (EHCI)"
- Validez par "Ok"

# Configurer la machine virtuelle

Toujours dans la fenêtre de configuration de la machine virtuelle, apportez les modifications suivantes.

Onglet "Système" : 
 - Processeur :
  - Nombre de processeurs : le maximum recommandé
  - Ressources allouées : 100%

Onglet "Affichage" :
 - Écran :
  - Nombre d'écrans : 2 (si vous avez une configuration double écran)

# Lancer le Téléporteur

Au lancement du Téléporteur, vous aurez 2 fenêtres qui s'ouvrent : chacune représente un écran virtuel. Vous pouvez déplacer chaque fenêtre sur un de vos écrans physiques et passer ensuite en mode plein écran : Menu "Écran" -> "Mode plein écran". Notez bien le raccourci clavier qui vous est indiqué avant de confirmer car il vous permettra de quitter le mode plein écran.

# Premier démarrage

Nous te conseillons à présent la lecture du Guide du Téléporteur à partir de la page 18 !
