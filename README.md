# Fiches récaps

**Toutes les notions vues en cours, récapitulées, triées, classées.**

N'hésitez pas à collaborer pour enrichir le contenu de ce repo !

> :wave: Tu souhaite modifier une fiche-récap, mais tu ne sais pas comment t'y prendre ?! Pas de panique, il y a [une fiche récap ](MAJ-fiche.md) pour ça ! :sunglasses: :rocket:
