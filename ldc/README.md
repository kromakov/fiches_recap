# Ligne de commande

_La ligne de commande est la meilleure amie du développeur. Aidez-la à vous aider, en apprenant son fonctionnement !_

## Utilisation de commandes

- [Utiliser le terminal](terminal.md)
- [Utiliser `git`](git.md)
- [Un peu d'administration système](adminsys.md)

## Configuration en ligne de commande

- [Configurer le serveur Apache](apache.md)
