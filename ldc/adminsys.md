# Admin système

### mettre en ligne un site web sur serveur:
* Complémentaire au développement web
* Différentes solutions d'hébergement
* Permet vision d'ensemble

`/!\ Sous Linux /!\`

### I- Différents types de serveurs
1. Serveur dédié (*physique* ou *virtuel*)
  **Virtuel** (*VPS ou Cloud*) : serveur dédié complètement émulé dans un physique
  (CPU, RAM, HDD ...)
  MAIS sans contrainte matérielle inaltérable ET évoluable sans changer le hardware (augmenter RAM etc.)
2. Hébergement mutualisé
  Une "part" de serveur : pas besoin d'administrer tout le serveur
  (*CPU, RAM etc.* ET logiciels installés (et serveur apache ou FTP))
  MAIS puissance partagée et pas possibilité non plus de modifier le soft
  (*ex : version/fonctionnalités de PHP*)

### II- Les 7 travaux de l'adminsys (administrateur système)
  1. Choisir le bon hébergement
  2. Installer le serveur (sauf si mutualisé)
  3. Configurer le serveur (OS...)
  4. Sécuriser le serveur
  5. Mettre le site en prod.
  6. Maintenir et surveiller le serveur
  7. Faire évoluer et migrer le serveur

### III- Quelques offres d'hébergement
  1. **OVH** (+ gros français)
    plusieurs offres pour héberger un site:
    * offres les moins chères (dès 1.49€HT/mois) : mutualisé
      *ex1 Kimsufi :*
      - 1Go de stockage
      - 1 nom de domaine offert (rien à voir avec l'hébergement)
      - 2 comptes mails offerts (idem, rien à voir)
      - 1 CMS (système de gestion du contenu) au choix parmi 4 (Wordpress, Joomla...)
      - Langages : PHP 5.5, 6, 7, Pearl, Python, MySQL...
      - BDD (base de donnée) : max 100 Mo et 30 connexions simultanées
      - upgrades possibles : perso (100Go stockage, 75 connexions simu...), pro (250Go etc.)
      - SSH = accès par ligne de commande
      ... c'est un hébergement très complet !
      *ex2 Performance :*
       - CPU 1x vCORE (virtuel) & RAM 2Go = puissance garantie !
  2. **Online** (+ ancien)
    *serveurs dédiés physiques* : attentif sur le hard et sur l'entretien !
    MAIS - cher à puissance égale
  3. **Gandi** : le plus pertinent pour nous


<!--**********-->
### Votre Serveur Gandi
-------------
* nom : nomDuServeur
* admin : monNom
* mdp : monPassword
* IPv4 : xxx.xxx.xxx.xxx

#### se connecter en LDC
* (invite par défaut : `monNom@nomDuServeur...`, super utilisateur : `root@nomDuServeur...`#)
  * connexion : `ssh monNom@xxx.xxx.xxx.xxx`
  * déconnexion : `exit`
  * devenir root : `su`
  * DL et installer apache : `apt-get install apache2`
  * DocumentRoot : `cd /var/www/html`
  * rendre dispo pour user : `chown -R monNom /var/www/html`
  * créer/éditer fichier : `nano monFichier`

#### Relier un nom domaine sur un serveur Gandi

Suivre la procédure "en deux clics" expliquée ici : http://wiki.gandi.net/fr/iaas/references/server/gandi-ai/domain-dns

Donc on s'arrête à _Il est nécessaire d'attendre au moins trois heures..._ si votre serveur et votre domaine sont chez Gandi (ce qui est le cas par défaut).

### LDC depuis local
(*HORS* du serveur donc, sur `mint@mint`) :
  * envoyer un fichier en SSH : `scp index.html monNom@xxx.xxx.xxx.xxx:/home/monNom`
  * envoyer un dossier en SSH : `scp -r monSite monNom@xxx.xxx.xxx.xxx:/home/monNom`
