# Les commentaires

## Que fait un commentaire ?

- Rien dans la plupart des cas
- Quelques exceptions :

Les Annotations Symfony qui permettent selon un structure précise de réaliser une action

```php
<?php
/**
 * @Route("/{id}", requirements={"id" = "\d+"}, defaults={"id" = 1})
 */
 ?>
```
## Utilité du commentaire

- Pour donner une indication à la personne qui reprendra le code. Cette personne pouvant être nous-même :slightly_smiling_face:

> Houla, j'ai fait ça il y'a à 6 mois, ça faisait quoi déjà... Je comprends plus rien ! :scream:

- Pour désactiver temporairement une ligne de code.

## Best practices du commentaire

- Simple et concis
- Utilisé généreusement pour rendre explicite votre demarche par n'importe quel lecteur
- Généralement rédigé en anglais pour qu'il puisse être compris dans un maximum de pays

## Réaliser un commentaire

### Commentaire simple (une seule ligne)

En HTML

```html

<!-- This is my main block -->

<div id="main">
  ....
</div>

```

En PHP

```php
<?php
//Compute total of cookies

$nbCookies = $nbNewCookies + $nbOldCookies;

?>

```

JS

```javascript
...

//Compute total of cookies

var nbCookies = nbNewCookies + nbOldCookies;

```

### Commentaire en bloc (plusieurs lignes)

HTML

```
<!--
<div id="main">
  <div id="block1"></div>
  <div id="block2"></div>
</div>
-->
```

PHP

```php
<?php
/*
Compute total of cookies


$nbCookies1 = $nbNewCookies1 + $nbOldCookies1;
$nbCookies2 = $nbNewCookies2 + $nbOldCookies2;
$nbCookies3 = $nbNewCookies3 + $nbOldCookies3;
$nbCookies4 = $nbNewCookies4 + $nbOldCookies4;
$nbCookies5 = $nbNewCookies5 + $nbOldCookies5;

*/
?>

```

Note : le principe d'utilisation en PHP est le même en Javascript

## Aller plus loin avec les commentaires

[Docblockr](packages.md)
