# ES2015

## `let`

L’instruction `let` vous permet de déclarer une variable comme l’instruction `var`, à la seule différence que la portée de `let` se limite à son bloc courant.  
On peut par exemple utiliser `let` pour une boucler `for`, la variable qui sert à l’itération sera alors limitée au niveau la boucle et n’entrera pas en conflit avec le reste du code !

[Lien MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/let)

## `const`

L’instruction `const` vous permet de déclarer une variable à assignation unique.
Ce qui signifie que la variable sera en “lecture seule” et qu'une valeur ne peut pas lui être affecté de nouveau.

Attention tout de même : C'est une constante au niveau référence. C'est-à-dire que le contenu d'un tableau ou d'un objet déclaré avec const bloque la réassignation de la variable, mais ne rend pas la valeur immuable.

[Lien MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/const)


## Fonction Fléchée (Arrow Function) `=>`

L’utilité principale d’une fonction fléchée(`Arrow Function`) est d’avoir une syntaxe plus courte.  
Au lieu d’avoir :
```js
 let multiply = function(item) {
  return item * item;
}
```

On aura une version raccourcie :
```js
let multiply = item => item * item;
```

À noter que les parenthèses ne sont obligatoires que si on a plus d’un seul argument.

```js
let add = (item1, item2) => item1 + item2;
```

Il y a 2 façons d’écrire une fonction fléchée, celle au-dessus et celle-là :
```js
let add = (item1, item2) => {
  return item1 + item2;
}
```
Les deux façon font exactement la même chose.  
La seule différence réside dans les `{ }` qui oblige d’utiliser un `return`.

Dernier schéma possible, si le callback n’a pas d’argument :
On peut soit utiliser un **couple de parenthèse**, soit un **underscore** :

`() => console.log(‘Hello’);`  
`_ => console.log(‘Hello’);`  

[Lien MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Fonctions_fl%C3%A9ch%C3%A9es)


## Littéraux de gabarits

Les littéraux de gabarits (_template literals_) sont des littéraux de chaînes de caractères qui intègrent des expressions.  
Ils sont délimités par des backticks `` ` `` et peuvent contenirs des _éléments de substitution_ : ils sont alors indiqués par un dollar et des accolades `${nomDeMaVariable}`.
On passe par exemple de :
```js
const a = 5;
const b = 10;
console.log('Je suis une string qui a un chiffre a: ' + a + ' et un chiffre b:' + b + ', et si on multiplie les 2 ça donne :' + (a * b));
```

à

```js
const a = 5;
const b = 10;
console.log(`Je suis une string qui a un chiffre a: ${a} et un chiffre b: ${b}, et si on multiplie les deux ça donne : ${a * b}`);
```

[Lien MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Litt%C3%A9raux_gabarits)


## Paramètre du reste

La syntaxe du paramètre du reste permet de représenter un **nombre indéfini** d'arguments contenus dans un tableau.

Pour exemple :
```js
function showInConsole(...arguments) {
  console.log(arguments);
}

showInConsole('Coucou', 'React', [1, 2, 3]);
// ['Coucou', 'React', [1, 2, 3]]
```

[Lien MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/param%C3%A8tres_du_reste)


## Shorthand property

Les objets, on en crée à la pelle quand on fait du JS.  
Souvent, on se retrouve à faire ce genre de choses :

```js
const firstname = 'Balthazar';
const name = 'Picsou';

const user = {
  firstname: firstname,
  name: name,
  hello: function() {
    console.log(`hello, I'm ${this.firstname} ${this.name}`);
  },
};
```

ES2015 nous apporte la possibilité d'écrire les propriétés de manière raccourcie, de la façon suivante :

```js
const firstname = 'Balthazar';
const name = 'Picsou';

const user = {
  firstname,
  name,
  hello() {
    console.log(`hello, I'm ${this.firstname} ${this.name}`);
  },
};
```

C'est-à-dire :
* Quand la propriété de l'objet a le même nom que la variable, on peut écrire `{ prop }` au lieu de `{ prop: prop }`
* Quand la propriété de l'objet est une fonction, on peut écrire `{ prop() { /* ... */ } }` au lieu de `{ prop: function() { /* ... */ } }`


## Décomposition

- Le principe : L'affectation par décomposition (_destructuring en anglais_) permet d'extraire des données d'un tableau ou d'un objet.

```js
const datas = {
  firstname: 'Parker',
  lastname: 'Lewis',
};

// ES5
var firstName = datas.firstname;
var lastName = datas.lastName;

// ES2015
const { firstname, lastname } = datas;
```

C'est pratiquement la même chose avec un tableau :yum:

```js
const students = ['Hannah', 'Coraline', 'Fred'];

// ES5
var first = students[0];
var second = students[1];
var last = students[2];

// ES2015
const [first, second, last] = students;
```

On peut aussi l'utiliser en tant que paramètre de fonction :

```js
const myFunction = ({ lang, method }) => {
  return `${lang} - ${method}`;
};

myFunction({ lang: 'React', method: 'ReactDOM' });
```


## `Array.from()`

* `Array.from()`, pour créer un array à partir d'une structure similaire, comme une `HTMLCollection`.
[Lien MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/from)


## Modules

### Import

L'instruction `import` est utilisée pour importer des données :
fonctions, objets ou valeurs primitives exportées depuis un autre script.

Il y a plusieurs possibilités pour importer un module :

* Si l'import est nommé :
`import { nomDeMaFonction } from 'mon-module';`

* Si il y a plusieurs imports nommés :
`import { nomFonctionUn, nomFonctionDeux } from 'mon-module';`

* Si il y a un `export default` :
`import MonModule from 'mon-module';`

* Si il y a un `export default` et plusieurs imports nommés…
`import MonModule, { nomFonctionUn, nomFonctionDeux } form 'mon-module';`

### Export

L'instruction `export` est utilisée pour permettre d'exporter des données.

Il existe deux types d'export, chacun correspondant à une des syntaxes présentées ci-dessous :

#### Les exports nommés

Ils permettent de rendre disponible une ou plusieurs données.
Le **nom utilisé** dans chacun des exports pourra être utilisé réciproquement
dans les imports pour désigner telle ou telle valeur qu'on souhaite récupérer.

```js
export const maVar = /* ... */;

export const maFonction = () => { /* ... */ };
```

#### Les exports par défaut

On n'en trouvera qu'un seul pour chaque module.
On pourra ainsi exporter une classe, une fonction, un objet ou une autre valeur
qu'on pourra importer par ailleurs **sans en connaître le nom**.
Ce sera en quelque sorte la valeur « principale » exportée,
qu'on pourra réimporter simplement.

```js
export default () => { /* ... */ };
```

```js
const maFonction = () => { /* ... */ };

export default maFonction;
```
