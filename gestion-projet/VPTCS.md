# Opquast

[Opquast](https://www.opquast.com/) (_Open Quality Standards_) est un référentiel généraliste centré sur la qualité des projets Web. Il est possible de passer une certification qui atteste d'un niveau de connaissance de ce référentiel.

## Modèle VPTCS

Le modèle VPTCS d'Opquast peut être d'une grande aide pour structurer son approche : évaluer la charge de travail, collaborer avec les différents métiers (chef de projet, designer, développeurs, etc.), s'organiser, contrôler la pertinence de son travail… Ce modèle offre une vue suffisamment globale pour englober toutes les étapes du projet, tout en segmentant les activités (commerciales, techniques, etc.)

![Le modèle VPTCS d'Opquast](img/vptcs-w3qualite.png)

On peut décliner ce modèle à travers différents prismes.

### Attentes utilisateur

![Les attentes utilisateurs dans le modèle VPTCS](img/opquast-vptcs-1-attentes.png)

### Domaines d'activité

![Les domaines d'activité dans le modèle VPTCS](img/opquast-vptcs-2-metiers.png)
