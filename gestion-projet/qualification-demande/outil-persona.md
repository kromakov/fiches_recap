Les persona
===========

Un persona est, concrètement, une fiche-texte agrémentée d'une photo, qui décrit un utilisateur-type, un archétype, une représentation fictive d'un utilisateur-cible du site.

Les persona peuvent être utilisés comme support de réflexion à tous les stades du projet, pour fixer des priorités, ainsi que pour cadrer les décisions à prendre en conception.

## Exemple de persona

![Exemple de persona](../img/persona.jpg)

## Création d'un persona

Ce n'est pas :

- un [profil-utilisateur](https://fr.wikipedia.org/wiki/Profil_utilisateur)
- un [segment marketing](https://www.definitions-marketing.com/definition/segment/)
- un rôle particulier.

Un persona est décrit au moyen de trois axes :

- le contexte (cadre de vie, histoire personnelle, habitudes, biais…)
- des buts & des comportements
- les implications sur le projet (besoins/attentes du persona par rapport au projet).

Idéalement, les persona, bien que fictifs, devraient être imaginés sur la base d'entretiens utilisateurs préalables au développement du projet. Comme c'est une démarche coûteuse et difficile à mettre en place, les persona sont généralement inventés en essayant d'explorer les pistes suivantes concernant les utilisateurs potentiels du projet :

- Contexte :
  - Quel est l’environnement dans lequel ils évoluent ?
  - Quelle est leur histoire, dans les grandes lignes ?
  - Quels sont leur traits de personnalité ?
  - Qu’est ce qu’ils aiment et n’aiment pas ?
  - Quelles sont leurs influences ? Leurs biais ?
- But et comportements (cohérents avec le contexte) :
  - Quels sont leurs buts ?
  - Quelles sont les tâches et activités associées, leur fréquence, leur durée, leur importance … les actions nécessaires pour atteindre leurs buts ?
  - Qu'est ce qui déclenche leurs actions ? (importance des évènements déclencheurs et freins à la [conversion](https://www.definitions-marketing.com/definition/conversion/))
- Implications sur le projet (en cohérence avec les buts & comportements) :
  - Quels sont les meilleurs moyens de rendre visible auprès d'eux le projet ?
  - Quels contenus recherchent-ils en priorité ?
  - Quels services recherchent-ils en priorité ?
  - Quels sont les points à éviter absolument pour éviter de les faire fuire ?

Un persona va imager _une_ piste probable issues d'une variétés de réponses possibles aux questions ci-dessus. Il faut bien noter que ces questions concernent les utilisateurs _réels_ (futurs ou actuels, selon l'état d'avancement du projet), pas les personnages fictifs ! Un persona est donc avant tout le _résultat_ d'un processus de recherche à propos des utilisateurs finaux (le persona étant créé _a posteriori_, pas pendant cette phase de recherche). Ce processus fait partie de la [qualification de la demande](../qualification-demande.md).

> Pour cadrer le processus créatif des persona, il peut être intéressant d'avoir en tête le [modèle VPTCS](../VPTCS.md).

## Utilisation d'un persona

L'intérêt des persona est de fournir un support concret de discussion, une aide à la prise de décision. À travers les persona, il est plus simple d'identifier les besoins qui doivent être *impérativement* remplis par le projet.

Ces besoins vont avoir des implications en terme de marketing, d'ergonomie (UX/UI), d'implémentation technique, de coût, de priorisation, etc.

## Risques

- Les persona doivent être spécifiques et précis, afin d'être « actionnables » (aident à prendre des décisions). Les persona courent le risque de modéliser/généraliser alors qu'ils sont conçus pour « exemplifier » !
- Les persona doivent aider à se connecter à une base utilisateur encore naissante, voire inexistante ; mais ne peuvent, et ne doivent, remplacer les retours utilisateurs de terrain (tests utilisateurs, enquêtes d'opinion, etc.). Rien de pire que des persona déconnectés du réel, et qui donnent un sentiment (erroné) de compréhension de sa base utilisateur !
- Les persona sont très utiles en début de projet (qualification de la demande, des besoins), mais également comme éléments de contrôle, par exemple pour les tests fonctionnels, les critères d'acceptance de _user stories_… Ne pas les oublier !
- La création et l'utilisation des persona est avant tout un prétexte pour discuter du projet et qualifier la demande. Ce ne doit pas être une activité solo du chef de projet ou du _Product Owner_, mais bien un moment récurrent d'échange collectif !

## Pour aller plus loin

- [_An introduction to personas and how to create them_](https://www.steptwo.com.au/papers/kmc_personas/) (en) présente entre autre le procédé de création d'un persona
- [_Personas and the Advantage of Designing for Yourself_](http://bokardo.com/archives/personas-and-the-advantage-of-designing-for-yourself/) (en) présente entre autre certaines faiblesses des persona