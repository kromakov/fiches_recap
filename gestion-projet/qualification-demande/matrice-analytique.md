Les matrices analytiques
========================

_Fiche en cours de rédaction._

## QQOQCCP

- https://fr.wikipedia.org/wiki/QQOQCCP
- https://www.qualiblog.fr/outils-et-methodes/methode-qqoqccp-outil-analyse-simple-et-performant/
- Le plus important est de réaliser une écoute active, pour _identifier_ dans les réponses du client les éléments d'intérêt pour passer faciliter la rédaction des use-cases : acteurs, intervenants, systèmes, buts respectifs, etc.
- Les questions _quoi, qui, où, quand_ sont plutôt factuelles et 1er degré, elles permettent de clarifier le besoin sous-jacent à la demande ; tandis que les questions _combien, comment, pourquoi_ sont plutôt contextuelles et 2d degré, elles permettent de clarifier les _contraintes_ associées au besoin.
- À l'issue de cette démarche analytique, il est de bon ton de produire un document récapitulatif, un _brief fonctionnel_, qu'on envoie au client pour validation. La forme importe peu, l'important c'est que le client et le prestataire soient d'accord sur l'analyse du besoin (sans aucune considération sur les fonctionnalités précises, l'ergonomie ou l'implémentation à ce stade)
- Contrôle des réponses aux questions avec [des indicateurs SMART](https://fr.wikipedia.org/wiki/Objectifs_et_indicateurs_SMART)

## _Story mapping_

Technique d'analyse consistant à organiser des user stories selon deux axes : temporalité et priorité.
