# Scrum

Scrum (signifiant une _mélée_ de Rugby), basé sur les principes agiles, est une méthode de gestion de projet ou plus exactement un cadre de développement.

Ainsi le [Scrum framework](https://www.scrum.org/resources/what-is-scrum) est appellé [](https://www.scrum.org/)

## Le concept

Des équipes pluridisciplinaires et auto-organisées réalisent des produits de manière itérative et incrémentale.

Scrum strucutre le développement en cycles de travail appelés **Sprint**. Ces cycles durent entre 1 et 4 semaines (en général 2 semaines) et s'enchaînent les uns après les autres.

Un sprint commence et fini à une date précise que le travail prévu soit achevé ou non. Les sprints ne sont jamais prolongés.

Avant chaque sprint, une réunion entre le chef de projet (Scrum master) et l'équipe se tient afin de déterminer les tâches (demandes) tangiblement terminables durant le sprint à venir. L'idée étant de pouvoir fournir / achever des fonctionnalités pouvant être "livrables". Suite à cette réunion, le sprint suivant ne recevra plus de nouvelles tâches.

Durant chaque sprint, l'équipe se réunit brièvement (10-15min) tous les jours afin de s'assurer de la progression, d'ajuster les prochaines étapes, de relever d'éventuels points bloquants.

Après chaque sprint, l'équipe présente le résultat de leur travail afin qu'il soit intégré au projet.

![Scrum](img/scrum.png) _issu de_ [scrum.org](https://www.scrum.org/)

## Résumé

- https://www.scrum.org/
- https://www.scrumalliance.org/why-scrum : lire "The SCRUM framework in 30 seconds"
- https://www.mountaingoatsoftware.com/agile/scrum/scrum-tools/task-boards
- Description ultra-simple d'un worflow Scrum avec Trello :
  - https://blog.hadrien.eu/2014/01/31/scrum-avec-trello/
  - https://trello.com/b/ultdy0tY/template-scrum-with-trello
- [Ressources sur SCRUM/Trello](https://trello.com/b/VliA4Xzm/scrum)

## Les rôles

### Product owner

- Responsable du "produit"
- Expert métier
- Représente le client (enjeux, intérêts, priorités)
- S'assure du ROI (retour sur investissement)

### Scrum master

- Facilitateur du projet
- Responsable projet
- Assure le respect de Scrum (sprint, tâches, responsabilités, ...)
- Ne peut pas être le Product owner

### Équipe

- Pluridisciplinaire / plurifonctionnelle
- Construit le produit
- Les membres travaillent **ensemble**, objectif commun
- Analyste, concepteur, architecte, développeur, ...

## Les composants

### Product Backlog

Le Product Backlog centralise la liste de tout ce qui doit être fait.

Le Product Backlog est une feuille de route, il evolue tout au long de la vie du produit.

C'est donc un document central, maintenu par le Product owner, qui sert de référenciel durant tout le projet.

Bien que fréquent composé par des user-stories, la forme des éléments du PB ont peu d'importance tant que leur signification et leur implication sont claire.

Il ressemble généralement à ceci :

| Priorité | Élement                              | Détails | Effort | Sprint |
| -------- | ------------------------------------ | ------- | ------ | ------ |
| 1        | En tant que ... je veux ...          | ...     | 5      | 1      |
| 2        | Amélioration des performances de ... | ...     | 13     | 3      |
| 3        | Mise à jour des serveurs ...         | ...     | 8      | 1      |
| 4        | En tant que ... je veux ...          | ...     | 21     | 2      |
| ...      | ...                                  | ...     | -      | -      |

Un Product Backlog doit être **DEEP**

- **Détaillé** : Les éléments prioritaires sont très détaillés (très fins). Généralement le top 15 du PB doit être composé d'éléments de petite taille (temps de réalisation) afin d'être réparti facilement sur les sprints à venir
- **Estimé** : Les éléments doivent être estimés, l'effort : (difficultés, temps de production, risques). 
- **Evolutif** (ou Emergent) : Le PB est continuellement en mutation, il évolue. Le Product owner doit prendre en compte les difficultés techniques, les impératifs, les temps, les nouvelles demandes, ...
- **Priorisé** : Il est crucial que le PB contiennent des éléments priorisés afin de dresser un schéma respectant le "Plus pour votre argent" = grande valeur métier / faible coût de production. Ce principe peut être outrrepasser par la gestion de l'urgence (sécurité, visibilité, etc...)

### Sprint Backlog

Le Sprint Backlog est le doc de regroupant l'ensemble des éléments du PB (Product Backlog) identifiés / choisis pour le sprint

Ce Sprint Backlog ne change pas durant le sprint.

Le processus de construction du doc peut être le suivant

```
En tant que client je veux ajouter un produit au panier

TODO 

- Modifier la base de données
- Créer la page (UI)
- Créer la page (JS)
- Écrire les tests
- Mettre à jour la page d'aide utilisateur
- ...
```


## Les phases

- Sprint planning
- Daily Scrum
- Sprint review
- Sprint retrospective

## Les outils

### Burndown chart

- Lecture sur wikipédia : [Burndown chart](https://fr.wikipedia.org/wiki/Burndown_chart)
- http://www.agilenutshell.com/burndown


### Kanban

Kanban est un système utilisé afin de limiter les tâches en cours. [Wikipédia](<https://fr.wikipedia.org/wiki/Kanban_(d%C3%A9veloppement)>).

Kanban en ligne :

- La référence, Trello : https://trello.com
- github projects (présent sur les repo)
- Un équivalent Open Source, français, dont la documentation est pertinente : https://kanboard.net/fr
- Une autre alternative : https://taiga.io/
